#!/bin/bash
echo "Stop Procedure : ruoyi-gateway.jar"
pid=`ps -ef |grep java|grep ruoyi-gateway.jar|awk '{print $2}'`
echo 'old service pid:'$pid
if [ "$pid" = "" ]
then
  echo "service is stoped"
else
  kill -9 $pid
  echo 'service is killed'
fi
sleep 3
nohup java -jar /opt/ruoyigateway/ruoyi-gateway.jar > /opt/ruoyigateway/log/gateway.log 2>&1 &
pid=`ps -ef |grep java|grep ruoyi-gateway.jar|awk '{print $2}'`
echo 'ruogyi-gateway started:/opt/ruoyigateway/log/gateway.log PID:'$pid
# tail -f /opt/ruoyigateway/log/gateway.log