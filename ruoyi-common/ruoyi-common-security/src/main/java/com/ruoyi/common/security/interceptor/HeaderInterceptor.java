package com.ruoyi.common.security.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.log4j.Log4j2;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.AsyncHandlerInterceptor;
import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.context.SecurityContextHolder;
import com.ruoyi.common.core.utils.ServletUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.security.auth.AuthUtil;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.system.api.model.LoginUser;

/**
 * 自定义请求头拦截器，将Header数据封装到线程变量中方便获取
 * 注意：此拦截器会同时验证当前用户有效期自动刷新有效期
 *
 * @author ruoyi
 */
@Log4j2
public class HeaderInterceptor implements AsyncHandlerInterceptor
{
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception
    {
        if (!(handler instanceof HandlerMethod))
        {
            return true;
        }

        SecurityContextHolder.setUserId(ServletUtils.getHeader(request, SecurityConstants.DETAILS_USER_ID));
        SecurityContextHolder.setUserName(ServletUtils.getHeader(request, SecurityConstants.DETAILS_USERNAME));
        SecurityContextHolder.setUserKey(ServletUtils.getHeader(request, SecurityConstants.USER_KEY));
        SecurityContextHolder.setUnionId(ServletUtils.getHeader(request, SecurityConstants.WX_UNIONID));
        SecurityContextHolder.setOpenId(ServletUtils.getHeader(request,SecurityConstants.USER_KEY));

        log.debug("DETAILS_USER_ID:{}",ServletUtils.getHeader(request, SecurityConstants.DETAILS_USER_ID));
        log.debug("DETAILS_USERNAME:{}",ServletUtils.getHeader(request, SecurityConstants.DETAILS_USERNAME));
        log.debug("USER_KEY:{}",ServletUtils.getHeader(request, SecurityConstants.USER_KEY));
        log.debug("WX_UNIONID:{}",ServletUtils.getHeader(request, SecurityConstants.WX_UNIONID));
        log.debug("WX_OPENID:{}",ServletUtils.getHeader(request, SecurityConstants.USER_KEY));

        String token = SecurityUtils.getToken();
        if (StringUtils.isNotEmpty(token))
        {
            LoginUser loginUser = AuthUtil.getLoginUser(token);
            if (StringUtils.isNotNull(loginUser))
            {
                AuthUtil.verifyLoginUserExpire(loginUser);
                SecurityContextHolder.set(SecurityConstants.LOGIN_USER, loginUser);
                log.debug("获取用户信息：{}",loginUser.toString());
//                SecurityContextHolder.setUserId(loginUser.getSysUser().getUserId());
//                SecurityContextHolder.setUserName(loginUser.getSysUser().getUserName());
//                SecurityContextHolder.setUserKey(loginUser.getToken());
//                SecurityContextHolder.setUnionId(loginUser.getUnionId());
//                SecurityContextHolder.setOpenId(loginUser.getToken());
            }
            else
            {
                log.debug("获取用户信息：{}",loginUser);
            }
        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception
    {
        SecurityContextHolder.remove();
    }
}
