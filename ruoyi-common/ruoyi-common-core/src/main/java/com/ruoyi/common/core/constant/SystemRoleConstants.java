package com.ruoyi.common.core.constant;

/**
 * @author songping
 * @date 2022/4/11 15:21:32
 */
public class SystemRoleConstants {

    /**
     * 服务商 系统角色的id
     */
    public static final Long MAINTAINER_ROLE_ID = 108L;
    /**
     * 服务商 系统角色标识
     */
    public static final String MAINTAINER_ROLE_NAME = "maintainer";
    /**
     * 服务商 系统部门的id
     */
    public static final Long MAINTAINER_DEPT_ID = 211L;
    /**
     * 投资商 系统角色的id
     */
    public static final Long FRANCHISEES_ROLE_ID = 107L;
    /**
     * 投资商 系统角色的标识
     */
    public static final String FRANCHISEES_ROLE_NAME = "franchisee";

    /**
     * 投资商 系统部门的id
     */
    public static final Long FRANCHISEES_DEPT_ID = 213L;
    /**
     * 广告主 系统角色的id
     */
    public static final Long ADVERTISING_ROLE_ID = 105L;
    /**
     * 广告主 系统角色的标识
     */
    public static final String ADVERTISING_ROLE_NAME = "advUser";
    /**
     *广告主 系统部门的id
     */
    public static final Long ADVERTISING_DEPT_ID = 202L;
    /**
     * 服务商员工 系统角色的id
     */
    public static final Long MAINTAINER_EMP_ROLE_ID = 109L;
    /**
     * 服务商员工 系统角色的标识
     */
    public static final String MAINTAINER_EMP_ROLE_NAME = "maintainerEmp";
    /**
     * 广告服务商 系统角色的id
     */
    public static final Long ADV_MAINTAINER_ROLE_ID = 111L;
    /**
     * 广告服务商 系统角色的id
     */
    public static final String ADV_MAINTAINER_ROLE_NAME = "advMaintainer";
    /**
     * 物业公司 系统角色的id
     */
    public static final Long COMPANY_ROLE_ID = 106L;
    /**
     * 物业公司 系统角色的id
     */
    public static final String COMPANY_ROLE_NAME = "company";

}
