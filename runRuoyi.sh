#!/bin/bash

APP=''
checkpid()
{
   javaps=`$JAVA_HOME/bin/jps  | grep $APP`

   if [ -n "$javaps" ]; then
      WX_USER_SERVICE_psid=`echo $javaps | awk '{print $1}'`
      echo "================================"
      echo "app: $APP already started!(pid:$WX_USER_SERVICE_psid)"
      echo "================================"
   else
      WX_USER_SERVICE_psid=0
      echo "app $APP not running..."
   fi
}


mvnb()
{
echo 'start pull'
cd /opt/scode/RuoYi-Cloud/
git pull
echo 'start building....'
mvn clean install -s ../settings.xml
echo 'copy jars'
cp /opt/scode/RuoYi-Cloud/ruoyi-auth/target/ruoyi-auth.jar /opt/deploy/jars/ruoyi-auth-NoHost.jar
cp /opt/scode/RuoYi-Cloud/ruoyi-gateway/target/ruoyi-gateway.jar /opt/deploy/jars/
cp /opt/scode/RuoYi-Cloud/ruoyi-modules/ruoyi-system/target/ruoyi-modules-system.jar /opt/deploy/jars/
cp /opt/scode/RuoYi-Cloud/ruoyi-modules/ruoyi-miniapp/target/ruoyi-modules-miniapp.jar /opt/deploy/jars/
cp /opt/scode/RuoYi-Cloud/ruoyi-modules/ruoyi-sms/target/ruoyi-modules-sms-3.2.0.jar /opt/deploy/jars/ruoyi-modules-sms.jar
cp /opt/scode/RuoYi-Cloud/ruoyi-visual/ruoyi-monitor/target/ruoyi-visual-monitor.jar /opt/deploy/jars/
cp /opt/scode/RuoYi-Cloud/ruoyi-modules/ruoyi-app/target/ruoyi-modules-app.jar /opt/deploy/jars/
cp /opt/scode/RuoYi-Cloud/ruoyi-modules/ruoyi-sms/target/ruoyi-modules-sms-3.2.0.jar /opt/deploy/jars/
}

run()
{
APP='nacos'
checkpid
        if [ $WX_USER_SERVICE_psid -ne 0 ]; then
#            # echo "================================"
#            # echo "warn: $APP already started! (pid=$WX_USER_SERVICE_psid)"
#            # echo "================================"
             echo "next...."
             echo ""
        else
                echo 'start nacos'
                JAVA_CMD="bash /root/wait-for-it.sh localhost:8848 -q -t 10  -- bash /opt/deploy/nacos/bin/startup.sh"                echo $JAVA_CMD
                su - root -c "$JAVA_CMD"
                sleep 10
                checkpid
        fi

APP='ruoyi-auth'
checkpid
        if [ $WX_USER_SERVICE_psid -ne 0 ]; then
              #echo "================================"
              #echo "warn: $APP already started! (pid=$WX_USER_SERVICE_psid)"
              #echo "================================"
              echo "next...."
              echo ""
        else
                echo 'start ruoyi-auth'
                JAVA_CMD="bash /root/wait-for-it.sh localhost:9200 -q -t 10  -- nohup java -jar -Dfile.encoding=utf-8 -Djava.security.egd=file:/dev/./urandom -jar /opt/deploy/jars/ruoyi-auth-NoHost.jar >/opt/deploy/logs/ruoyi-auth.log 2>&1 &"
                echo $JAVA_CMD
                su - root -c "$JAVA_CMD"
                sleep 10
                checkpid
        fi

#APP='ruoyi-gateway'
#checkpid
#        if [ $WX_USER_SERVICE_psid -ne 0 ]; then
              #echo "================================"
              #echo "warn: $APP already started! (pid=$WX_USER_SERVICE_psid)"
              #echo "================================"
#             echo "next...."
#              echo ""
#        else
#                echo 'start ruoyi-gateway'
#                JAVA_CMD="bash /root/wait-for-it.sh localhost:8080 -q -t 10  -- nohup java -jar -Dfile.encoding=utf-8 -Djava.security.egd=file:/dev/./urandom -jar /opt/deploy/jars/ruoyi-gateway.jar >/opt/deploy/logs/ruoyi-gateway.log 2>&1 &"
#                echo $JAVA_CMD
#                su - root -c "$JAVA_CMD"
#                sleep 10
#                checkpid
#        fi


APP='ruoyi-modules-system'
checkpid
        if [ $WX_USER_SERVICE_psid -ne 0 ]; then
              #echo "================================"
              #echo "warn: $APP already started! (pid=$WX_USER_SERVICE_psid)"
              #echo "================================"
              echo "next...."
              echo ""
        else
                echo 'start ruoyi-modules-system'
                JAVA_CMD="bash /root/wait-for-it.sh localhost:9201 -q -t 10  -- nohup java -jar -Dfile.encoding=utf-8 -Djava.security.egd=file:/dev/./urandom -jar /opt/deploy/jars/ruoyi-modules-system.jar >/opt/deploy/logs/ruoyi-system.log 2>&1 &"
                echo $JAVA_CMD
                su - root -c "$JAVA_CMD"
                sleep 10
                checkpid
        fi

#APP='ruoyi-visual-monitor'
#checkpid
#        if [ $WX_USER_SERVICE_psid -ne 0 ]; then
              #echo "================================"
              #echo "warn: $APP already started! (pid=$WX_USER_SERVICE_psid)"
              #echo "================================"
#              echo "next...."
#              echo ""
#        else
#                echo 'start ruoyi-monitor'
#                JAVA_CMD="bash /root/wait-for-it.sh localhost:9100 -q -t 10  -- nohup java -jar -Dfile.encoding=utf-8 -Djava.security.egd=file:/dev/./urandom -jar /opt/deploy/jars/ruoyi-visual-monitor.jar >/opt/deploy/logs/ruoyi-monitor.log 2>&1 &"
#                echo $JAVA_CMD
#                su - root -c "$JAVA_CMD"
#                sleep 10
#                checkpid
#        fi


APP='ruoyi-modules-miniapp'
checkpid
        if [ $WX_USER_SERVICE_psid -ne 0 ]; then
              #echo "================================"
              #echo "warn: $APP already started! (pid=$WX_USER_SERVICE_psid)"
              #echo "================================"
              echo "next...."
              echo ""
        else
                echo 'start ruoyi-modules-miniapp'
                JAVA_CMD="bash /root/wait-for-it.sh localhost:9333 -q -t 10  -- nohup java -jar  /opt/deploy/jars/ruoyi-modules-miniapp.jar >/opt/deploy/logs/ruoyi-miniapp.log 2>&1 &"
                echo $JAVA_CMD
                su - root -c "$JAVA_CMD"
                sleep 10
                checkpid
        fi

APP='ruoyi-modules-app'
checkpid
        if [ $WX_USER_SERVICE_psid -ne 0 ]; then
              #echo "================================"
              #echo "warn: $APP already started! (pid=$WX_USER_SERVICE_psid)"
              #echo "================================"
              echo "next...."
              echo ""
        else
                echo 'start ruoyi-modules-app'
                JAVA_CMD="bash /root/wait-for-it.sh localhost:9303 -q -t 10  -- nohup java -jar -Dfile.encoding=utf-8 -Djava.security.egd=file:/dev/./urandom -jar /opt/deploy/jars/ruoyi-modules-app.jar >/opt/deploy/logs/ruoyi-app.log 2>&1 &"
                echo $JAVA_CMD
                su - root -c "$JAVA_CMD"
                sleep 10
                checkpid
        fi

APP='ruoyi-modules-sms'
checkpid
        if [ $WX_USER_SERVICE_psid -ne 0 ]; then
              #echo "================================"
              #echo "warn: $APP already started! (pid=$WX_USER_SERVICE_psid)"
              #echo "================================"
              echo "next...."
              echo ""
        else
                echo 'start ruoyi-modules-sms'
                JAVA_CMD="bash /root/wait-for-it.sh localhost:9302 -q -t 10  -- nohup java -jar -Dfile.encoding=utf-8 -Djava.security.egd=file:/dev/./urandom -jar /opt/deploy/jars/ruoyi-modules-sms-3.2.0.jar >/opt/deploy/logs/ruoyi-sms.log 2>&1 &"
                echo $JAVA_CMD
                su - root -c "$JAVA_CMD"
                sleep 10
                checkpid
        fi
} #run



stop(){
#       APP='nacos'
#       checkpid
#       JAVA_CMD="kill -9 $WX_USER_SERVICE_psid"
#       su - root -c "$JAVA_CMD"
#       checkpid

        APP='ruoyi-auth'
        checkpid
        if [ $WX_USER_SERVICE_psid -ne 0 ]; then
        JAVA_CMD="kill -9 $WX_USER_SERVICE_psid"
        su - root -c "$JAVA_CMD"
        fi
        checkpid

#       APP='ruoyi-gateway'
#        checkpid
#        JAVA_CMD="kill -9 $WX_USER_SERVICE_psid"
#        su - root -c "$JAVA_CMD"
#        checkpid

        APP='ruoyi-modules-system'
        checkpid
        if [ $WX_USER_SERVICE_psid -ne 0 ]; then
        JAVA_CMD="kill -9 $WX_USER_SERVICE_psid"
        su - root -c "$JAVA_CMD"
        fi
        checkpid

        APP='ruoyi-modules-miniapp'
        checkpid
        if [ $WX_USER_SERVICE_psid -ne 0 ]; then
        JAVA_CMD="kill -9 $WX_USER_SERVICE_psid"
        su - root -c "$JAVA_CMD"
        fi
        checkpid

        APP='ruoyi-modules-app'
        checkpid
        if [ $WX_USER_SERVICE_psid -ne 0 ]; then
        JAVA_CMD="kill -9 $WX_USER_SERVICE_psid"
        su - root -c "$JAVA_CMD"
        fi
        checkpid

        APP='ruoyi-modules-sms'
        checkpid
        if [ $WX_USER_SERVICE_psid -ne 0 ]; then
        JAVA_CMD="kill -9 $WX_USER_SERVICE_psid"
        su - root -c "$JAVA_CMD"
        fi
        checkpid

#       APP='ruoyi-visual-monitor'
#       checkpid
#        JAVA_CMD="kill -9 $WX_USER_SERVICE_psid"
#        su - root -c "$JAVA_CMD"
#        checkpid
}

info()
{
        APP='nacos'
        checkpid

        APP='ruoyi-auth'
        checkpid

        APP='ruoyi-gateway'
        checkpid

        APP='ruoyi-modules-system'
        checkpid

        APP='ruoyi-modules-miniapp'
        checkpid

        APP='ruoyi-modules-app'
        checkpid

        APP='ruoyi-modules-sms'
        checkpid

        APP='ruoyi-visual-monitor'
        checkpid
}

case "$1" in
   'build')
      mvnb
      ;;
   'stop')
     stop
     ;;
   'update')
     stop
     mvnb
     run
     ;;
   'run')
     run
     ;;
   'info')
     info
     ;;
  *)
     echo "Usage: $0 {build|stop|update|run|info}"
     exit 1
esac
exit 0