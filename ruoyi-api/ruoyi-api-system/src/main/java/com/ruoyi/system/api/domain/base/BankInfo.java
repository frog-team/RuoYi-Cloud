package com.ruoyi.system.api.domain.base;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "bank_info")
public class BankInfo {

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    private String bankCode;

    private String bankName;

    private Integer sort;

}
