package com.ruoyi.system.api;

import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.system.api.domain.SysDept;
import com.tdd.wx.users.entity.WxUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.domain.SysUser;
import com.ruoyi.system.api.factory.RemoteUserFallbackFactory;
import com.ruoyi.system.api.model.LoginUser;

import java.util.Map;

/**
 * 用户服务
 *
 * @author ruoyi
 */
@FeignClient(contextId = "remoteUserService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteUserFallbackFactory.class)
public interface RemoteUserService
{
    /**
     * 通过用户名查询用户信息
     *
     * @param username 用户名
     * @param source 请求来源
     * @return 结果
     */
    @GetMapping("/user/info/{username}")
    public R<LoginUser> getUserInfo(@PathVariable("username") String username, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 注册用户信息
     *
     * @param sysUser 用户信息
     * @param source 请求来源
     * @return 结果
     */
    @PostMapping("/user/register")
    public R<Boolean> registerUserInfo(@RequestBody SysUser sysUser, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("/user/findByUnionId")
    public R<SysUser> findByUnionid(@RequestParam("unionid") String unionid, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @PostMapping("/user/newregister")
    public R<SysUser> newregisterUserInfo(@RequestBody SysUser sysUser, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 新增系统用户
     * @param user 系统用户信息
     * @return
     */
    @PostMapping("/user/addSysUser")
    public R<SysUser> addSysUser(@RequestBody SysUser user);

//    /**
//     * 新增系统用户
//     * @param user 系统用户信息
//     * @return
//     */
//    @PostMapping("/user/edit")
//    public R<SysUser> editSysUser(@RequestBody SysUser user);

    /**
     * 用户授权角色
     * @param roleIds 角色id的数组
     * @param userId  用户id
     * @return
     */
    @PutMapping("/user/authRoleByUseId/{userId}")
    public R insertAuthRoleByUserId(@PathVariable("userId") Long userId,@RequestParam("roleIds") Long[] roleIds);

    /**
     * 创建部门
     * @param dept 部门信息
     * @return
     */
    @PostMapping("/dept/addDept")
    public R<SysDept> addDept(@RequestBody SysDept dept);


    /**
     * 依据 userId 与 判断是否为广告主角色
     * songping 2022-04-11
     * @param userId 当前用户id
     * @param params
     * @return
     */
    @GetMapping("/user/checkIsAdvertising/{userId}")
    public String checkIsAdvertising(@PathVariable("userId") Long userId, @RequestParam("params") String params);
}
