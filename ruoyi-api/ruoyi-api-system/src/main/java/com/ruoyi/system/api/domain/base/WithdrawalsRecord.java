package com.ruoyi.system.api.domain.base;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName(value = "withdrawals_record")
public class WithdrawalsRecord {

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("提现金额")
    private BigDecimal money;

    @ApiModelProperty("提现后的金额")
    private BigDecimal laterMoney;

    @ApiModelProperty("银行名称")
    private String bankName;

    @ApiModelProperty("银行卡卡号")
    private String bankNo;

    @ApiModelProperty("开户行")
    private String openingBank;

    @ApiModelProperty("提现状态（10:待处理 20:提现成功 30:提现失败）")
    private Integer withdrawalsState;

    @ApiModelProperty("提现方式（10:银行卡 20:支付宝 30:微信）")
    private Integer withdrawalsWay;

    @ApiModelProperty("提现时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date withdrawalsTime;

    @ApiModelProperty("用户user_id")
    private Long userId;

    @ApiModelProperty("转账时间")
    private Date payTime;

    @ApiModelProperty("交易流水号")
    private String dealNum;

    @ApiModelProperty("银行返回的报文")
    private String bankMessage;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("提现渠道（1=小程序 2=web端 3=App 等)")
    private Integer withdrawOrigin;

    private Long invoiceId;

    private String payeeName;

    @ApiModelProperty("实际到账金额")
    private BigDecimal actualAccountAmount;

    @ApiModelProperty("扣税金额")
    private BigDecimal taxAmount;

    @ApiModelProperty("税率")
    private BigDecimal taxRate;

    @ApiModelProperty("开票方式(1=电子发票，2=纸质发票)")
    private Integer invoiceWay;

    @ApiModelProperty("纸质发票上传的资源id")
    private String resId;

    @ApiModelProperty("电子发票地址")
    private String elecInvoiceUrl;

    @TableField(exist = false)
    private String unionid;


}
