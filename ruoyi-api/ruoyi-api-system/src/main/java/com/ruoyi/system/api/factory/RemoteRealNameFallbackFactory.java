package com.ruoyi.system.api.factory;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.RemoteRealNameService;
import com.ruoyi.system.api.model.RealNameAuthInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * 文件服务降级处理
 *
 * @author ruoyi
 */
@Component
public class RemoteRealNameFallbackFactory implements FallbackFactory<RemoteRealNameService>
{
    private static final Logger log = LoggerFactory.getLogger(RemoteRealNameFallbackFactory.class);

    @Override
    public RemoteRealNameService create(Throwable throwable)
    {
        log.error("短信服务调用失败:{}", throwable.getMessage());
        return new RemoteRealNameService()
        {
            @Override
            public R<RealNameAuthInfo> findByUnionIdAndType(String unionid, Long roleType,String source)
            {
                return R.fail("查询认证失败:" + throwable.getMessage());
            }

        };
    }
}
