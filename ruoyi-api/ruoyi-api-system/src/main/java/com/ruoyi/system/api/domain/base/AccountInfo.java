package com.ruoyi.system.api.domain.base;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class AccountInfo {

    @TableId(value = "user_id",type = IdType.INPUT)
    private Long userId;

    @ApiModelProperty("余额")
    private BigDecimal balance;

    @ApiModelProperty("用户微信uid")
    private String fkUid;

    @ApiModelProperty("银行卡号")
    private String bankCardNo;

    @ApiModelProperty("支付宝账号")
    private String alipayAccount;

    @ApiModelProperty("创建时间")
    private Date createDate;

    @ApiModelProperty("修改时间")
    private Date updateDate;

    @ApiModelProperty("总收入")
    private BigDecimal totalIncome;

    @ApiModelProperty("公众号openid")
    private String openid;

    @ApiModelProperty("真实姓名")
    private String realName;

    @ApiModelProperty("税率")
    @TableField(exist = false)
    private BigDecimal taxRate;
}
