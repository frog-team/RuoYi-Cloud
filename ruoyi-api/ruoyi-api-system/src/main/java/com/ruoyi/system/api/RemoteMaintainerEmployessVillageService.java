package com.ruoyi.system.api;

import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.domain.app.MaintainerEmployeesVillage;
import com.ruoyi.system.api.factory.RemoteMaintainerEmployessVillageFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(contextId = "remoteMaintainerEmployessVillageService", value = ServiceNameConstants.APP_SERVICE, fallbackFactory = RemoteMaintainerEmployessVillageFallbackFactory.class)
public interface RemoteMaintainerEmployessVillageService {

    @GetMapping("maintainerEmployeesVillage/findByEmployeesUnionid")
    public R<List<MaintainerEmployeesVillage>> findByEmployeesUnionid(@RequestParam("unionid") String unionid, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("maintainerEmployeesVillage/findEUidAndMid")
    public R<List<MaintainerEmployeesVillage>> findEUidAndMid(@RequestParam("eUnionid") String eUnionid,@RequestParam("mUid") Long mUid, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("maintainerEmployeesVillage/save")
    public R save(@RequestParam("employeesUnionid")String employeesUnionid,@RequestParam("maintainerUserId")Long maintainerUserId,@RequestParam("villageCodes")String villageCodes,@RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 基于员工的uid 获取当前 服务商员工的信息
     * @param empUnionId 员工的uid
     * @return 当前员工的信息
     */
    @GetMapping("maintainerEmployeesVillage/findMaintainerIdByEmpUnionId")
    public R<MaintainerEmployeesVillage>  findMaintainerIdByEmpUnionId(@RequestParam("empUnionId") String empUnionId);
}
