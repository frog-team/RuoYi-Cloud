package com.ruoyi.system.api;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.domain.base.AccountInfo;
import com.ruoyi.system.api.domain.base.BankInfo;
import com.ruoyi.system.api.domain.base.WithdrawalsRecord;
import com.ruoyi.system.api.factory.RemoteWithdrawalsRecordServiceFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(contextId = "remoteWithdrawalsRecordService", value = ServiceNameConstants.MINIAPP_SERVICE, fallbackFactory = RemoteWithdrawalsRecordServiceFallbackFactory.class)
public interface RemoteWithdrawalsRecordService {

    @GetMapping("withdrawalsRecord/getBalance")
    public R<AccountInfo> getBalance(@RequestParam("unionid") String unionid,@RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("withdrawalsRecord/applyCash")
    public R applyCash(@RequestBody WithdrawalsRecord withdrawalsRecord, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("withdrawalsRecord/bankList")
    public R<List<BankInfo>> bankList(@RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("withdrawalsRecord/selfListPage")
    public R<Page<WithdrawalsRecord>> selfListPage(@RequestParam("page") Page<WithdrawalsRecord> page, @RequestParam("unionid") String unionid,@RequestHeader(SecurityConstants.FROM_SOURCE) String source);

}
