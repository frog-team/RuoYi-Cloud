package com.ruoyi.system.api.factory;

import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.RemoteMaintainerEmployessVillageService;
import com.ruoyi.system.api.RemoteRealNameService;
import com.ruoyi.system.api.domain.app.MaintainerEmployeesVillage;
import com.ruoyi.system.api.model.RealNameAuthInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 文件服务降级处理
 *
 * @author ruoyi
 */
@Component
public class RemoteMaintainerEmployessVillageFallbackFactory implements FallbackFactory<RemoteMaintainerEmployessVillageService>
{
    private static final Logger log = LoggerFactory.getLogger(RemoteMaintainerEmployessVillageFallbackFactory.class);

    @Override
    public RemoteMaintainerEmployessVillageService create(Throwable throwable)
    {
        log.error("短信服务调用失败:{}", throwable.getMessage());
        return new RemoteMaintainerEmployessVillageService()
        {
            @Override
            public R<List<MaintainerEmployeesVillage>> findByEmployeesUnionid(String unionid, String source)
            {
                return R.fail("查询员工信息失败:" + throwable.getMessage());
            }

            @Override
            public R<List<MaintainerEmployeesVillage>> findEUidAndMid(String eUnionid, Long mUid,String source){
                return R.fail("查询员工信息失败:" + throwable.getMessage());
            }

            @Override
            public R save(String employeesUnionid,Long maintainerUserId,String villageCodes,String source){
                return R.fail("保存员工信息失败:" + throwable.getMessage());
            }

            @Override
            public R<MaintainerEmployeesVillage> findMaintainerIdByEmpUnionId(String empUnionId) {
                return R.fail("获取员工信息失败:" + throwable.getMessage());
            }
        };
    }
}
