package com.ruoyi.system.api;

import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.factory.RemoteRealNameFallbackFactory;
import com.ruoyi.system.api.model.RealNameAuthInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(contextId = "remoteRealNameService", value = ServiceNameConstants.MINIAPP_SERVICE, fallbackFactory = RemoteRealNameFallbackFactory.class)
public interface RemoteRealNameService {

    @GetMapping("wxuser/findByUnionIdAndType")
    public R<RealNameAuthInfo> findByUnionIdAndType(@RequestParam("unionid") String unionid, @RequestParam("roleType") Long roleType, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

}
