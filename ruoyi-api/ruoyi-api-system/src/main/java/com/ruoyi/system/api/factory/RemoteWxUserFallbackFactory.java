package com.ruoyi.system.api.factory;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.system.api.RemoteUserService;
import com.ruoyi.system.api.RemoteWxUserService;
import com.tdd.wx.users.entity.WxUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author lobyliang
 */
@Component
public class RemoteWxUserFallbackFactory implements FallbackFactory<RemoteWxUserService>{

    private static final Logger log = LoggerFactory.getLogger(RemoteWxUserFallbackFactory.class);

    @Override
    public RemoteWxUserService create(Throwable cause){
        log.error("用户服务调用失败:{}", cause.getMessage());
        return new RemoteWxUserService(){
            @Override
            public R<?> getUnionIdByUserId(Long userId){
                return null;
            }

            @Override
            public AjaxResult isWxUserExist(String open_id){
                return AjaxResult.error("查询失败："+cause.getLocalizedMessage());
            }

            @Override
            public AjaxResult findBySystemUserId(Long user_id){
                return AjaxResult.error("查询失败："+cause.getLocalizedMessage());
            }

            @Override
            public AjaxResult findByOpenId(String open_id){
                return AjaxResult.error("查询失败："+cause.getLocalizedMessage());
            }

            @Override
            public AjaxResult findByUnionId(String unionid){
                return AjaxResult.error("查询失败："+cause.getLocalizedMessage());
            }

            @Override
            public TableDataInfo findByShareUnionId(String union_id){
                TableDataInfo tableDataInfo = new TableDataInfo();
                tableDataInfo.setMsg("查询失败："+cause.getLocalizedMessage());
                return tableDataInfo;
            }

            @Override
            public TableDataInfo findByOrigin(int origin){
                TableDataInfo tableDataInfo = new TableDataInfo();
                tableDataInfo.setMsg("查询失败："+cause.getLocalizedMessage());
                return tableDataInfo;
            }

            @Override
            public AjaxResult save(WxUser user){
                return AjaxResult.error("保存失败："+cause.getLocalizedMessage());
            }

            @Override
            public AjaxResult update(WxUser user){
                return AjaxResult.error("查询失败："+cause.getLocalizedMessage());
            }

            @Override
            public AjaxResult delByOpenId(String open_id){
                return AjaxResult.error("查询失败："+cause.getLocalizedMessage());
            }

            @Override
            public TableDataInfo findAll(int pageSize, int pageNo, WxUser example, Integer order){
                TableDataInfo tableDataInfo = new TableDataInfo();
                tableDataInfo.setMsg("查询失败："+cause.getLocalizedMessage());
                return tableDataInfo;
            }

            @Override
            public AjaxResult increasePoints(String open_id, Integer point){
                return AjaxResult.error("操作失败："+cause.getLocalizedMessage());
            }

            @Override
            public AjaxResult consumePoints(String unionId, Integer point){
                return AjaxResult.error("操作失败："+cause.getLocalizedMessage());
            }

            @Override
            public AjaxResult newFindByUnionId(String unionId){
                return AjaxResult.error("查询失败："+cause.getLocalizedMessage());
            }

            @Override
            public AjaxResult updateWxUser(WxUser wxUser){
                return AjaxResult.error("更新失败："+cause.getLocalizedMessage());
            }

            @Override
            public AjaxResult getUserBagLeft(String unionId, String admCode){
                return AjaxResult.error("查询失败："+cause.getLocalizedMessage());
            }

            @Override
            public AjaxResult updateReceiveBagAmount(String openId, int amount){
                return AjaxResult.error("更新失败："+cause.getLocalizedMessage());
            }

            @Override
            public AjaxResult recordGiveOutBagComplete(String operateUserUnionId, int bagAmount){
                return AjaxResult.error("更新失败："+cause.getLocalizedMessage());
            }

            @Override
            public WxUser getBySystemUserId(Long sysUserId){
                return null;
            }

            @Override
            public R<?> getOpenId(String appid, String code){
                return R.fail("操作失败，请稍后再试");
            }
        };
    }
}
