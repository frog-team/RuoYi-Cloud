package com.ruoyi.system.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Date;

/**
 * 实名认证信息对象 real_name_auth_info
 *
 * @author lobyliang
 * @date 2022-01-05
 */
@ApiModel("real_name_auth_info(实名认证信息)")
public class RealNameAuthInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty("主键")
    @Excel(name = "主键")
    private Long id;

    /**
     * 平台表主键
     */
    @ApiModelProperty("平台表主键")
    @Excel(name = "平台表主键")
    private Long userId;

    /**
     * 微信unionid
     */
    @ApiModelProperty("微信unionid")
    @Excel(name = "微信unionid")
    private String unionid;

    /**
     * 身份证正面照
     */
    @ApiModelProperty("身份证正面照")
    @Excel(name = "身份证正面照")
    private String cardPositive;

    /**
     * 身份证反面照
     */
    @ApiModelProperty("身份证反面照")
    @Excel(name = "身份证反面照")
    private String cardReverse;

    /**
     * 法人姓名/真实姓名
     */
    @ApiModelProperty("法人姓名/真实姓名")
    @Excel(name = "法人姓名/真实姓名")
    private String realName;

    /**
     * 身份证号
     */
    @ApiModelProperty("身份证号")
    @Excel(name = "身份证号")
    private String idCard;

    /**
     * 手机号
     */
    @ApiModelProperty("手机号")
    @Excel(name = "手机号")
    private String tel;

    /**
     * 营业执照
     */
    @ApiModelProperty("营业执照")
    @Excel(name = "营业执照")
    private String businessLicense;

    /**
     * 企业税号
     */
    @ApiModelProperty("企业税号")
    @Excel(name = "企业税号")
    private String companyNumber;

    /**
     * 企业类型
     */
    @ApiModelProperty("企业类型")
    @Excel(name = "企业类型")
    private String companyType;

    /**
     * 企业说在地区
     */
    @ApiModelProperty("企业说在地区")
    @Excel(name = "企业说在地区")
    private String companyAddr;

    /**
     * 经营开始时间
     */
    @ApiModelProperty("经营开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "经营开始时间" , width = 30, dateFormat = "yyyy-MM-dd")
    private Date businessBeginTime;

    /**
     * 经营结束时间
     */
    @ApiModelProperty("经营结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "经营结束时间" , width = 30, dateFormat = "yyyy-MM-dd")
    private Date businessEndTime;

    /**
     * 收货人姓名
     */
    @ApiModelProperty("收货人姓名")
    @Excel(name = "收货人姓名")
    private String postName;

    /**
     * 收货电话
     */
    @ApiModelProperty("收货电话")
    @Excel(name = "收货电话")
    private String postTel;

    /**
     * 收货所在区域
     */
    @ApiModelProperty("收货所在区域")
    @Excel(name = "收货所在区域")
    private String postAddr;

    /**
     * 收货详细地址
     */
    @ApiModelProperty("收货详细地址")
    @Excel(name = "收货详细地址")
    private String postDetailAddr;

    /**
     * 认证类型：0：个人认证，1：企业认证
     */
    @ApiModelProperty("认证类型：0：个人认证，1：企业认证")
    @Excel(name = "认证类型：0：个人认证，1：企业认证")
    private Long authType;

    @ApiModelProperty("认证角色类型：10：服务商；20：加盟商；30：广告商")
    private Long roleType;

    @ApiModelProperty("企业名称")
    private String companyName;

    private Date createTime;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getRoleType() {
        return roleType;
    }

    public void setRoleType(Long roleType) {
        this.roleType = roleType;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }

    public String getUnionid() {
        return unionid;
    }

    public void setCardPositive(String cardPositive) {
        this.cardPositive = cardPositive;
    }

    public String getCardPositive() {
        return cardPositive;
    }

    public void setCardReverse(String cardReverse) {
        this.cardReverse = cardReverse;
    }

    public String getCardReverse() {
        return cardReverse;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getRealName() {
        return realName;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getTel() {
        return tel;
    }

    public void setBusinessLicense(String businessLicense) {
        this.businessLicense = businessLicense;
    }

    public String getBusinessLicense() {
        return businessLicense;
    }

    public void setCompanyNumber(String companyNumber) {
        this.companyNumber = companyNumber;
    }

    public String getCompanyNumber() {
        return companyNumber;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyAddr(String companyAddr) {
        this.companyAddr = companyAddr;
    }

    public String getCompanyAddr() {
        return companyAddr;
    }

    public void setBusinessBeginTime(Date businessBeginTime) {
        this.businessBeginTime = businessBeginTime;
    }

    public Date getBusinessBeginTime() {
        return businessBeginTime;
    }

    public void setBusinessEndTime(Date businessEndTime) {
        this.businessEndTime = businessEndTime;
    }

    public Date getBusinessEndTime() {
        return businessEndTime;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostTel(String postTel) {
        this.postTel = postTel;
    }

    public String getPostTel() {
        return postTel;
    }

    public void setPostAddr(String postAddr) {
        this.postAddr = postAddr;
    }

    public String getPostAddr() {
        return postAddr;
    }

    public void setPostDetailAddr(String postDetailAddr) {
        this.postDetailAddr = postDetailAddr;
    }

    public String getPostDetailAddr() {
        return postDetailAddr;
    }

    public void setAuthType(Long authType) {
        this.authType = authType;
    }

    public Long getAuthType() {
        return authType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id" , getId())
                .append("userId" , getUserId())
                .append("unionid" , getUnionid())
                .append("cardPositive" , getCardPositive())
                .append("cardReverse" , getCardReverse())
                .append("realName" , getRealName())
                .append("idCard" , getIdCard())
                .append("tel" , getTel())
                .append("businessLicense" , getBusinessLicense())
                .append("companyNumber" , getCompanyNumber())
                .append("companyType" , getCompanyType())
                .append("companyAddr" , getCompanyAddr())
                .append("businessBeginTime" , getBusinessBeginTime())
                .append("businessEndTime" , getBusinessEndTime())
                .append("postName" , getPostName())
                .append("postTel" , getPostTel())
                .append("postAddr" , getPostAddr())
                .append("postDetailAddr" , getPostDetailAddr())
                .append("createTime" , getCreateTime())
                .append("authType" , getAuthType())
                .toString();
    }
}
