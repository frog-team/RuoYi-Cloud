package com.ruoyi.system.api;

import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.system.api.factory.RemoteWxUserFallbackFactory;
import com.tdd.wx.users.entity.WxUser;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author lobyliang
 */
@FeignClient(contextId = "remoteWxUserService", value = ServiceNameConstants.AUTH_SERVICE, fallbackFactory = RemoteWxUserFallbackFactory.class)
public interface RemoteWxUserService{
    @GetMapping("/wxUser/UnionId/{userId}")
    public R<?> getUnionIdByUserId(@PathVariable(value = "userId") Long userId);

    @GetMapping(value = {"/wxUser/isExist/{openid}"})
    @ApiOperation("查询微信用户是否存在")
    public AjaxResult isWxUserExist(@PathVariable(value = "openid", required = false) String open_id);


    @GetMapping(value = {"/wxUser/bySystemUserId/{openid}"})
    @ApiOperation("查询微信用户信息-ry-SystemId")
    public AjaxResult findBySystemUserId(@PathVariable(value = "openid", required = false) Long user_id);

    @GetMapping(value = {"/wxUser/byOpenId/{openid}"})
    @ApiOperation("查询微信用户信息-OpenId")
    public AjaxResult findByOpenId(@PathVariable(value = "openid", required = false) String open_id);

    @GetMapping(value = {"/wxUser/byUnionId/{unionid}"})
    @ApiOperation("查询微信用户信息-UnionId")
    public AjaxResult findByUnionId(@PathVariable(value = "unionid", required = false) String unionid);

    @GetMapping(value = {"/wxUser/ByShareUnionId/{unionid}"})
    @ApiOperation("查询微信用户信息-分享人")
    public TableDataInfo findByShareUnionId(@PathVariable(value = "unionid", required = false) String union_id);

    @GetMapping(value = {"/wxUser/byOrigin/{origin}"})
    @ApiOperation("查询微信用户信息-来源来源0：未知，1：出袋小程序，2.广告投放小程序，3.运维APP，4.广告投放网站，5.管理中台")
    public TableDataInfo findByOrigin(@PathVariable("origin") int origin);

    @PostMapping("/wxUser")
    @ApiOperation("新建微信用户信息")
    public AjaxResult save(@RequestBody WxUser user);

    @PutMapping("/wxUser")
    @ApiOperation("更新微信用户信息")
    public AjaxResult update(@RequestBody WxUser user);

    @DeleteMapping("/wxUser/{openid}")
    @ApiOperation("删除微信用户信息")
    public AjaxResult delByOpenId(@PathVariable("openid") String open_id);

    @PostMapping("/wxUser/all")
    @ApiOperation("获取所有用户信息,order=>0升序")
    public TableDataInfo findAll(@RequestParam("pageSize") int pageSize, @RequestParam("pageNo") int pageNo, @RequestBody WxUser example, @RequestParam("order") Integer order);

    @PutMapping(value = {"/wxUser/points/{point}/{openid}"})
    @ApiOperation("增加积分")
    public AjaxResult increasePoints(@PathVariable(value = "openid", required = false) String open_id, @PathVariable("point") Integer point);

    @PutMapping(value = {"/wxUser/points/consume/{point}/{unionId}"})
    @ApiOperation("使用积分")
    public AjaxResult consumePoints(@PathVariable(value = "unionId", required = false) String unionId, @PathVariable("point") Integer point);

    @GetMapping(value = {"/wxUser/new/byUnionId/{unionId}"})
    @ApiOperation("查找用户信息-外包")
    public AjaxResult newFindByUnionId(@PathVariable(value = "unionId", required = false) String unionId);

    @PutMapping(value = {"/wxUser/new"})
    @ApiOperation("更新用户信息-外包")
    public AjaxResult updateWxUser(@RequestBody WxUser wxUser);

    @PutMapping(value = {"/wxUser/bagsLeftToday/{admCode}/{unionId}"})
    @ApiOperation("今日可领垃圾袋数量")
    public AjaxResult getUserBagLeft(@PathVariable(value = "unionId", required = false) String unionId, @PathVariable("admCode") String admCode);

    @PutMapping(value = {"/wxUser/receiveBagAmount/{amount}/{openid}"})
    @ApiOperation("领取垃圾袋数量及时间")
    public AjaxResult updateReceiveBagAmount(@PathVariable(value = "openid", required = false) String openId, @PathVariable("amount") int amount);

    @ApiOperation("记录出袋完成")
    @PutMapping("/wxUser/recordGiveOutBagComplete/{operateUserUnionId}/{bagAmount}")
    public AjaxResult recordGiveOutBagComplete(@PathVariable("operateUserUnionId") String operateUserUnionId, @PathVariable("bagAmount") int bagAmount);

    @ApiOperation("按系统用户ID查找")
    @GetMapping("/wxUser/getBySystemUserId/{sysUserId}")
    public WxUser getBySystemUserId(@PathVariable("sysUserId") Long sysUserId);

    @ApiOperation("通过微信平台获取用户OpenId")
    @GetMapping("/wxminiapp/getOpenId/{appid}/{code}")
    public R<?> getOpenId(@PathVariable("appid") String appid,@PathVariable("code") String code);
}
