package com.ruoyi.system.api.factory;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.RemoteFileService;
import com.ruoyi.system.api.RemoteSmsService;
import com.ruoyi.system.api.domain.SysFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件服务降级处理
 *
 * @author ruoyi
 */
@Component
public class RemoteSmsFallbackFactory implements FallbackFactory<RemoteSmsService>
{
    private static final Logger log = LoggerFactory.getLogger(RemoteSmsFallbackFactory.class);

    @Override
    public RemoteSmsService create(Throwable throwable)
    {
        log.error("短信服务调用失败:{}", throwable.getMessage());
        return new RemoteSmsService()
        {
            @Override
            public R sendVerificationCode(String tel,String source)
            {
                return R.fail("发送验证码失败:" + throwable.getMessage());
            }

            @Override
            public R<Boolean> checkVerificationCode(String tel,String code, String source)
            {
                return R.fail("校验验证码失败:" + throwable.getMessage());
            }

            @Override
            public R sendRealtySystemAccount(String tel, String username, String password) {
                return R.fail("发送系统账号信息失败:" + throwable.getMessage());
            }

        };
    }
}
