package com.ruoyi.system.api.domain.app;

import com.baomidou.mybatisplus.annotation.TableField;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import com.tdd.wx.users.entity.WxUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.Tag;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

/**
 * 服务商-员工-小区关联对象 maintainer_employees_village
 *
 * @author lobyliang
 * @date 2022-01-23
 */
@ApiModel("maintainer_employees_village(服务商-员工-小区关联)")
@Data
@ToString
public class MaintainerEmployeesVillage
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty("主键")
    private Long id;

    /** 小区code */
    @ApiModelProperty("小区code")
    @Excel(name = "小区code")
    private String villageCode;

    /** 员工code */
    @ApiModelProperty("员工code")
    @Excel(name = "员工code")
    private String employeesUnionid;

    /** 服务商userid */
    @ApiModelProperty("服务商userid")
    @Excel(name = "服务商userid")
    private Long maintainerUserId;

    private Date createTime;

    @TableField(exist = false)
    private WxUser userinfo;

}
