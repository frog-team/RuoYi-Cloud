package com.ruoyi.system.api.domain.base;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName(value = "balance_record")
public class BalanceRecord {

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("用户id")
    private Long userId;

    @ApiModelProperty("金额")
    private BigDecimal amount;

    @ApiModelProperty("金额存量")
    private BigDecimal balance;

    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("操作类型")
    private String action;

    @ApiModelProperty("描述")
    private String remark;

}
