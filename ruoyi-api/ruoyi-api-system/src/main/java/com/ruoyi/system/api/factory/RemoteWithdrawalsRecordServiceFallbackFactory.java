package com.ruoyi.system.api.factory;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.RemoteFileService;
import com.ruoyi.system.api.RemoteWithdrawalsRecordService;
import com.ruoyi.system.api.domain.SysFile;
import com.ruoyi.system.api.domain.base.AccountInfo;
import com.ruoyi.system.api.domain.base.BankInfo;
import com.ruoyi.system.api.domain.base.WithdrawalsRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Component
public class RemoteWithdrawalsRecordServiceFallbackFactory implements FallbackFactory<RemoteWithdrawalsRecordService> {

    private static final Logger log = LoggerFactory.getLogger(RemoteFileFallbackFactory.class);

    @Override
    public RemoteWithdrawalsRecordService create(Throwable throwable)
    {
        log.error("提现服务调用失败:{}", throwable.getMessage());
        return new RemoteWithdrawalsRecordService()
        {
            @Override
            public R<AccountInfo> getBalance(String unionid, String source){
                return R.fail("查询员工信息失败:" + throwable.getMessage());
            }

            @Override
            public R applyCash(WithdrawalsRecord withdrawalsRecord, String source){
                return R.fail("查询员工信息失败:" + throwable.getMessage());
            }

            @Override
            public R<List<BankInfo>> bankList(String source){
                return R.fail("查询员工信息失败:" + throwable.getMessage());
            }

            @Override
            public R<Page<WithdrawalsRecord>> selfListPage(Page<WithdrawalsRecord> page,String unionid,String source){
                return R.fail("查询提现记录失败:" + throwable.getMessage());
            }

        };
    }

}
