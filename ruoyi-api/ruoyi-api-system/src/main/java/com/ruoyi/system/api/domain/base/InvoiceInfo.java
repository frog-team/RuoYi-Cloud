package com.ruoyi.system.api.domain.base;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("invoice_info")
public class InvoiceInfo {

    @ApiModelProperty("主键")
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("抬头类型(1=企业,2=个人)")
    private Integer titleType;

    @ApiModelProperty("发票类型(1=增值税普通电子发票 2=增值税专用发票)")
    private Integer invoiceType;

    @ApiModelProperty("发票抬头")
    private String invoiceTitle;

    @ApiModelProperty("发票内容(购买设备 广告投放等)")
    private String invoiceContent;

    @ApiModelProperty("税号")
    private String dutyParagraph;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("开票金额")
    private BigDecimal invoicePrice;

    @ApiModelProperty("开票时间")
    private Date invoiceDate;

    @ApiModelProperty("申请时间")
    private Date applyDate;

    @ApiModelProperty("申请来源(1=小程序 2=web端 3=app等)")
    private Integer applyOrigin;

    @ApiModelProperty("开票方式(1=电子发票，2=纸质发票)")
    private Integer invoiceWay;

    @ApiModelProperty("纸质发票上传的资源id")
    private String resId;

    @ApiModelProperty("电子发票地址")
    private String elecInvoiceUrl;

}
