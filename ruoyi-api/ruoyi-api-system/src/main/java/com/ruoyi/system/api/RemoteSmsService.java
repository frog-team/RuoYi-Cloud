package com.ruoyi.system.api;

import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.factory.RemoteSmsFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(contextId = "remoteSmsService", value = ServiceNameConstants.SMS_SERVICE, fallbackFactory = RemoteSmsFallbackFactory.class)
public interface RemoteSmsService {

    @GetMapping("sendVerificationCode")
    public R sendVerificationCode(@RequestParam("tel")String tel, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("checkVerificationCode")
    public R<Boolean> checkVerificationCode(@RequestParam("tel")String tel, @RequestParam("code")String code, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 通过短信向物业公司发送 后台管理系统账号
     * @param tel 手机号
     * @param username 用户名
     * @param password 密码
     * @return
     */
    @GetMapping("/sendRealtySystemAccount")
    public R sendRealtySystemAccount(@RequestParam("tel") String tel,@RequestParam("username") String username, @RequestParam("password") String password);
}
