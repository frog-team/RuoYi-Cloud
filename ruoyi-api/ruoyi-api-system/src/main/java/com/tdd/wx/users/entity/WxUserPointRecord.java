package com.tdd.wx.users.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 微信用户积分记录表
 * @author loby
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxUserPointRecord {

    @Id
    private String id;

    private String unionid;

    /**
     * 操作
     */
    private String operation;

    /**
     * 操作code
     */
    private String operationCode;

    /**
     * 变动金额 （正负值）
     */
    private BigDecimal point;

    private Date createTime;

    /**
     * 创建时间毫秒
     */
    private Long createTimer;


}
