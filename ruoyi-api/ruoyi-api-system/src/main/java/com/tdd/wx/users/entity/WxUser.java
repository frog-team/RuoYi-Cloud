package com.tdd.wx.users.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
//import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/***
 * 微信用户表
 * @author loby
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxUser implements Serializable {
    @Id
    String unionId;

    /**
     * 发袋小程序openId
     */
    String openId;

    /**
     * 广告小程序openId
     */
    String openIdAdv;
    /**
     * 服务商小程序openId（预留）
     */
    String openIdMan;
    /**
     * 小程序openId（预留4）
     */
    String openId4;
    /**
     * 小程序openId(预留5）
     */
    String openId5;
    /**
     * 推荐人微信unionid
     */
    String shareUnionId;
    /**
     * 来源0：未知，1：出袋小程序，2.广告投放小程序，3.运维APP，4.广告投放网站，5.管理中台，..
     */
    Integer orgin;
    /***
     * 来源渠道
     */
    Integer channel;
    /***
     * 微信昵称
     */
    String nickName;
    /***
     * 头像URL
     */
    String avatar;
    /***
     * 手机号
     */
    String phone;
    /***
     * 性别：0：未知;1:男;2:女
     */
    String gener;

    /**
     * 国家
     */
    String country;
    /***
     * 所属省份
     */
    String province;
    /***
     * 所在城市
     */
    String city;
    /**
     * 坐标经度
     */
    Double longitude;

    /**
     * 坐标纬度
     */
    Double latitude;
    /**
     * 用户积分
     */
    Integer userPoints;

    /**
     * 最后取袋子的日期
     */
    LocalDate lastGetBagDate = null;

    /**
     * 最后取袋子弹数量
     */
    Integer receiveBagAmount;

    public Integer increasePoint(Integer point) {
        if(userPoints == null)
        {
            userPoints = 0;
        }
        userPoints += point == null ? 1 : point;
        return userPoints;
    }

    /***
     * 消费积分总额
     */
    Integer consumePoints;

    public Integer consumePoints(int points) {
        if(userPoints == null)
        {
            userPoints = 0;
        }
        if (points > 0 && points <= userPoints) {
            userPoints -= points;
            consumePoints += points;
        }
        return userPoints;
    }

    /**
     * 用户等级
     */
    Integer userLevel;

    public Integer upgradeLevel(Integer plusLevel) {
        if(userLevel == null)
        {
            userLevel = 1;
        }
        userLevel += plusLevel == null ? 1 : plusLevel;
        return userLevel;
    }



    /***
     * 创建日期
     */
    LocalDateTime createDate;
    /***
     * 更新日期
     */
    LocalDateTime updateDate;
    /***
     * 系统账户用户id
     */
    Long sysUserId;

    /**
     * 用户名
     */
    String sysUserName;
}
