package com.tdd.wx.users.api;

import com.tdd.wx.users.entity.WxUser;

import java.util.List;

/***
 * 为微信登录提供微信用户信息查询服务
 * @author loby
 */
public interface WxUserServiceApi{



    /**
     * 检查维修用户是否存在
     * @param open_id 微信open_id
     * @return 是否存在
     */
    boolean isWxUserExist(String open_id);

    /***
     * 查询微信用户
     * @param user_id 系统用户id
     * @return 微信用户对象
     */
    WxUser findBySystemUserId(Long user_id);

    /***
     * 查询微信用户
     * @param open_id open_id
     * @return 微信用户对象
     */
    WxUser findByOpenId(String open_id);


    /***
     * 查询微信用户
     * @param union_id union_id
     * @return 微信用户对象
     */
    WxUser findByUnionId(String union_id);

    /***
     * 查询微信用户
     * @param union_id union_id
     * @return 微信用户对象
     */
    List<WxUser> findByShareUnionId(String union_id);

    /***
     * 查询微信用户
     * @param origin origin
     * @return 微信用户对象
     */
    List<WxUser> findByOrigin(int origin);

    /**
     * 新建微信用户
     * @param user 用户信息
     */
    void save(WxUser user);

    /**
     * 更新微信用户
     * @param user 用户信息
     */
    void update(WxUser user);

    /**
     * 删除微信用户
     * @param open_id 用户id
     */
    void delByOpenId(String open_id);

    /***
     * 返回默认用户信息（cloudplateform)
     * @param wxUser 微信用户
     * @return cloud-platfrom 提供的用户信息
     */
    WxUser getDefaultUser(WxUser wxUser);

    /***
     * 查询所有微信用户
     * @param pageSize 单页数据量
     * @param pageNo 页码，从1开始
     * @param example 过滤example
     * @param order 用用户名排序 0：升序，1：降序
     * @return 用户列表
     */
    List<WxUser> findAll(int pageSize, int pageNo, WxUser example, Integer order);

    /**
     * 用户积分添加
     * @param open_id
     * @param point 添加积分数量，默认1
     * @return 用户积分
     */
    Integer increasePoints(String open_id,Integer point);

    /***
     * 用户消费积分
     * @param open_id
     * @param point 消费积分数量
     * @return 用户积分
     */
    Integer consumePoints(String open_id,Integer point);

    /**
     * 用户升级
     * @param open_id 微信id
     * @param plusLevel 增加的等级，默认为1
     * @return 用户等级
     */
    Integer userUpgrade(String open_id,Integer plusLevel);

    /**
     * 新用户，查找UnionId
     * @param unionId
     *
     * @return WxUser
     */
    WxUser newFindByUnionId(String unionId);

    /**
     * 更新用户SystemUserId
     * @param wxUser
     */
    void updateWxUser(WxUser wxUser);

    /**
     * 查询今日可领取袋子数量
     * @param admCode  广告机code
     *
     * @return 数量
     */
    int getUserBagLeft(String unionId,String admCode);

    /**
     * 记录取袋人的领取数量和时间
     * @param openId
     * @param amount
     */
    void updateReceiveBagAmount(String openId, int amount);
}
