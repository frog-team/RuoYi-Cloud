package com.ruoyi.auth.form;

import lombok.Data;

@Data
public class WxAppLoginBody {

    private String accessToken;

    private String openid;

    private String authCode;
}
