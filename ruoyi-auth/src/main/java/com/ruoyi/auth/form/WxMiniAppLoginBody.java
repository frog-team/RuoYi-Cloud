package com.ruoyi.auth.form;

import lombok.Data;

@Data
public class WxMiniAppLoginBody {

    private String appid;

    private String code;

    private String iv;

    private String datail;

    private String telIv;

    private String telDatail;

    /**
     * 坐标经度
     */
    private String longitude;

    /**
     * 坐标纬度
     */
    private String latitude;

}
