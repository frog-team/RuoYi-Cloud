package com.ruoyi.auth.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @author lobyliang
 */
@ConfigurationProperties(prefix="spring.redis")
@EnableCaching
@Data
public class JedisPoolConfiguration{

//    @Value("${spring.redis.host}")
    private String host;

//    @Value("${spring.redis.port}")
    private int port;

//    @Value("${spring.redis.timeout}")
    private int timeout;

//    @Value("${spring.redis.password}")
    private String password;
    @Bean
    @ConfigurationProperties(prefix="spring.redis.pool")
    public JedisPoolConfig getRedisConfig(){
        JedisPoolConfig config = new JedisPoolConfig();
        return config;
    }

    @Bean
    public JedisPool jedisPool()
    {
        return new JedisPool(getRedisConfig(),
                host,
                port,
                timeout,
                password);
    }
}
