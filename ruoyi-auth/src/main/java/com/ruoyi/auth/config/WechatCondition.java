package com.ruoyi.auth.config;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * @author lobyliang
 */
public class WechatCondition  implements Condition{

    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata){
        Environment environment = context.getEnvironment();
        Boolean enable = environment.getProperty("wechat.enable", Boolean.class);
        String componentAppId = environment.getProperty("wechat.open.componentAppId");
        String componentSecret = environment.getProperty("wechat.open.componentSecret");
        return enable && ! StringUtils.isEmpty(componentAppId) && !StringUtils.isEmpty(componentSecret);
    }
}
