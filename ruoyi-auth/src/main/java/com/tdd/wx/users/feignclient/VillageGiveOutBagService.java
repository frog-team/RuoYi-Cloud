package com.tdd.wx.users.feignclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author lobyliang
 */

@FeignClient(contextId = "VillageGiveOutBagService", value = "basic-data")
public interface VillageGiveOutBagService{
    /**
     * 获取小区默认每日发放袋子数量
     * @param admCode
     *
     * @return
     */
    @GetMapping("/basVillage/defaultBagGiveOutAmount/{admCode}")
    public float getVillageDefaultBagGiveOutAmount(@PathVariable("admCode") String admCode);
}
