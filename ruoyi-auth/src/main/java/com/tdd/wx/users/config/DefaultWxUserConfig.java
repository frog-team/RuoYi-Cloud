package com.tdd.wx.users.config;

import com.tdd.wx.users.entity.WxUser;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

/**
 * @author loby
 */
@Component
@Getter
public class DefaultWxUserConfig{
    @Value("${tdd.wxUser.default.id}")
    Long defaultSystemUserId;
    @Value("${tdd.wxUser.default.username}")
    String defaultSystemUserName;
    @Value("${tdd.wxUser.default.name}")
    String name;
    public WxUser getDefaultUser(WxUser wxUser)
    {
        if(! ObjectUtils.isEmpty(wxUser))
        {
            wxUser.setSysUserId(defaultSystemUserId);
            wxUser.setSysUserName(defaultSystemUserName);
            return wxUser;
        }
        return  null;
    }
}
