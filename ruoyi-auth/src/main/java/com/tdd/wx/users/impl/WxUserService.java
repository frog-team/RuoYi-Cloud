package com.tdd.wx.users.impl;

import com.tdd.wx.users.api.IWxUserService;
import com.tdd.wx.users.api.WxUserServiceApi;
import com.tdd.wx.users.entity.WxUser;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author loby
 */
@DubboService(version = "3.0.0",interfaceClass = WxUserServiceApi.class,interfaceName = "WxUserService")
//@Api(value="微信用户管理Dubbo服务",tags="管理微信用户信息，并验证登录",hidden=false)
@Slf4j
@CacheConfig(cacheNames = "wx-user")
public class WxUserService implements WxUserServiceApi {

    @Autowired
    IWxUserService wxUserService;

    @Override
    public WxUser getDefaultUser(WxUser wxUser) {
        return wxUserService.getDefaultUser(wxUser);
    }

    @Override
    public List<WxUser> findAll(int pageSize, int pageNo, WxUser example, Integer order) {
        return wxUserService.findAll(pageSize, pageNo, example, order);
    }

    @Override
    public boolean isWxUserExist(String open_id) {
        return wxUserService.isWxUserExist(open_id);
    }

    @Override
    public WxUser findBySystemUserId(Long user_id){
        return wxUserService.findBySystemUserId(user_id);
    }

    @Override
    public WxUser findByOpenId(@NotNull String open_id) {
        return wxUserService.findByOpenId(open_id);
    }

    @Override
    public WxUser findByUnionId(String union_id){
        return wxUserService.findByUnionId(union_id);
    }

    @Override
    public List<WxUser> findByShareUnionId(String union_id){
        return findByShareUnionId(union_id);
    }

    @Override
    public List<WxUser> findByOrigin(int origin){
        return wxUserService.findByOrigin(origin);
    }

    @Override
    public Integer increasePoints(String open_id,Integer point)
    {
        return wxUserService.increasePoints(open_id, point);
    }

    @Override
    public Integer consumePoints(String open_id,Integer point)
    {
        return wxUserService.consumePoints(open_id, point);
    }

    @Override
    public Integer userUpgrade(String open_id,Integer plusLevel)
    {
        return wxUserService.userUpgrade(open_id, plusLevel);
    }
    @Override
    public void save(@NotNull WxUser user) {
        wxUserService.save(user);
    }

    @Override
    public void update(@NotNull WxUser user) {
        wxUserService.update(user);
    }

    @Override
    public void delByOpenId(String open_id) {
        wxUserService.delByOpenId(open_id);
    }

    @Override
    public WxUser newFindByUnionId(String unionId){
        return wxUserService.newFindByUnionId(unionId);
    }

    @Override
    public void updateWxUser(WxUser wxUser){
        wxUserService.updateWxUser(wxUser);
    }

    @Override
    public int getUserBagLeft(String unionId,String admCode)
    {
        return wxUserService.getUserBagLeft(unionId,admCode);
    }

    @Override
    public void updateReceiveBagAmount(String unionId, int amount)
    {
        wxUserService.updateReceiveBagAmount(unionId,amount);
    }

}
