package com.ruoyi.sms.service;

import cn.hutool.json.JSONUtil;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.teaopenapi.models.Config;
import com.ruoyi.common.core.constant.AliyunSmsTemplateCode;
import com.ruoyi.sms.config.AliyunSmsConfig;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Primary
@Service
@Log4j2
public class AliyunSmsServiceImpl implements ISmsService{

    @Autowired
    private AliyunSmsConfig aliyunSmsConfig;

//    /**
//     * 访问若依后台的路径
//     */
//    @Value("access.path")
//    private String accessPath;

    public com.aliyun.dysmsapi20170525.Client createClient(String accessKeyId,String accessKeySecret,String endpoint) throws Exception {
        Config config = new Config()
                // 您的AccessKey ID
                .setAccessKeyId(accessKeyId)
                // 您的AccessKey Secret
                .setAccessKeySecret(accessKeySecret);
        // 访问的域名
        config.endpoint = endpoint;
        return new com.aliyun.dysmsapi20170525.Client(config);
    }

    @Override
    public String sendSms(String tel,String templateCode,Map templateParam){
        // 工具类获取值
        String endpoint = aliyunSmsConfig.getEndpoint();
        String accessKeyId = aliyunSmsConfig.getAccessKeyId();
        String accessKeySecret = aliyunSmsConfig.getAccessKeySecret();
        String signName = aliyunSmsConfig.getSignName();
        try {
            com.aliyun.dysmsapi20170525.Client client = createClient(accessKeyId,accessKeySecret,endpoint);
            SendSmsRequest sendSmsRequest = new SendSmsRequest();
            sendSmsRequest.setSignName(signName);
            sendSmsRequest.setPhoneNumbers(tel);
            sendSmsRequest.setTemplateCode(templateCode);
            sendSmsRequest.setTemplateParam(JSONUtil.toJsonStr(templateParam));
            SendSmsResponse response = client.sendSms(sendSmsRequest);
            if("OK".equals(response.getBody().getCode())){
                //成功
                return response.getBody().getMessage();
            }else {
                return response.getBody().getCode();
            }
        }catch (Exception e){
            e.printStackTrace();
            return "发送失败！";
        }
    }


    @Override
    public boolean sendVerificationCode(String tel, String code){
        Map<String,String> param = new HashMap<>();
        param.put("code",code);
        String res = sendSms(tel, AliyunSmsTemplateCode.VERIFICATION_CODE,param);
        if("OK".equals(res)){
            return true;
        }else {
            log.error(res);
            return false;
        }
    }

    /**
     * 向物业公司发送  后台管理系统账号
     * @param tel 手机号
     * @param username 用户名
     * @param password 密码
     * @return
     */
    @Override
    public boolean sendRealtySystemAccount(String tel, String username, String password) {
        Map<String,String> param = new HashMap<>();
        param.put("username",username);
        param.put("password",password);
//        param.put("accessPath",accessPath);
        String res = sendSms(tel, AliyunSmsTemplateCode.REALTY_SYSTEM_ACCOUNT,param);
        if("OK".equals(res)){
            return true;
        }else {
            log.error(res);
            return false;
        }
    }

}
