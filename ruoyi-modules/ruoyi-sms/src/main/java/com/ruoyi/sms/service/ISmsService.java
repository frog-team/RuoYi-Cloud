package com.ruoyi.sms.service;

import java.util.Map;

public interface ISmsService {

    public String sendSms(String tel, String templateCode, Map templateParam);

    boolean sendVerificationCode(String tel, String code);

    /**
     * 向物业公司发送  后台管理系统账号
     * @param tel 手机号
     * @param username 用户名
     * @param password 密码
     * @return
     */
    boolean sendRealtySystemAccount(String tel,String username, String password);
}
