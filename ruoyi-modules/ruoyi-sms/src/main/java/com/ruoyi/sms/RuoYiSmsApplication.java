package com.ruoyi.sms;

import com.ruoyi.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@EnableCustomSwagger2
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class RuoYiSmsApplication {

    public static void main(String[] args) {
        SpringApplication.run(RuoYiSmsApplication.class, args);
    }

}
