package com.ruoyi.app.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.api.domain.app.MaintainerEmployeesVillage;

/**
 * 服务商-员工-小区关联Service接口
 *
 * @author lobyliang
 * @date 2022-01-23
 */
public interface IMaintainerEmployeesVillageService extends IService<MaintainerEmployeesVillage>
{
    /**
     * 查询服务商-员工-小区关联
     *
     * @param id 服务商-员工-小区关联主键
     * @return 服务商-员工-小区关联
     */
    public MaintainerEmployeesVillage selectMaintainerEmployeesVillageById(Long id);

    /**
     * 查询服务商-员工-小区关联列表
     *
     * @param maintainerEmployeesVillage 服务商-员工-小区关联
     * @return 服务商-员工-小区关联集合
     */
    public List<MaintainerEmployeesVillage> selectMaintainerEmployeesVillageList(MaintainerEmployeesVillage maintainerEmployeesVillage);

    /**
     * 新增服务商-员工-小区关联
     *
     * @param maintainerEmployeesVillage 服务商-员工-小区关联
     * @return 结果
     */
    public int insertMaintainerEmployeesVillage(MaintainerEmployeesVillage maintainerEmployeesVillage);

    /**
     * 修改服务商-员工-小区关联
     *
     * @param maintainerEmployeesVillage 服务商-员工-小区关联
     * @return 结果
     */
    public int updateMaintainerEmployeesVillage(MaintainerEmployeesVillage maintainerEmployeesVillage);

    /**
     * 批量删除服务商-员工-小区关联
     *
     * @param ids 需要删除的服务商-员工-小区关联主键集合
     * @return 结果
     */
    public int deleteMaintainerEmployeesVillageByIds(Long[] ids);

    /**
     * 删除服务商-员工-小区关联信息
     *
     * @param id 服务商-员工-小区关联主键
     * @return 结果
     */
    public int deleteMaintainerEmployeesVillageById(Long id);

    List<MaintainerEmployeesVillage> findByMaintainerUID(Long uid);

    List<MaintainerEmployeesVillage> findByEmployeesUnionid(String unionid);

    List<MaintainerEmployeesVillage> findEUidAndMid(String eUnionid, Long mUid);

    MaintainerEmployeesVillage getInfo(String eUnionid, Long mUid, String villageCode);

    void save(String employeesUnionid, Long maintainerUserId, String villageCodes);

    Page employeesList(Page page, Long maintainerUserId);

    MaintainerEmployeesVillage findByMEV(Long mid, String eid, String vCode);


    /**
     * songping 2022-04-07
     * 基于 员工的uid 查询当前 服务商员工的信息
     * @param empUnionId 员工的uid
     * @return
     */
    MaintainerEmployeesVillage findMaintainerIdByEmpUnionId(String empUnionId);
}
