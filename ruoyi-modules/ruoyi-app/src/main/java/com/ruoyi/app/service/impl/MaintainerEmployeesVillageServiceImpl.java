package com.ruoyi.app.service.impl;

import java.util.*;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.system.api.domain.app.MaintainerEmployeesVillage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.MaintainerEmployeesVillageMapper;
import com.ruoyi.app.service.IMaintainerEmployeesVillageService;

/**
 * 服务商-员工-小区关联Service业务层处理
 *
 * @author lobyliang
 * @date 2022-01-23
 */
@Service
@DS("tdddb")
public class MaintainerEmployeesVillageServiceImpl extends ServiceImpl<MaintainerEmployeesVillageMapper,MaintainerEmployeesVillage> implements IMaintainerEmployeesVillageService
{
    @Autowired
    private MaintainerEmployeesVillageMapper maintainerEmployeesVillageMapper;

    /**
     * 查询服务商-员工-小区关联
     *
     * @param id 服务商-员工-小区关联主键
     * @return 服务商-员工-小区关联
     */
    @Override
    public MaintainerEmployeesVillage selectMaintainerEmployeesVillageById(Long id)
    {
        return maintainerEmployeesVillageMapper.selectMaintainerEmployeesVillageById(id);
    }

    /**
     * 查询服务商-员工-小区关联列表
     *
     * @param maintainerEmployeesVillage 服务商-员工-小区关联
     * @return 服务商-员工-小区关联
     */
    @Override
    public List<MaintainerEmployeesVillage> selectMaintainerEmployeesVillageList(MaintainerEmployeesVillage maintainerEmployeesVillage)
    {
        return maintainerEmployeesVillageMapper.selectMaintainerEmployeesVillageList(maintainerEmployeesVillage);
    }

    /**
     * 新增服务商-员工-小区关联
     *
     * @param maintainerEmployeesVillage 服务商-员工-小区关联
     * @return 结果
     */
    @Override
    public int insertMaintainerEmployeesVillage(MaintainerEmployeesVillage maintainerEmployeesVillage)
    {
        return maintainerEmployeesVillageMapper.insertMaintainerEmployeesVillage(maintainerEmployeesVillage);
    }

    /**
     * 修改服务商-员工-小区关联
     *
     * @param maintainerEmployeesVillage 服务商-员工-小区关联
     * @return 结果
     */
    @Override
    public int updateMaintainerEmployeesVillage(MaintainerEmployeesVillage maintainerEmployeesVillage)
    {
        return maintainerEmployeesVillageMapper.updateMaintainerEmployeesVillage(maintainerEmployeesVillage);
    }

    /**
     * 批量删除服务商-员工-小区关联
     *
     * @param ids 需要删除的服务商-员工-小区关联主键
     * @return 结果
     */
    @Override
    public int deleteMaintainerEmployeesVillageByIds(Long[] ids)
    {
        return maintainerEmployeesVillageMapper.deleteMaintainerEmployeesVillageByIds(ids);
    }

    /**
     * 删除服务商-员工-小区关联信息
     *
     * @param id 服务商-员工-小区关联主键
     * @return 结果
     */
    @Override
    public int deleteMaintainerEmployeesVillageById(Long id)
    {
        return maintainerEmployeesVillageMapper.deleteMaintainerEmployeesVillageById(id);
    }

    @Override
    public List<MaintainerEmployeesVillage> findByMaintainerUID(Long uid){
        LambdaQueryWrapper<MaintainerEmployeesVillage> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(MaintainerEmployeesVillage::getMaintainerUserId,uid);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public List<MaintainerEmployeesVillage> findByEmployeesUnionid(String unionid){
        LambdaQueryWrapper<MaintainerEmployeesVillage> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(MaintainerEmployeesVillage::getEmployeesUnionid,unionid);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public List<MaintainerEmployeesVillage> findEUidAndMid(String eUnionid, Long mUid){
        LambdaQueryWrapper<MaintainerEmployeesVillage> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(MaintainerEmployeesVillage::getEmployeesUnionid,eUnionid);
        wrapper.eq(MaintainerEmployeesVillage::getMaintainerUserId,mUid);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public MaintainerEmployeesVillage getInfo(String eUnionid, Long mUid, String villageCode){
        LambdaQueryWrapper<MaintainerEmployeesVillage> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(MaintainerEmployeesVillage::getEmployeesUnionid,eUnionid);
        wrapper.eq(MaintainerEmployeesVillage::getMaintainerUserId,mUid);
        if(StrUtil.isEmpty(villageCode)){
            wrapper.isNull(MaintainerEmployeesVillage::getVillageCode);
        }else {
            wrapper.eq(MaintainerEmployeesVillage::getVillageCode,villageCode);
        }
        return baseMapper.selectOne(wrapper);
    }


    @Override
    public void save(String employeesUnionid, Long maintainerUserId, String villageCodes){
        if(StrUtil.isEmpty(villageCodes)){
            if(getInfo(employeesUnionid,maintainerUserId,null) == null){
                MaintainerEmployeesVillage r = new MaintainerEmployeesVillage();
                r.setEmployeesUnionid(employeesUnionid);
                r.setMaintainerUserId(maintainerUserId);
                r.setCreateTime(DateTime.now());
                baseMapper.insert(r);
            }
        }else {
            List<String> villages = Arrays.asList(villageCodes.split(","));
            for (String c : villages) {
                if(getInfo(employeesUnionid,maintainerUserId,c) == null){
                    MaintainerEmployeesVillage r = new MaintainerEmployeesVillage();
                    r.setEmployeesUnionid(employeesUnionid);
                    r.setMaintainerUserId(maintainerUserId);
                    r.setVillageCode(c);
                    r.setCreateTime(DateTime.now());
                    baseMapper.insert(r);
                }
            }
        }
    }

    @Override
    public Page employeesList(Page page, Long maintainerUserId){
        List<Map> employeess = baseMapper.employeesIdList(page,maintainerUserId);
        employeess.forEach(m->{
            String eid = MapUtil.getStr(m,"employeesUnionid",null);
            if(StrUtil.isNotEmpty(eid)){
                m.put("villageInfo",this.findByEmployeesUnionid(eid));
            }
        });
        page.setRecords(employeess);
        return page;
    }

    @Override
    public MaintainerEmployeesVillage findByMEV(Long mid,String eid,String vCode){
        LambdaQueryWrapper<MaintainerEmployeesVillage> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(MaintainerEmployeesVillage::getMaintainerUserId,mid);
        wrapper.eq(MaintainerEmployeesVillage::getEmployeesUnionid,eid);
        wrapper.eq(MaintainerEmployeesVillage::getVillageCode,vCode);
        return baseMapper.selectOne(wrapper);
    }

    /**
     * songping 2022-04-07
     * @param empUnionId 员工的uid
     * @return
     */
    @Override
    public MaintainerEmployeesVillage findMaintainerIdByEmpUnionId(String empUnionId) {
        LambdaQueryWrapper<MaintainerEmployeesVillage> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(MaintainerEmployeesVillage::getEmployeesUnionid,empUnionId);
        return baseMapper.selectOne(wrapper);
    }

}
