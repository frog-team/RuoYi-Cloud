package com.ruoyi.app;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import com.ruoyi.common.security.annotation.EnableCustomConfig;
import com.ruoyi.common.security.annotation.EnableRyFeignClients;
import com.ruoyi.common.swagger.annotation.EnableCustomSwagger2;
import com.ruoyi.system.api.RemoteLogService;
import com.ruoyi.system.api.RemoteRealNameService;
import com.ruoyi.system.api.RemoteWithdrawalsRecordService;
import com.tdd.adm.service.feign.IncomeFeignClient;
import com.tdd.adm.service.feign.MonitorServerFeignClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients(clients = {RemoteLogService.class,IncomeFeignClient.class, MonitorServerFeignClient.class,RemoteRealNameService.class,RemoteWithdrawalsRecordService.class})
@SpringBootApplication(exclude = DruidDataSourceAutoConfigure.class)
@ComponentScan(basePackages = {"com.tdd.adm.service.feign.*","com.ruoyi.*"})
public class RuoYiAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(RuoYiAppApplication.class, args);
    }

}
