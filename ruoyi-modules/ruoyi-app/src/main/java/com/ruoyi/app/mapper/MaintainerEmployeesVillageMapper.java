package com.ruoyi.app.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.system.api.domain.app.MaintainerEmployeesVillage;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 服务商-员工-小区关联Mapper接口
 *
 * @author lobyliang
 * @date 2022-01-23
 */
public interface MaintainerEmployeesVillageMapper extends BaseMapper<MaintainerEmployeesVillage>
{
    /**
     * 查询服务商-员工-小区关联
     *
     * @param id 服务商-员工-小区关联主键
     * @return 服务商-员工-小区关联
     */
    public MaintainerEmployeesVillage selectMaintainerEmployeesVillageById(Long id);

    /**
     * 查询服务商-员工-小区关联列表
     *
     * @param maintainerEmployeesVillage 服务商-员工-小区关联
     * @return 服务商-员工-小区关联集合
     */
    public List<MaintainerEmployeesVillage> selectMaintainerEmployeesVillageList(MaintainerEmployeesVillage maintainerEmployeesVillage);

    /**
     * 新增服务商-员工-小区关联
     *
     * @param maintainerEmployeesVillage 服务商-员工-小区关联
     * @return 结果
     */
    public int insertMaintainerEmployeesVillage(MaintainerEmployeesVillage maintainerEmployeesVillage);

    /**
     * 修改服务商-员工-小区关联
     *
     * @param maintainerEmployeesVillage 服务商-员工-小区关联
     * @return 结果
     */
    public int updateMaintainerEmployeesVillage(MaintainerEmployeesVillage maintainerEmployeesVillage);

    /**
     * 删除服务商-员工-小区关联
     *
     * @param id 服务商-员工-小区关联主键
     * @return 结果
     */
    public int deleteMaintainerEmployeesVillageById(Long id);

    /**
     * 批量删除服务商-员工-小区关联
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMaintainerEmployeesVillageByIds(Long[] ids);

    @Select("SELECT employees_unionid as employeesUnionid,MIN(create_time) as createTime from maintainer_employees_village WHERE maintainer_user_id = #{maintainerId} GROUP BY employees_unionid")
    List<Map> employeesIdList(Page page, @Param("maintainerId") Long maintainerId);
}
