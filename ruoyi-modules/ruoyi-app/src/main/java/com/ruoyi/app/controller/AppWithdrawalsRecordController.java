package com.ruoyi.app.controller;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.system.api.RemoteWithdrawalsRecordService;
import com.ruoyi.system.api.domain.base.AccountInfo;
import com.ruoyi.system.api.domain.base.BankInfo;
import com.ruoyi.system.api.domain.base.WithdrawalsRecord;
import com.tdd.wx.users.api.WxUserServiceApi;
import com.tdd.wx.users.entity.WxUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("appWithdrawalsRecord")
@Api(tags = "提现记录")
public class AppWithdrawalsRecordController {

    @Autowired
    private RemoteWithdrawalsRecordService remoteWithdrawalsRecordService;

//    @DubboReference(version = "3.0.0" , interfaceClass = WxUserServiceApi.class, interfaceName = "WxUserService" , lazy = true, check = false)
//    private WxUserServiceApi wxUserServiceApi;

    @ApiOperation("查看余额")
    @GetMapping("getBalance")
    public R<AccountInfo> getBalance(){
        String unionid = SecurityUtils.getUnionId();
        R<AccountInfo> res = remoteWithdrawalsRecordService.getBalance(unionid, SecurityConstants.INNER);
        return res;
    }


    @ApiOperation("申请提现")
    @PostMapping("applyCash")
    public R applyCash(@RequestBody WithdrawalsRecord withdrawalsRecord){
        String unionid = SecurityUtils.getUnionId();
        withdrawalsRecord.setUnionid(unionid);
        return remoteWithdrawalsRecordService.applyCash(withdrawalsRecord,SecurityConstants.INNER);
    }

    @ApiOperation("银行列表")
    @GetMapping("bankList")
    public R<List<BankInfo>> bankList(){
        return remoteWithdrawalsRecordService.bankList(SecurityConstants.INNER);
    }

    @ApiOperation("查看自己提现记录")
    @GetMapping("selfListPage")
    public R<Page<WithdrawalsRecord>> selfListPage(Page<WithdrawalsRecord> page){
        String unionid = SecurityUtils.getUnionId();
        return remoteWithdrawalsRecordService.selfListPage(page,unionid,SecurityConstants.INNER);
    }


}

