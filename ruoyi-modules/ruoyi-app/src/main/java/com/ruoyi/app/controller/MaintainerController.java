package com.ruoyi.app.controller;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.HexUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.utils.ShareCodeUtil;
import com.ruoyi.common.redis.service.RedisService;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.system.api.RemoteRealNameService;
import com.ruoyi.system.api.model.RealNameAuthInfo;
import com.tdd.wx.users.api.WxUserServiceApi;
import com.tdd.wx.users.entity.WxUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.GET;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("maintainer")
@Api(tags = "服务商信息")
public class MaintainerController {

    @DubboReference(version = "3.0.0" , interfaceClass = WxUserServiceApi.class, interfaceName = "WxUserService" , lazy = true, check = false)
    private WxUserServiceApi wxUserServiceApi;
    @Autowired
    private RedisService redisService;
    @Autowired
    private RemoteRealNameService remoteRealNameService;

    @ApiOperation("获取当前用户信息")
    @GetMapping("selfInfo")
    public R selfInfo(){
        String unionid = SecurityUtils.getUnionId();
        if(StrUtil.isEmpty(unionid)){
            return R.fail(401,"请先登录！");
        }
        WxUser wxUser = wxUserServiceApi.findByUnionId(unionid);
        Map rtMap = new HashMap<>();
        try {
            R<RealNameAuthInfo> res = remoteRealNameService.findByUnionIdAndType(wxUser.getUnionId(),10L, SecurityConstants.INNER);
            RealNameAuthInfo realNameAuthInfo = res.getData();
            rtMap.put("realNameAuthInfo",realNameAuthInfo);
        }catch (Exception e){
            rtMap.put("realNameAuthInfo",null);
        }
        rtMap.put("userInfo",wxUser);
        rtMap.put("userType","employees");
        if(wxUser.getSysUserId() != null){
            String inviteCode = HexUtil.toHex(wxUser.getSysUserId());
            if(inviteCode.length() < 5){
                for (int i=0;i<5;i++){
                    inviteCode = "0"+inviteCode;
                    if(inviteCode.length()>=5){
                        break;
                    }
                }
            }
            rtMap.put("inviteCode", inviteCode);
        }

        if(wxUser != null && wxUser.getSysUserId() != null){
            rtMap.put("userType","maintainer");
        }
        return R.ok(rtMap);
    }

    @ApiOperation("获取邀请码")
    @GetMapping("getShareCode")
    public R getShareCode(){
        String unionid = SecurityUtils.getUnionId();
        WxUser wxUser = wxUserServiceApi.findByUnionId(unionid);
        if(StrUtil.isEmpty(unionid)){
            return R.fail(401,"请先登录！");
        }
        //获取原code
        String oldCode = redisService.getCacheObject("maintainer_unionid_"+unionid);
        if(!StrUtil.isEmpty(oldCode)){
            redisService.deleteObject("auth_code_"+oldCode);
            redisService.deleteObject("maintainer_unionid_"+unionid);
        }
        String newCode = ShareCodeUtil.generateShareCode(IdUtil.simpleUUID());
        redisService.setCacheObject("maintainer_unionid_"+unionid,newCode,24L, TimeUnit.HOURS);
        redisService.setCacheObject("auth_code_"+newCode,wxUser.getSysUserId(),24L, TimeUnit.HOURS);
        return R.ok(newCode);
    }

}
