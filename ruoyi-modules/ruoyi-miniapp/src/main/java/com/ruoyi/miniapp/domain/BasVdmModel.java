package com.ruoyi.miniapp.domain;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.springframework.data.annotation.Id;

/**
 * 广告机型号对象 bas_vdm_model
 *
 * @author lobyliang
 * @date 2022-01-14
 */
@ApiModel("bas_vdm_model(广告机型号)")
@Data
@ToString
public class BasVdmModel {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty("主键")
    @TableId(value = "vdm_model_id" , type = IdType.AUTO)
    private Integer vdmModelId;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    @Excel(name = "名称")
    private String vdmModelName;

    /**
     * 高度cm
     */
    @ApiModelProperty("高度cm")
    @Excel(name = "高度cm")
    private Float height;

    /**
     * 宽度cm
     */
    @ApiModelProperty("宽度cm")
    @Excel(name = "宽度cm")
    private Float width;

    /**
     * 深度cm
     */
    @ApiModelProperty("深度cm")
    @Excel(name = "深度cm")
    private Float depth;

    /**
     * 出袋口数量
     */
    @ApiModelProperty("出袋口数量")
    @Excel(name = "出袋口数量")
    private String bagOutletAmount;

    /**
     * 屏幕大小,英寸，多屏幕，以最大的为准
     */
    @ApiModelProperty("屏幕大小,英寸，多屏幕，以最大的为准")
    @Excel(name = "屏幕大小,英寸，多屏幕，以最大的为准")
    private Float screenSize;

    /**
     * 垃圾袋容量
     */
    @ApiModelProperty("垃圾袋容量")
    @Excel(name = "垃圾袋容量")
    private Integer bagCapacity;

    /**
     * 安装场景\n0：小区柜机\n1：室内台机\n2：室外柜机\n3：商场柜机
     */
    @ApiModelProperty("安装场景\n0：小区柜机\n1：室内台机\n2：室外柜机\n3：商场柜机")
    @Excel(name = "安装场景\n0：小区柜机\n1：室内台机\n2：室外柜机\n3：商场柜机")
    private Integer fkSceneNo;

    /**
     * 用电功率
     */
    @ApiModelProperty("用电功率")
    @Excel(name = "用电功率")
    private Integer ePower;

    /**
     * 最新固件版本号
     */
    @ApiModelProperty("最新固件版本号")
    @Excel(name = "最新固件版本号")
    private String lastFirmwareVersion;

    /**
     * 备注说明
     */
    @ApiModelProperty("备注说明")
    @Excel(name = "备注说明")
    private String memo;

    /**
     * 广告机可承受袋子最大宽度
     */
    @ApiModelProperty("广告机可承受袋子最大宽度")
    @Excel(name = "广告机可承受袋子最大宽度")
    private BigDecimal sackWidth;

    /**
     * 价格
     */
    @ApiModelProperty("价格")
    @Excel(name = "价格")
    private BigDecimal admPrice;

    /**
     * 规格，小程序展示用
     */
    @ApiModelProperty("规格，小程序展示用")
    @Excel(name = "规格，小程序展示用")
    private String specification;

    /**
     * 简介
     */
    @ApiModelProperty("简介")
    @Excel(name = "简介")
    private String instruction;

    /**
     * 封面图片
     */
    @ApiModelProperty("封面图片")
    @Excel(name = "封面图片")
    private String poster;

}
