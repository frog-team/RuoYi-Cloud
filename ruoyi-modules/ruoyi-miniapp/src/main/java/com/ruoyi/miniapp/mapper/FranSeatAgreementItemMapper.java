package com.ruoyi.miniapp.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.miniapp.domain.FraFranchisees;
import com.ruoyi.miniapp.domain.FranSeatAgreementItem;

/**
 * 加盟商购买点位订单明细Mapper接口
 *
 * @author lobyliang
 * @date 2022-01-14
 */
public interface FranSeatAgreementItemMapper extends BaseMapper<FranSeatAgreementItem> {
    /**
     * 查询加盟商购买点位订单明细
     *
     * @param pkFranSeatItemId 加盟商购买点位订单明细主键
     * @return 加盟商购买点位订单明细
     */
    public FranSeatAgreementItem selectFranSeatAgreementItemByPkFranSeatItemId(Long pkFranSeatItemId);

    /**
     * 查询加盟商购买点位订单明细列表
     *
     * @param franSeatAgreementItem 加盟商购买点位订单明细
     * @return 加盟商购买点位订单明细集合
     */
    public List<FranSeatAgreementItem> selectFranSeatAgreementItemList(FranSeatAgreementItem franSeatAgreementItem);

    /**
     * 新增加盟商购买点位订单明细
     *
     * @param franSeatAgreementItem 加盟商购买点位订单明细
     * @return 结果
     */
    public int insertFranSeatAgreementItem(FranSeatAgreementItem franSeatAgreementItem);

    /**
     * 修改加盟商购买点位订单明细
     *
     * @param franSeatAgreementItem 加盟商购买点位订单明细
     * @return 结果
     */
    public int updateFranSeatAgreementItem(FranSeatAgreementItem franSeatAgreementItem);

    /**
     * 删除加盟商购买点位订单明细
     *
     * @param pkFranSeatItemId 加盟商购买点位订单明细主键
     * @return 结果
     */
    public int deleteFranSeatAgreementItemByPkFranSeatItemId(Long pkFranSeatItemId);

    /**
     * 批量删除加盟商购买点位订单明细
     *
     * @param pkFranSeatItemIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFranSeatAgreementItemByPkFranSeatItemIds(Long[] pkFranSeatItemIds);
}
