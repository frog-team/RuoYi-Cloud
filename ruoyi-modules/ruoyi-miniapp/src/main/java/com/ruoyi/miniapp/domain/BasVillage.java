package com.ruoyi.miniapp.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.ruoyi.common.core.web.domain.BaseEntity;

import java.util.List;

/**
 * 小区信息对象 bas_village
 *
 * @author lobyliang
 * @date 2022-01-12
 */
@ApiModel("bas_village(小区信息)")
@Data
@ToString
public class BasVillage {
    private static final long serialVersionUID = 1L;

    /**
     * 小区编码：9位街道码+类型码2位+3位顺序码
     */
    @ApiModelProperty("小区编码：9位街道码+类型码2位+3位顺序码")
    private String pkVillageCode;

    /**
     * 小区名称
     */
    @ApiModelProperty("小区名称")
    @Excel(name = "小区名称")
    private String villageName;

    /**
     * 所属行政区划街道编码
     */
    @ApiModelProperty("所属行政区划街道编码")
    @Excel(name = "所属行政区划街道编码")
    private String fkDivisionCode;

    /**
     * 首写字母快速查询编码
     */
    @ApiModelProperty("首写字母快速查询编码")
    @Excel(name = "首写字母快速查询编码")
    private String alphabetCode;

    /**
     * 开发商
     */
    @ApiModelProperty("开发商")
    @Excel(name = "开发商")
    private String developers;

    /**
     * 物业公司
     */
    @ApiModelProperty("物业公司")
    @Excel(name = "物业公司")
    private String propertyCompnay;

    /**
     * 物业公司组织code
     */
    @ApiModelProperty("物业公司组织code")
    @Excel(name = "物业公司组织code")
    private String fkPropertyCompCode;

    /**
     * 小区类型
     */
    @ApiModelProperty("小区类型")
    @Excel(name = "小区类型")
    private Integer dtVillageTypeNo;

    /**
     * 楼栋数量
     */
    @ApiModelProperty("楼栋数量")
    @Excel(name = "楼栋数量")
    private Integer buildingAmount;

    /**
     * 居民数量
     */
    @ApiModelProperty("居民数量")
    @Excel(name = "居民数量")
    private Integer residentAmount;

    /**
     * 小区地址
     */
    @ApiModelProperty("小区地址")
    @Excel(name = "小区地址")
    private String address;

    /**
     * 服务商id
     */
    @ApiModelProperty("服务商id")
    @Excel(name = "服务商id")
    private Long fkMaintainerId;

    /**
     * 小区中心经度
     */
    @ApiModelProperty("小区中心经度")
    @Excel(name = "小区中心经度")
    private Double centerLongitude;

    /**
     * 小区中心维度
     */
    @ApiModelProperty("小区中心维度")
    @Excel(name = "小区中心维度")
    private Double centerLatitude;

    /**
     * 小区边界，采用json文件的形式，描述小区大概的边界。\n或将小区边界存储在资源管理模块中，这里仅仅保持一个url
     */
    @ApiModelProperty("小区边界，采用json文件的形式，描述小区大概的边界。\n或将小区边界存储在资源管理模块中，这里仅仅保持一个url")
    @Excel(name = "小区边界，采用json文件的形式，描述小区大概的边界。\n或将小区边界存储在资源管理模块中，这里仅仅保持一个url")
    private String boundary;

    /**
     * 开发商统一信用代码
     */
    @ApiModelProperty("开发商统一信用代码")
    @Excel(name = "开发商统一信用代码")
    private String fkDeveloperCode;

    /**
     * 入住率
     */
    @ApiModelProperty("入住率")
    @Excel(name = "入住率")
    private Float occupancyRate;

    /**
     * 聚居区
     */
    @ApiModelProperty("聚居区")
    @Excel(name = "聚居区")
    private Long fkColonryDistrictId;

    /**
     * 商圈id
     */
    @ApiModelProperty("商圈id")
    @Excel(name = "商圈id")
    private Long fkBusinessDistId;

    /**
     * 日平均出袋数量
     */
    @ApiModelProperty("日平均出袋数量")
    @Excel(name = "日平均出袋数量")
    private String avgBagPerDay;

    /**
     * 垃圾袋折扣
     */
    @ApiModelProperty("垃圾袋折扣")
    @Excel(name = "垃圾袋折扣")
    private Float bagDiscount;

    /**
     * 屏幕广告折扣
     */
    @ApiModelProperty("屏幕广告折扣")
    @Excel(name = "屏幕广告折扣")
    private Float screenDiscount;

    /**
     * 广告机数量
     */
    @ApiModelProperty("广告机数量")
    @Excel(name = "广告机数量")
    private Integer admAmount;

    /**
     * 袋身广告拼单成功后的折扣
     */
    @ApiModelProperty("袋身广告拼单成功后的折扣")
    @Excel(name = "袋身广告拼单成功后的折扣")
    private Float jointDiscount;

    @TableField(exist = false)
    @ApiModelProperty("未安装数量")
    private Integer notInstallCount;

    @TableField(exist = false)
    @ApiModelProperty("所有机位数量")
    private Integer allSeatCount;

    @ApiModelProperty("楼栋信息")
    @TableField(exist = false)
    private List<BasBuildings> buildingsList;

    @TableField(exist = false)
    private Integer hasCheckNum;

}
