package com.ruoyi.miniapp.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import cn.hutool.core.util.StrUtil;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.miniapp.domain.AdmSeats;
import com.ruoyi.miniapp.mapper.AdmSeatsMapper;
import com.ruoyi.miniapp.service.IAdmSeatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 广告机机位,锁定机位操作在redis中进行，Service业务层处理
 *
 * @author lobyliang
 * @date 2022-01-11
 */
@Service
@DS("tdddb")
public class AdmSeatsServiceImpl extends ServiceImpl<AdmSeatsMapper, AdmSeats> implements IAdmSeatsService {

    /**
     * 查询广告机机位,锁定机位操作在redis中进行，
     *
     * @param pkSeatCode 广告机机位,锁定机位操作在redis中进行，主键
     * @return 广告机机位, 锁定机位操作在redis中进行，
     */
    @Override
    public AdmSeats selectAdmSeatsByPkSeatCode(String pkSeatCode) {
        return baseMapper.selectAdmSeatsByPkSeatCode(pkSeatCode);
    }

    /**
     * 查询广告机机位,锁定机位操作在redis中进行，列表
     *
     * @param admSeats 广告机机位,锁定机位操作在redis中进行，
     * @return 广告机机位, 锁定机位操作在redis中进行，
     */
    @Override
    public List<AdmSeats> selectAdmSeatsList(AdmSeats admSeats) {
        return baseMapper.selectAdmSeatsList(admSeats);
    }

    /**
     * 新增广告机机位,锁定机位操作在redis中进行，
     *
     * @param admSeats 广告机机位,锁定机位操作在redis中进行，
     * @return 结果
     */
    @Override
    public int insertAdmSeats(AdmSeats admSeats) {
        return baseMapper.insertAdmSeats(admSeats);
    }

    /**
     * 修改广告机机位,锁定机位操作在redis中进行，
     *
     * @param admSeats 广告机机位,锁定机位操作在redis中进行，
     * @return 结果
     */
    @Override
    public int updateAdmSeats(AdmSeats admSeats) {
        return baseMapper.updateAdmSeats(admSeats);
    }

    /**
     * 批量删除广告机机位,锁定机位操作在redis中进行，
     *
     * @param pkSeatCodes 需要删除的广告机机位,锁定机位操作在redis中进行，主键
     * @return 结果
     */
    @Override
    public int deleteAdmSeatsByPkSeatCodes(String[] pkSeatCodes) {
        return baseMapper.deleteAdmSeatsByPkSeatCodes(pkSeatCodes);
    }

    /**
     * 删除广告机机位,锁定机位操作在redis中进行，信息
     *
     * @param pkSeatCode 广告机机位,锁定机位操作在redis中进行，主键
     * @return 结果
     */
    @Override
    public int deleteAdmSeatsByPkSeatCode(String pkSeatCode) {
        return baseMapper.deleteAdmSeatsByPkSeatCode(pkSeatCode);
    }

    /**
     * 查询小区机位数量
     *
     * @param villageCode 小区编码
     * @param isEmpty     是否有机器
     * @return
     */
    @Override
    public Integer getVillageNum(String villageCode, Integer isEmpty) {
        LambdaQueryWrapper<AdmSeats> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(AdmSeats::getFkVillageCode, villageCode);
        if (isEmpty != null) {
            wrapper.eq(AdmSeats::getIsEmpty, isEmpty);
        }
        return baseMapper.selectCount(wrapper).intValue();
    }

    @Override
    public List<AdmSeats> findByBuildingCode(String code) {
        LambdaQueryWrapper<AdmSeats> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(AdmSeats::getFkBuildingCode, code);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public List<AdmSeats> findByCodes(List<String> codes) {
        LambdaQueryWrapper<AdmSeats> wrapper = Wrappers.lambdaQuery();
        wrapper.in(AdmSeats::getPkSeatCode, codes);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public void updateFranchiseeIdByCodes(List<String> codes, Long franchiseeId) {
        LambdaUpdateWrapper<AdmSeats> wrapper = Wrappers.lambdaUpdate();
        wrapper.in(AdmSeats::getPkSeatCode, codes);
        wrapper.set(AdmSeats::getFkFranchiseeId, franchiseeId);
        baseMapper.update(null, wrapper);
    }

    @Override
    public void updateFranchiseeIdByCodes(String code, Long franchiseeId) {
        LambdaUpdateWrapper<AdmSeats> wrapper = Wrappers.lambdaUpdate();
        wrapper.eq(AdmSeats::getPkSeatCode, code);
        wrapper.set(AdmSeats::getFkFranchiseeId, franchiseeId);
        baseMapper.update(null, wrapper);
    }

    /**
     * 更新机位占用状态
     * @param code
     * @param status
     */
    @Override
    public void updateSeatEmpt(String code,Integer status){
        if(status == null){
            return;
        }
        LambdaUpdateWrapper<AdmSeats> wrapper = Wrappers.lambdaUpdate();
        wrapper.eq(AdmSeats::getPkSeatCode,code);
        wrapper.set(AdmSeats::getIsEmpty,status);
        baseMapper.update(null,wrapper);
    }

    @Override
    public Integer getAdmCount(String villageCode, Integer hasBuy){
        LambdaQueryWrapper<AdmSeats> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(StrUtil.isNotEmpty(villageCode),AdmSeats::getFkVillageCode,villageCode);
        if(hasBuy != null){
            if(hasBuy == 1){
                wrapper.isNotNull(AdmSeats::getFkFranchiseeId);
            }else{
                wrapper.isNull(AdmSeats::getFkFranchiseeId);
            }
        }
        return baseMapper.selectCount(wrapper).intValue();
    }

    @Override
    public List<String> villageAdmSeatsCodes(String code){
        LambdaQueryWrapper<AdmSeats> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(AdmSeats::getFkVillageCode,code);
        return baseMapper.selectList(wrapper).stream().map(AdmSeats::getPkSeatCode).collect(Collectors.toList());
    }

    /**
     * 依据 adm_seats 主键补全 投资人分润金额
     * @param code 机位主键
     * @param franPricePerBag 投资人分润金额
     * @author songping  时间 2022-03-22
     */
    @Override
    public void updateFranPricePerBagByCode(String code, BigDecimal franPricePerBag) {
        LambdaUpdateWrapper<AdmSeats> wrapper = Wrappers.lambdaUpdate();
        wrapper.eq(AdmSeats::getPkSeatCode, code);
        wrapper.set(AdmSeats::getFranPricePerBag, franPricePerBag);
        baseMapper.update(null, wrapper);
    }

}
