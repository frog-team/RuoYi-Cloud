package com.ruoyi.miniapp.dto;

import com.ruoyi.system.api.model.RealNameAuthInfo;
import lombok.Data;

@Data
public class RealNameDto extends RealNameAuthInfo {

    private String code;

}
