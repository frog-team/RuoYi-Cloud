package com.ruoyi.miniapp.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.miniapp.domain.AdmProcPlan;

/**
 * 生产计划(基于投资商设备购买)Service接口
 *
 * @author lobyliang
 * @date 2022-01-14
 */
public interface IAdmProcPlanService extends IService<AdmProcPlan> {
    /**
     * 查询生产计划(基于投资商设备购买)
     *
     * @param pkAdmProcCode 生产计划(基于投资商设备购买)主键
     * @return 生产计划(基于投资商设备购买)
     */
    public AdmProcPlan selectAdmProcPlanByPkAdmProcCode(String pkAdmProcCode);

    /**
     * 查询生产计划(基于投资商设备购买)列表
     *
     * @param admProcPlan 生产计划(基于投资商设备购买)
     * @return 生产计划(基于投资商设备购买)集合
     */
    public List<AdmProcPlan> selectAdmProcPlanList(AdmProcPlan admProcPlan);

    /**
     * 新增生产计划(基于投资商设备购买)
     *
     * @param admProcPlan 生产计划(基于投资商设备购买)
     * @return 结果
     */
    public int insertAdmProcPlan(AdmProcPlan admProcPlan);

    /**
     * 修改生产计划(基于投资商设备购买)
     *
     * @param admProcPlan 生产计划(基于投资商设备购买)
     * @return 结果
     */
    public int updateAdmProcPlan(AdmProcPlan admProcPlan);

    /**
     * 批量删除生产计划(基于投资商设备购买)
     *
     * @param pkAdmProcCodes 需要删除的生产计划(基于投资商设备购买)主键集合
     * @return 结果
     */
    public int deleteAdmProcPlanByPkAdmProcCodes(String[] pkAdmProcCodes);

    /**
     * 删除生产计划(基于投资商设备购买)信息
     *
     * @param pkAdmProcCode 生产计划(基于投资商设备购买)主键
     * @return 结果
     */
    public int deleteAdmProcPlanByPkAdmProcCode(String pkAdmProcCode);

    AdmProcPlan findBySeatOrderCode(String fkSeatOrderCode);
}
