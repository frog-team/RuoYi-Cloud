package com.ruoyi.miniapp.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.miniapp.domain.BasVillage;

import java.util.List;

/**
 * 小区信息Service接口
 *
 * @author lobyliang
 * @date 2022-01-12
 */
public interface IBasVillageService extends IService<BasVillage> {
    /**
     * 查询小区信息
     *
     * @param pkVillageCode 小区信息主键
     * @return 小区信息
     */
    public BasVillage selectBasVillageByPkVillageCode(String pkVillageCode);

    /**
     * 查询小区信息列表
     *
     * @param basVillage 小区信息
     * @return 小区信息集合
     */
    public List<BasVillage> selectBasVillageList(BasVillage basVillage);

    /**
     * 新增小区信息
     *
     * @param basVillage 小区信息
     * @return 结果
     */
    public int insertBasVillage(BasVillage basVillage);

    /**
     * 修改小区信息
     *
     * @param basVillage 小区信息
     * @return 结果
     */
    public int updateBasVillage(BasVillage basVillage);

    /**
     * 批量删除小区信息
     *
     * @param pkVillageCodes 需要删除的小区信息主键集合
     * @return 结果
     */
    public int deleteBasVillageByPkVillageCodes(String[] pkVillageCodes);

    /**
     * 删除小区信息信息
     *
     * @param pkVillageCode 小区信息主键
     * @return 结果
     */
    public int deleteBasVillageByPkVillageCode(String pkVillageCode);

    Page<BasVillage> listPage(Page<BasVillage> page, String name);

    BasVillage getBasVillageInfo(String code);

    BasVillage getByCode(String code);

    List<BasVillage> findByCodes(List<String> codes);

    List<BasVillage> maintainerVillages(Long maintainerId);

    List<BasVillage> findByBasVillageCodes(List<String> codes);
}
