package com.ruoyi.miniapp.mongo.repository;

import com.tdd.wx.users.entity.WxUser;
import com.tdd.wx.users.entity.WxUserPointRecord;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @author loby
 */
@Repository
public interface WxUserPointRecordRepository extends MongoRepository<WxUserPointRecord, String> {
}
