package com.ruoyi.miniapp.service;

import com.ruoyi.miniapp.domain.DiscountsScope;

import java.util.List;

public interface IDiscountsScopeService {

    public List<DiscountsScope> getDiscountsScopeBytype(Integer type);

    public DiscountsScope getDiscountsScopeByid(Integer id);
}
