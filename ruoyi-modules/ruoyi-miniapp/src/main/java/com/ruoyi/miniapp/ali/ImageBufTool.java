package com.ruoyi.miniapp.ali;

import com.freewayso.image.combiner.enums.OutputFormat;
import com.ruoyi.miniapp.service.IBufferdImageService;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.DecimalFormat;

/**
 * @author lobyliang
 */
public class ImageBufTool
{

    public static IBufferdImageService bufferdImageService;
    public static Float getFileSizeWithM(long size)
    {
        DecimalFormat decimalFormat = new DecimalFormat("#.000");
        String fileSizeString = "";
        fileSizeString = decimalFormat.format((double) size / 1024.f/1024.f);
        return Float.parseFloat(fileSizeString);
    }

    public static OutputFormat getImageOutPutFormat(String suffix)
    {
        OutputFormat oFormat = OutputFormat.PNG;
        if(suffix != null){
            try{
                oFormat = OutputFormat.valueOf(suffix.toUpperCase());
            }catch(Exception e){
            }
        }
        return oFormat;
    }


    public static BufferedImage getBufferImage(String src)
    {
        if(bufferdImageService!=null)
        {
           return bufferdImageService.getBufferImage(src);
        }
        return null;
    }

    public static void writeToFile(InputStream is, String fileName) throws IOException
    {
        BufferedInputStream in = null;
        BufferedOutputStream out = null;
        in = new BufferedInputStream(is);
        out = new BufferedOutputStream(new FileOutputStream(fileName));
        int len = -1;
        byte[] b = new byte[1024];
        while((len = in.read(b)) != -1)
        {
            out.write(b, 0, len);
        }
        in.close();
        out.close();
    }
}
