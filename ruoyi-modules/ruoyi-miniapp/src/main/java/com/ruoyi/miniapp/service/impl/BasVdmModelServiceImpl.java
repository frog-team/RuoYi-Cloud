package com.ruoyi.miniapp.service.impl;

import java.util.List;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.miniapp.mapper.BasVdmModelMapper;
import com.ruoyi.miniapp.domain.BasVdmModel;
import com.ruoyi.miniapp.service.IBasVdmModelService;

/**
 * 广告机型号Service业务层处理
 *
 * @author lobyliang
 * @date 2022-01-14
 */
@Service
@DS("tdddb")
public class BasVdmModelServiceImpl extends ServiceImpl<BasVdmModelMapper, BasVdmModel> implements IBasVdmModelService {
    @Autowired
    private BasVdmModelMapper basVdmModelMapper;

    /**
     * 查询广告机型号
     *
     * @param vdmModelId 广告机型号主键
     * @return 广告机型号
     */
    @Override
    public BasVdmModel selectBasVdmModelByVdmModelId(String vdmModelId) {
        return basVdmModelMapper.selectBasVdmModelByVdmModelId(vdmModelId);
    }

    /**
     * 查询广告机型号列表
     *
     * @param basVdmModel 广告机型号
     * @return 广告机型号
     */
    @Override
    public List<BasVdmModel> selectBasVdmModelList(BasVdmModel basVdmModel) {
        return basVdmModelMapper.selectBasVdmModelList(basVdmModel);
    }

    /**
     * 新增广告机型号
     *
     * @param basVdmModel 广告机型号
     * @return 结果
     */
    @Override
    public int insertBasVdmModel(BasVdmModel basVdmModel) {
        return basVdmModelMapper.insertBasVdmModel(basVdmModel);
    }

    /**
     * 修改广告机型号
     *
     * @param basVdmModel 广告机型号
     * @return 结果
     */
    @Override
    public int updateBasVdmModel(BasVdmModel basVdmModel) {
        return basVdmModelMapper.updateBasVdmModel(basVdmModel);
    }

    /**
     * 批量删除广告机型号
     *
     * @param vdmModelIds 需要删除的广告机型号主键
     * @return 结果
     */
    @Override
    public int deleteBasVdmModelByVdmModelIds(String[] vdmModelIds) {
        return basVdmModelMapper.deleteBasVdmModelByVdmModelIds(vdmModelIds);
    }

    /**
     * 删除广告机型号信息
     *
     * @param vdmModelId 广告机型号主键
     * @return 结果
     */
    @Override
    public int deleteBasVdmModelByVdmModelId(String vdmModelId) {
        return basVdmModelMapper.deleteBasVdmModelByVdmModelId(vdmModelId);
    }

}
