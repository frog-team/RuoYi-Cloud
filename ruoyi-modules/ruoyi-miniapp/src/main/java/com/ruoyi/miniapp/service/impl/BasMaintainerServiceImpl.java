package com.ruoyi.miniapp.service.impl;

import java.util.List;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.miniapp.mapper.BasMaintainerMapper;
import com.ruoyi.miniapp.domain.BasMaintainer;
import com.ruoyi.miniapp.service.IBasMaintainerService;

/**
 * 服务商Service业务层处理
 *
 * @author lobyliang
 * @date 2022-01-21
 */
@Service
@DS("tdddb")
public class BasMaintainerServiceImpl extends ServiceImpl<BasMaintainerMapper,BasMaintainer> implements IBasMaintainerService
{
    @Autowired
    private BasMaintainerMapper basMaintainerMapper;

    /**
     * 查询服务商
     *
     * @param userId 服务商主键
     * @return 服务商
     */
    @Override
    public BasMaintainer selectBasMaintainerByUserId(Long userId)
    {
        return basMaintainerMapper.selectBasMaintainerByUserId(userId);
    }

    /**
     * 查询服务商列表
     *
     * @param basMaintainer 服务商
     * @return 服务商
     */
    @Override
    public List<BasMaintainer> selectBasMaintainerList(BasMaintainer basMaintainer)
    {
        return basMaintainerMapper.selectBasMaintainerList(basMaintainer);
    }

    /**
     * 新增服务商
     *
     * @param basMaintainer 服务商
     * @return 结果
     */
    @Override
    public int insertBasMaintainer(BasMaintainer basMaintainer)
    {
        return basMaintainerMapper.insertBasMaintainer(basMaintainer);
    }

    /**
     * 修改服务商
     *
     * @param basMaintainer 服务商
     * @return 结果
     */
    @Override
    public int updateBasMaintainer(BasMaintainer basMaintainer)
    {
        return basMaintainerMapper.updateBasMaintainer(basMaintainer);
    }

    /**
     * 批量删除服务商
     *
     * @param userIds 需要删除的服务商主键
     * @return 结果
     */
    @Override
    public int deleteBasMaintainerByUserIds(Long[] userIds)
    {
        return basMaintainerMapper.deleteBasMaintainerByUserIds(userIds);
    }

    /**
     * 删除服务商信息
     *
     * @param userId 服务商主键
     * @return 结果
     */
    @Override
    public int deleteBasMaintainerByUserId(Long userId)
    {
        return basMaintainerMapper.deleteBasMaintainerByUserId(userId);
    }
}
