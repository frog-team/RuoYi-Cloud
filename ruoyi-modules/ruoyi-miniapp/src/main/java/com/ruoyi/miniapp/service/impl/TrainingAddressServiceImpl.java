package com.ruoyi.miniapp.service.impl;

import com.github.wxiaoqi.security.common.tool.ToolPager;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.miniapp.domain.TrainingAddress;
import com.ruoyi.miniapp.dto.ArgumentsDto;
import com.ruoyi.miniapp.dto.TrainingAddressDto;
import com.ruoyi.miniapp.mapper.TrainingAddressMapper;
import com.ruoyi.miniapp.service.IRealNameAuthInfoService;
import com.ruoyi.miniapp.service.ITrainingAddressService;
import com.ruoyi.system.api.model.RealNameAuthInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 表名：TRAINING_ADDRESS 	 说明：培训地址 
 */

@Service
public class TrainingAddressServiceImpl implements ITrainingAddressService {

	private Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	private TrainingAddressMapper trainingAddressMapper;

	@Autowired
	private IRealNameAuthInfoService realNameAuthInfoService;

	/**
	 *  查询培训地址
	 * @return
	 */
	@Override
	public List<TrainingAddress> getTrainingAddressInfoList(String divisionCode) {
		List<TrainingAddress> maintainerApplyInfo = trainingAddressMapper.getTrainingAddressInfo(divisionCode);
		if(maintainerApplyInfo == null){
			return new ArrayList<>(0);
		}
		return maintainerApplyInfo;
	}

	/**
	 *  查询培训地址 单条
	 * @return
	 */
	@Override
	public AjaxResult getTrainingAddressInfoOne(String divisionCode) {
		List<TrainingAddress> maintainerApplyInfo = trainingAddressMapper.getTrainingAddressInfo(divisionCode);
		return AjaxResult.success(maintainerApplyInfo);
	}

	/**
	 *  查询培训场次
	 * @return
	 */
	@Override
	public AjaxResult getTrainingAddressInfoBytrainingName(String trainingName) {
		List<TrainingAddress> trainingAddresses = trainingAddressMapper.getTrainingAddressInfoBytrainingName(trainingName);
		for (TrainingAddress trainingAddress : trainingAddresses) {
			//判断是否已到截止日期
			if (LocalDateTime.now().isAfter(trainingAddress.getAddressEndDate())){
				trainingAddress.setIsCutOff(0);
			}else {
				trainingAddress.setIsCutOff(1);
			}
		}
		return AjaxResult.success(trainingAddresses);
	}

	/**
	 *  返回用户报名信息
	 * @return
	 */
	@Override
	public AjaxResult getUserInfo(Integer id) {
		Long userId = SecurityUtils.getUserId();
		//查询实名表
		RealNameAuthInfo realNameAuthInfo = realNameAuthInfoService.selRealNameAuth(userId);
		//查询场次信息
		TrainingAddressDto trainingAddressByKey = trainingAddressMapper.getTrainingAddressByKey(id);
		trainingAddressByKey.setUserName(realNameAuthInfo.getRealName());
		trainingAddressByKey.setUserPhone(realNameAuthInfo.getTel());
		return AjaxResult.success(trainingAddressByKey);
	}
	/**
	 *  运维后台查询市
	 * @return
	 */
	@Override
	public List<TrainingAddress> getCityList() {
		List<TrainingAddress> trainingcities = trainingAddressMapper.selCityList();
		if(trainingcities == null){
			return new ArrayList<>(0);
		}
		return trainingcities;
	}

	/**
	 *  运维后台查询市
	 * @return
	 */
	@Override
	public AjaxResult selCityList() {
		List<TrainingAddress> trainingAddresses = trainingAddressMapper.selCityList();
		return AjaxResult.success(trainingAddresses);
	}

	/**
	 * 运维后台
	 * #根据市级编码查询下面有多少培训基地
	 */
	@Override
	public AjaxResult selTrainingNameList(String fkDivisionCode) {
		//查询下面培训基地
		List<String> trainingName = trainingAddressMapper.selTrainingNameList(fkDivisionCode);
		List<ArgumentsDto> argumentsDtos=new ArrayList<>();
		for (String str : trainingName) {
			ArgumentsDto dto=new ArgumentsDto();
			List<TrainingAddress> trainingAddresses = trainingAddressMapper.selTrainingAddressList(str);
			dto.setTrainingName(str);
			dto.setTrainingAddresses(trainingAddresses);
			argumentsDtos.add(dto);
//			argumentsDtos.forEach(p -> {p.setTrainingName(str); p.setTrainingAddresses(trainingAddresses);});
		}
		return AjaxResult.success(argumentsDtos);
	}
	/**
	 * 运维后台
	 * # 根据培训基地名称查询场次
	 * @return
	 */
	@Override
	public AjaxResult selTrainingAddressList(String teainingName) {
		List<TrainingAddress> trainingAddresses = trainingAddressMapper.selTrainingAddressList(teainingName);
		return AjaxResult.success(trainingAddresses);
	}


	/**
	 *  批量修改
	 *  返回报名用户id
	 * @return
	 */
	@Override
	public AjaxResult selStudentList(Integer id) {
		List<Long> StudentList = new ArrayList<>();
		List<Long> selStudentListA = trainingAddressMapper.selStudentListA(id);
		List<Long> selStudentListB = trainingAddressMapper.selStudentListB(id);
		StudentList.addAll(selStudentListA);
		StudentList.addAll(selStudentListB);
		return AjaxResult.success(StudentList);
	}


	/**
	 *  获取培训基地场次（次数）  没用
	 * @return
	 */
	@Override
	public String getTrainingCountBytrainingName(String trainingName) {
		Integer countBytrainingName = trainingAddressMapper.getTrainingCountBytrainingName(trainingName);
		String count = String.valueOf(countBytrainingName);
		if (countBytrainingName < 9){
			count = "0" + count;
		}
		return count;
	}

	/**
	 *  剩余名额+ -
	 */
	@Override
	public void updateSurplusPlacesA(Integer id) {
		trainingAddressMapper.updateSurplusPlacesA(id);
	}
	/**
	 *  剩余名额+ -
	 */
	@Override
	public void updateSurplusPlacesB(Integer id) {
		trainingAddressMapper.updateSurplusPlacesB(id);
	}


	/**
	 * 增加
	 */
	@Override
	public int insertTrainingAddress(TrainingAddress obj){
		int count = trainingAddressMapper.insertTrainingAddress(obj);
		logger.info("TRAINING_ADDRESS表插入"+count+"条数据;KEY="+obj.getId());
		return count;
	}

	/**
	 * 查询count
	 */
	@Override
	public int getTrainingAddressCount(TrainingAddress obj){
		int count = trainingAddressMapper.getTrainingAddressCount(obj);
		return count;
	}

	/**
	 * 查询List
	 */
	@Override
	public List<TrainingAddress> getTrainingAddressList(TrainingAddress obj){
		List<TrainingAddress> objList = trainingAddressMapper.getTrainingAddressList(obj);
		return objList;
	}

	/**
	 * 分页查询
	 * @return
	 */
	@Override
	public AjaxResult getPageTrainingAddress(TrainingAddress obj){
		if(null == obj.getCurrentPage() || obj.getCurrentPage() <= 0 ){
			obj.setCurrentPage(1);
		}
		if(null == obj.getPageSize() || obj.getPageSize() <= 0 ){
			obj.setPageSize(ToolPager.DEFAULT_PAGE_SIZE);
		}
		int count = trainingAddressMapper.getTrainingAddressCount(obj);
		List<TrainingAddress> objList = trainingAddressMapper.getTrainingAddressList(obj);
		return AjaxResult.success(new TableDataInfo(objList,count));
	}

	/**
	 * 主键查询
	 */
	@Override
	public TrainingAddressDto getTrainingAddressByKey(Integer key) {
		return trainingAddressMapper.getTrainingAddressByKey(key);
	}

	/**
	 * 多主键查询
	 */
	@Override
	public List<TrainingAddress> getTrainingAddressByKeys(List<Long> key) {
		return trainingAddressMapper.getTrainingAddressByKeys(key);
	}

	/**
	 * 修改
	 */
	@Override
	public int updateTrainingAddress(TrainingAddress obj){
		int count = trainingAddressMapper.updateTrainingAddress(obj);
		return count;
	}

	/**
	 * 删除
	 * @return
	 */
	@Override
	public AjaxResult deleteTrainingAddress(Integer key){
		List<Long> selStudentListA = trainingAddressMapper.selStudentListA(key);
		List<Long> selStudentListB = trainingAddressMapper.selStudentListB(key);
		if (selStudentListA.size() > 0 || selStudentListB.size() > 0){
			return AjaxResult.error("抱歉,该培训地址无法删除！！！");
		}else {
			trainingAddressMapper.deleteTrainingAddress(key);
			return AjaxResult.success("删除成功！");
		}
	}

	/**
	 * 批量增加
	 */
	@Override
	public int batchInsertTrainingAddress(List<TrainingAddress> objList){
		int count = trainingAddressMapper.batchInsertTrainingAddress(objList);
		logger.info("TRAINING_ADDRESS成功插入"+count+"条数据");
		return count;
	}

	/**
	 * 批量修改
	 */
	@Override
	public int batchUpdateTrainingAddress(List<TrainingAddress> objList){
		int count = trainingAddressMapper.batchUpdateTrainingAddress(objList);
		logger.info("TRAINING_ADDRESS成功更新"+count+"条数据");
		return count;
	}

	/**
	 * 批量删除
	 */
	@Override
	public int batchDeleteTrainingAddress(List<Long> keys){
		int count = trainingAddressMapper.batchDeleteTrainingAddress(keys);
		logger.info("TRAINING_ADDRESS成功删除"+count+"条数据");
		return count;
	}

}