package com.ruoyi.miniapp.service.impl;

import java.util.List;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.miniapp.domain.AdmSeats;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.miniapp.mapper.FranSeatAgreementItemMapper;
import com.ruoyi.miniapp.domain.FranSeatAgreementItem;
import com.ruoyi.miniapp.service.IFranSeatAgreementItemService;

/**
 * 加盟商购买点位订单明细Service业务层处理
 *
 * @author lobyliang
 * @date 2022-01-14
 */
@Service
@DS("tdddb")
public class FranSeatAgreementItemServiceImpl extends ServiceImpl<FranSeatAgreementItemMapper, FranSeatAgreementItem> implements IFranSeatAgreementItemService {
    @Autowired
    private FranSeatAgreementItemMapper franSeatAgreementItemMapper;

    /**
     * 查询加盟商购买点位订单明细
     *
     * @param pkFranSeatItemId 加盟商购买点位订单明细主键
     * @return 加盟商购买点位订单明细
     */
    @Override
    public FranSeatAgreementItem selectFranSeatAgreementItemByPkFranSeatItemId(Long pkFranSeatItemId) {
        return franSeatAgreementItemMapper.selectFranSeatAgreementItemByPkFranSeatItemId(pkFranSeatItemId);
    }

    /**
     * 查询加盟商购买点位订单明细列表
     *
     * @param franSeatAgreementItem 加盟商购买点位订单明细
     * @return 加盟商购买点位订单明细
     */
    @Override
    public List<FranSeatAgreementItem> selectFranSeatAgreementItemList(FranSeatAgreementItem franSeatAgreementItem) {
        return franSeatAgreementItemMapper.selectFranSeatAgreementItemList(franSeatAgreementItem);
    }

    /**
     * 新增加盟商购买点位订单明细
     *
     * @param franSeatAgreementItem 加盟商购买点位订单明细
     * @return 结果
     */
    @Override
    public int insertFranSeatAgreementItem(FranSeatAgreementItem franSeatAgreementItem) {
        return franSeatAgreementItemMapper.insertFranSeatAgreementItem(franSeatAgreementItem);
    }

    /**
     * 修改加盟商购买点位订单明细
     *
     * @param franSeatAgreementItem 加盟商购买点位订单明细
     * @return 结果
     */
    @Override
    public int updateFranSeatAgreementItem(FranSeatAgreementItem franSeatAgreementItem) {
        return franSeatAgreementItemMapper.updateFranSeatAgreementItem(franSeatAgreementItem);
    }

    /**
     * 批量删除加盟商购买点位订单明细
     *
     * @param pkFranSeatItemIds 需要删除的加盟商购买点位订单明细主键
     * @return 结果
     */
    @Override
    public int deleteFranSeatAgreementItemByPkFranSeatItemIds(Long[] pkFranSeatItemIds) {
        return franSeatAgreementItemMapper.deleteFranSeatAgreementItemByPkFranSeatItemIds(pkFranSeatItemIds);
    }

    /**
     * 删除加盟商购买点位订单明细信息
     *
     * @param pkFranSeatItemId 加盟商购买点位订单明细主键
     * @return 结果
     */
    @Override
    public int deleteFranSeatAgreementItemByPkFranSeatItemId(Long pkFranSeatItemId) {
        return franSeatAgreementItemMapper.deleteFranSeatAgreementItemByPkFranSeatItemId(pkFranSeatItemId);
    }

    @Override
    public List<FranSeatAgreementItem> findBySeatCode(String seatCode) {
        LambdaQueryWrapper<FranSeatAgreementItem> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(FranSeatAgreementItem::getFkSeatCode, seatCode);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public void updPlacementInfoByFranSeatItemId(Long pkFranSeatItemId, String placementInfo) {
        LambdaUpdateWrapper<FranSeatAgreementItem> wrapper = Wrappers.lambdaUpdate();
        wrapper.eq(FranSeatAgreementItem::getPkFranSeatItemId, pkFranSeatItemId);
        wrapper.set(FranSeatAgreementItem::getPlacementInfo, placementInfo);
        baseMapper.update(null, wrapper);
    }
}
