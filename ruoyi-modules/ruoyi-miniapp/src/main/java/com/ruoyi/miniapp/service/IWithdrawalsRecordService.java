package com.ruoyi.miniapp.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.api.domain.base.WithdrawalsRecord;

public interface IWithdrawalsRecordService extends IService<WithdrawalsRecord> {
    Page<WithdrawalsRecord> listPage(Page<WithdrawalsRecord> page, Long userId);
}
