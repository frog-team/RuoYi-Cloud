package com.ruoyi.miniapp.service;


import com.ruoyi.miniapp.domain.ResItem;

import java.util.List;

/**
 * 资源条目Service接口
 *
 * @author lobyliang
 * @date 2022-01-08
 */
public interface IResItemService
{
    /**
     * 查询资源条目
     *
     * @param pkResId 资源条目主键
     * @return 资源条目
     */
    public ResItem selectResItemByPkResId(String pkResId);

    /**
     * 查询资源条目列表
     * @param checkStatus
     * @param offsetEnd
     * @param offsetStart
     * @param resTypeId
     * @return 资源条目集合
     */
    public List<ResItem> selectResItemList(Integer checkStatus,Long resTypeId,Integer offsetStart,Integer offsetEnd);

    /**
     * 查询资源条目列表 条数
     * @param resTypeId
     * @param checkStatus
     * @return
     */
    public Integer selectItemListCount(Integer checkStatus,Long resTypeId);

    /**
     * 查询资源OSS外网访问地址
     * @param pkResId  pkKey
     *
     * @return OSS外网访问地址
     */
    public String getResUrl(String pkResId);

    /**
     * 新增资源条目
     *
     * @param resItem 资源条目
     * @return 结果
     */
    public int insertResItem(ResItem resItem);

    /**
     * 修改资源条目
     *
     * @param resItem 资源条目
     * @return 结果
     */
    public int updateResItem(ResItem resItem);

    /**
     * 批量删除资源条目
     *
     * @param pkResIds 需要删除的资源条目主键集合
     * @return 结果
     */
    public int deleteResItemByPkResIds(String[] pkResIds);

    /**
     * 删除资源条目信息
     *
     * @param pkResId 资源条目主键
     * @return 结果
     */
    public int deleteResItemByPkResId(String pkResId);

    /**
     * 软删除资源
     * @param pkResId  id
     *
     * @return 结果
     */
    public int softDelete(String pkResId);

    /**
     * 获取我的资源列表
     * @param checkStatus 资源状态
     * @param pkResIds
     * @param ownerUId
     * @param offsetStart
     * @param offsetEnd
     * @return
     */
    public List<ResItem> selectResItemByResType(Integer checkStatus,List<String> pkResIds,long ownerUId,Integer offsetStart,Integer offsetEnd);

    /**
     * 查询我的资源列表条数
     * @param checkStatus
     * @param pkResIds
     * @param ownerUId
     * @return
     */
    public Integer selectResItemByResTypeCount(Integer checkStatus,List<String> pkResIds,long ownerUId);

}
