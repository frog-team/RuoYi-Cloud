package com.ruoyi.miniapp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.miniapp.domain.AdmVillageStock;

import java.util.List;

/**
 * 广告机小区库存信息Service接口
 *
 * @author lobyliang
 * @date 2022-01-12
 */
public interface IAdmVillageStockService extends IService<AdmVillageStock> {
    /**
     * 查询广告机小区库存信息
     *
     * @param pkStockId 广告机小区库存信息主键
     * @return 广告机小区库存信息
     */
    public AdmVillageStock selectAdmVillageStockByPkStockId(Long pkStockId);

    /**
     * 查询广告机小区库存信息列表
     *
     * @param admVillageStock 广告机小区库存信息
     * @return 广告机小区库存信息集合
     */
    public List<AdmVillageStock> selectAdmVillageStockList(AdmVillageStock admVillageStock);

    /**
     * 新增广告机小区库存信息
     *
     * @param admVillageStock 广告机小区库存信息
     * @return 结果
     */
    public int insertAdmVillageStock(AdmVillageStock admVillageStock);

    /**
     * 修改广告机小区库存信息
     *
     * @param admVillageStock 广告机小区库存信息
     * @return 结果
     */
    public int updateAdmVillageStock(AdmVillageStock admVillageStock);

    /**
     * 批量删除广告机小区库存信息
     *
     * @param pkStockIds 需要删除的广告机小区库存信息主键集合
     * @return 结果
     */
    public int deleteAdmVillageStockByPkStockIds(Long[] pkStockIds);

    /**
     * 删除广告机小区库存信息信息
     *
     * @param pkStockId 广告机小区库存信息主键
     * @return 结果
     */
    public int deleteAdmVillageStockByPkStockId(Long pkStockId);
}
