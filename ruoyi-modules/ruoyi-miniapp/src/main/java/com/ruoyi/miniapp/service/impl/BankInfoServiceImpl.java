package com.ruoyi.miniapp.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.miniapp.mapper.BankInfoMapper;
import com.ruoyi.miniapp.service.IBankInfoService;
import com.ruoyi.system.api.domain.base.BankInfo;
import org.springframework.stereotype.Service;

@Service
@DS("tdddb")
public class BankInfoServiceImpl extends ServiceImpl<BankInfoMapper, BankInfo> implements IBankInfoService {
}
