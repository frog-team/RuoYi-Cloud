package com.ruoyi.miniapp.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.ruoyi.miniapp.domain.DiscountsScope;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
@DS("tdddb")
public interface TsalesmanMapper {


    public int insertTsalesman(DiscountsScope obj);
}
