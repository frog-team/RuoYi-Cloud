package com.ruoyi.miniapp.service.impl;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import cn.hutool.core.util.StrUtil;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.redis.service.RedisService;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.miniapp.domain.BasBuildings;
import com.ruoyi.miniapp.domain.BasVillage;
import com.ruoyi.miniapp.mapper.BasVillageMapper;
import com.ruoyi.miniapp.service.IAdmSeatsService;
import com.ruoyi.miniapp.service.IBasBuildingsService;
import com.ruoyi.miniapp.service.IBasVillageService;
import com.ruoyi.miniapp.service.ITCodeAdministrativeDivisionService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

/**
 * 小区信息Service业务层处理
 *
 * @author lobyliang
 * @date 2022-01-12
 */
@Service
@DS("tdddb")
public class BasVillageServiceImpl extends ServiceImpl<BasVillageMapper, BasVillage> implements IBasVillageService {
    @Autowired
    private ITCodeAdministrativeDivisionService codeAdministrativeDivisionService;
    @Autowired
    private IAdmSeatsService iAdmSeatsService;
    @Autowired
    private IBasBuildingsService buildingsService;


    /**
     * 查询小区信息
     *
     * @param pkVillageCode 小区信息主键
     * @return 小区信息
     */
    @Override
    public BasVillage selectBasVillageByPkVillageCode(String pkVillageCode) {
        return baseMapper.selectBasVillageByPkVillageCode(pkVillageCode);
    }

    /**
     * 查询小区信息列表
     *
     * @param basVillage 小区信息
     * @return 小区信息
     */
    @Override
    public List<BasVillage> selectBasVillageList(BasVillage basVillage) {
        return baseMapper.selectBasVillageList(basVillage);
    }

    /**
     * 新增小区信息
     *
     * @param basVillage 小区信息
     * @return 结果
     */
    @Override
    public int insertBasVillage(BasVillage basVillage) {
        return baseMapper.insertBasVillage(basVillage);
    }

    /**
     * 修改小区信息
     *
     * @param basVillage 小区信息
     * @return 结果
     */
    @Override
    public int updateBasVillage(BasVillage basVillage) {
        return baseMapper.updateBasVillage(basVillage);
    }

    /**
     * 批量删除小区信息
     *
     * @param pkVillageCodes 需要删除的小区信息主键
     * @return 结果
     */
    @Override
    public int deleteBasVillageByPkVillageCodes(String[] pkVillageCodes) {
        return baseMapper.deleteBasVillageByPkVillageCodes(pkVillageCodes);
    }

    /**
     * 删除小区信息信息
     *
     * @param pkVillageCode 小区信息主键
     * @return 结果
     */
    @Override
    public int deleteBasVillageByPkVillageCode(String pkVillageCode) {
        return baseMapper.deleteBasVillageByPkVillageCode(pkVillageCode);
    }

    @Override
    public Page<BasVillage> listPage(Page<BasVillage> page, String name) {
        QueryWrapper<BasVillage> wrapper = Wrappers.query();
        wrapper.like(StrUtil.isNotEmpty(name), "village_name", name);
        wrapper.last("ORDER BY (SELECT count(*) from adm_seats where fk_village_code = bas_village.pk_village_code and is_empty = 1) DESC");
        baseMapper.selectPage(page, wrapper);
        page.getRecords().forEach(r -> {
            //机位信息
            r.setNotInstallCount(iAdmSeatsService.getVillageNum(r.getPkVillageCode(), 1));
            r.setBuildingsList(buildingsService.findByVillageCode(r.getPkVillageCode()));
//            r.setNotInstallCount(iAdmSeatsService.getAdmCount(r.getPkVillageCode(),0));
            r.setAllSeatCount(iAdmSeatsService.getAdmCount(r.getPkVillageCode(),null));

        });
        return page;
    }

    @Override
    public BasVillage getBasVillageInfo(String code) {
        BasVillage info = this.getByCode(code);
        //楼栋信息
        info.setBuildingsList(buildingsService.findByVillageCode(info.getPkVillageCode()));
        return info;
    }

    @Override
    public BasVillage getByCode(String code) {
        LambdaQueryWrapper<BasVillage> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(BasVillage::getPkVillageCode, code);
        return baseMapper.selectOne(wrapper);
    }

    @Override
    public List<BasVillage> findByCodes(List<String> codes) {
        LambdaQueryWrapper<BasVillage> wrapper = Wrappers.lambdaQuery();
        wrapper.in(BasVillage::getPkVillageCode, codes);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public List<BasVillage> maintainerVillages(Long maintainerId){
        LambdaQueryWrapper<BasVillage> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(BasVillage::getFkMaintainerId,maintainerId);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public List<BasVillage> findByBasVillageCodes(List<String> codes){
        LambdaQueryWrapper<BasVillage> wrapper = Wrappers.lambdaQuery();
        if(codes.size()<=0){
            return new ArrayList<>();
        }
        wrapper.in(BasVillage::getPkVillageCode,codes);
        return baseMapper.selectList(wrapper);
    }

}
