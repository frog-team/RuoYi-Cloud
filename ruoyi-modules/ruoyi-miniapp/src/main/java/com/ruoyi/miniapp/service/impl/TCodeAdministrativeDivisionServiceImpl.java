package com.ruoyi.miniapp.service.impl;

import java.util.List;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.miniapp.domain.TCodeAdministrativeDivision;
import com.ruoyi.miniapp.mapper.TCodeAdministrativeDivisionMapper;
import com.ruoyi.miniapp.service.ITCodeAdministrativeDivisionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 行政区划代码GB-T2260-2017Service业务层处理
 *
 * @author lobyliang
 * @date 2022-01-12
 */
@Service
@DS("tdddb")
public class TCodeAdministrativeDivisionServiceImpl extends ServiceImpl<TCodeAdministrativeDivisionMapper, TCodeAdministrativeDivision> implements ITCodeAdministrativeDivisionService {

    /**
     * 查询行政区划代码GB-T2260-2017
     *
     * @param pkDivisionCode 行政区划代码GB-T2260-2017主键
     * @return 行政区划代码GB-T2260-2017
     */
    @Override
    public TCodeAdministrativeDivision selectTCodeAdministrativeDivisionByPkDivisionCode(String pkDivisionCode) {
        return baseMapper.selectTCodeAdministrativeDivisionByPkDivisionCode(pkDivisionCode);
    }

    /**
     * 查询行政区划代码GB-T2260-2017列表
     *
     * @param tCodeAdministrativeDivision 行政区划代码GB-T2260-2017
     * @return 行政区划代码GB-T2260-2017
     */
    @Override
    public List<TCodeAdministrativeDivision> selectTCodeAdministrativeDivisionList(TCodeAdministrativeDivision tCodeAdministrativeDivision) {
        return baseMapper.selectTCodeAdministrativeDivisionList(tCodeAdministrativeDivision);
    }

    /**
     * 新增行政区划代码GB-T2260-2017
     *
     * @param tCodeAdministrativeDivision 行政区划代码GB-T2260-2017
     * @return 结果
     */
    @Override
    public int insertTCodeAdministrativeDivision(TCodeAdministrativeDivision tCodeAdministrativeDivision) {
        return baseMapper.insertTCodeAdministrativeDivision(tCodeAdministrativeDivision);
    }

    /**
     * 修改行政区划代码GB-T2260-2017
     *
     * @param tCodeAdministrativeDivision 行政区划代码GB-T2260-2017
     * @return 结果
     */
    @Override
    public int updateTCodeAdministrativeDivision(TCodeAdministrativeDivision tCodeAdministrativeDivision) {
        return baseMapper.updateTCodeAdministrativeDivision(tCodeAdministrativeDivision);
    }

    /**
     * 批量删除行政区划代码GB-T2260-2017
     *
     * @param pkDivisionCodes 需要删除的行政区划代码GB-T2260-2017主键
     * @return 结果
     */
    @Override
    public int deleteTCodeAdministrativeDivisionByPkDivisionCodes(String[] pkDivisionCodes) {
        return baseMapper.deleteTCodeAdministrativeDivisionByPkDivisionCodes(pkDivisionCodes);
    }

    /**
     * 删除行政区划代码GB-T2260-2017信息
     *
     * @param pkDivisionCode 行政区划代码GB-T2260-2017主键
     * @return 结果
     */
    @Override
    public int deleteTCodeAdministrativeDivisionByPkDivisionCode(String pkDivisionCode) {
        return baseMapper.deleteTCodeAdministrativeDivisionByPkDivisionCode(pkDivisionCode);
    }

    @Override
    public TCodeAdministrativeDivision findByCode(String code) {
        LambdaQueryWrapper<TCodeAdministrativeDivision> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(TCodeAdministrativeDivision::getPkDivisionCode, code);
        return baseMapper.selectOne(wrapper);
    }
}
