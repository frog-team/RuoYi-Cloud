package com.ruoyi.miniapp.mongo;

import com.ruoyi.miniapp.mongo.repository.WxUserRepository;
import com.tdd.wx.users.entity.WxUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * 微信用户管理
 *
 * @author loby
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class WxUserHandler {

    private final WxUserRepository wxUserRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    public WxUserHandler(WxUserRepository wxUserRepository) {
        this.wxUserRepository = wxUserRepository;
    }

    public WxUser findByOpenId(String openid) {
        Optional<WxUser> user = wxUserRepository.findById(openid);
        if (ObjectUtils.isEmpty(user) && user.isPresent()) {
            return null;
        }
        return user.get();
    }

    public WxUser findByUnionId(String unionId) {
        WxUser usr = new WxUser();
        usr.setUnionId(unionId);
        return findOneByExample(usr);
    }

    public WxUser findBySystemUserId(Long user_id) {
        WxUser user = new WxUser();
        user.setSysUserId(user_id);
        return findOneByExample(user);
    }

    public WxUser insert(WxUser wxUser) {
        wxUser.setCreateDate(LocalDateTime.now());
        return wxUserRepository.insert(wxUser);
    }

    public WxUser save(WxUser wxUser) {
        return wxUserRepository.save(wxUser);
    }

    public WxUser update(WxUser wxUser) {
        return wxUserRepository.save(wxUser);
    }

    public void delById(String open_id) {
        wxUserRepository.deleteById(open_id);
    }

    public List<WxUser> findByExample(WxUser example) {
        return wxUserRepository.findAll(Example.of(example));
    }

    public WxUser findOneByExample(WxUser example) {
        Optional<WxUser> one = wxUserRepository.findOne(Example.of(example));
        if (one.isPresent()) {
            return one.get();
        }
        return null;
    }

    /***
     * 查询所有微信用户
     * @param pageSize 单页数据量
     * @param pageNo 页码，从1开始
     * @param example 过滤example
     * @param order 用用户名排序 0：升序，1：降序
     * @return
     */
    public Page<WxUser> findAll(int pageSize, int pageNo, WxUser example, Integer order) {
        PageRequest pageRequest = null;
        if (!ObjectUtils.isEmpty(order)) {
            pageRequest = PageRequest.of(pageNo, pageSize, order == 0 ? Sort.Direction.ASC : Sort.Direction.DESC, "createDate" , "nickName");
        } else {
            pageRequest = PageRequest.of(pageNo, pageSize);
        }
        return wxUserRepository.findAll(pageRequest);
    }

    public WxUser newFindByUnionId(String unionId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("unionId").is(unionId));
        return mongoTemplate.findOne(query, WxUser.class);
    }

    public void updateWxUser(WxUser wxUser) {
        Query query = new Query(Criteria.where("openId").is(wxUser.getOpenId()));
        Update update = new Update().set("sysUserId" , wxUser.getSysUserId());
        mongoTemplate.updateFirst(query, update, WxUser.class);
    }

}
