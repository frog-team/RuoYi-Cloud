package com.ruoyi.miniapp.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.ToString;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 广告机机位,锁定机位操作在redis中进行，对象 adm_seats
 *
 * @author lobyliang
 * @date 2022-01-11
 */
@ApiModel("adm_seats(广告机机位,锁定机位操作在redis中进行，)")
@Data
@ToString
public class AdmSeats {
    private static final long serialVersionUID = 1L;

    /**
     * 机位code小区编号+3位序号
     */
    @ApiModelProperty("机位code小区编号+3位序号")
    @TableId(value = "pk_seat_code" , type = IdType.ASSIGN_UUID)
    private String pkSeatCode;

    /**
     * 安装-生产计划明细表主键
     */
    @ApiModelProperty("安装-生产计划明细表主键")
    @Excel(name = "安装-生产计划明细表主键")
    private String fkAdmProcItemId;

    /**
     * 资源id 主键
     */
    @ApiModelProperty("资源id 主键")
    @Excel(name = "资源id 主键")
    private String fkResId;

    /**
     * 小区编码
     */
    @ApiModelProperty("小区编码")
    @Excel(name = "小区编码")
    private String fkVillageCode;

    /**
     * 供电方式
     */
    @ApiModelProperty("供电方式")
    @Excel(name = "供电方式")
    private String dtPowerFromNo;

    /**
     * 投资商id
     */
    @ApiModelProperty("投资商id")
    @Excel(name = "投资商id")
    private String fkFranchiseeId;

    /**
     * app版本
     */
    @ApiModelProperty("app版本")
    @Excel(name = "app版本")
    private String firmwareVersion;

    /**
     * 主板code
     */
    @ApiModelProperty("主板code")
    @Excel(name = "主板code")
    private String admBoardCode;

    /**
     * 广告机外壳code
     */
    @ApiModelProperty("广告机外壳code")
    @Excel(name = "广告机外壳code")
    private String admShellCode;

    /**
     * 楼栋描述，什么区，多少栋
     */
    @ApiModelProperty("楼栋描述，什么区，多少栋")
    @Excel(name = "楼栋描述，什么区，多少栋")
    private String building;

    /**
     * 外健，楼栋详细信息
     */
    @ApiModelProperty("外健，楼栋详细信息")
    @Excel(name = "外健，楼栋详细信息")
    private String fkBuildingCode;

    /**
     * 单元号
     */
    @ApiModelProperty("单元号")
    @Excel(name = "单元号")
    private String unitNo;

    /**
     * 纬度
     */
    @ApiModelProperty("纬度")
    @Excel(name = "纬度")
    private Long latitude;

    /**
     * 经度
     */
    @ApiModelProperty("经度")
    @Excel(name = "经度")
    private Long longtitude;

    /**
     * 设备激活时间
     */
    @ApiModelProperty("设备激活时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "设备激活时间" , width = 30, dateFormat = "yyyy-MM-dd")
    private Date activationDate;

    /**
     * 日平均出袋数量
     */
    @ApiModelProperty("日平均出袋数量")
    @Excel(name = "日平均出袋数量")
    private Long avgBagPerDay;

    /**
     * 机位信息位置描述(如：某个单元楼栋停车场口)
     */
    @ApiModelProperty("机位信息位置描述(如：某个单元楼栋停车场口)")
    @Excel(name = "机位信息位置描述(如：某个单元楼栋停车场口)")
    private String memo;

    /**
     * 是否有机器
     */
    @ApiModelProperty("是否有机器")
    @Excel(name = "是否有机器")
    private Long isEmpty;

    /**
     * 投资商分润金额，每袋子/元
     */
    @ApiModelProperty("投资商分润金额，每袋子/元")
    @Excel(name = "投资商分润金额，每袋子/元")
    private BigDecimal franPricePerBag;

    @TableField(exist = false)
    @ApiModelProperty("已选择")
    private Integer hasCheck;

}
