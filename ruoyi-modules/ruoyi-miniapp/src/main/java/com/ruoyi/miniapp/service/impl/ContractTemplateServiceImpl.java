package com.ruoyi.miniapp.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.miniapp.domain.ContractTemplate;
import com.ruoyi.miniapp.mapper.ContractTemplateMapper;
import com.ruoyi.miniapp.service.IContractTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.rmi.ConnectIOException;

/**
 * @author songping
 * @date 2022/2/14 12:49:49
 */
@Service
@DS("tdddb")
public class ContractTemplateServiceImpl implements IContractTemplateService {

    @Autowired
    private ContractTemplateMapper contractTemplateMapper;
    @Override
    public AjaxResult getValidConTemplateByConType(Integer conType) {
        String data = contractTemplateMapper.selValidTemplateByConType(conType);
        if(ObjectUtils.isEmpty(data)){
            AjaxResult.error("不存在该类型的合同");
        }
        return AjaxResult.success("获取资源成功",data);
    }

    @Override
    public ContractTemplate getConTemplate(Integer conType) {
        return contractTemplateMapper.selConTemplateByConType(conType);
    }

    @Override
    public String getNamePathByVillageCode(String villageCode) {
        return contractTemplateMapper.selNamePathByVillageCode(villageCode);
    }

    @Override
    public String getMaxSequenceCodeByVillageCode(String villageCode) {
        return contractTemplateMapper.selMaxSequenceCodeByVillageCode(villageCode);
    }
}
