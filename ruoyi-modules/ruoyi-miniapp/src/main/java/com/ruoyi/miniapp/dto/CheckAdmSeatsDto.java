package com.ruoyi.miniapp.dto;

import lombok.Data;


@Data
public class CheckAdmSeatsDto {

    private String admSeatsCode;
    private String admSeatsName;

}
