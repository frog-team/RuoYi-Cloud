package com.ruoyi.miniapp.domain;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 广告机小区库存信息对象 adm_village_stock
 *
 * @author lobyliang
 * @date 2022-01-12
 */
@ApiModel("adm_village_stock(广告机小区库存信息)")
@Data
@ToString
public class AdmVillageStock {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty("主键")
    @Excel(name = "主键")
    private Long pkStockId;

    /**
     * 所属小区
     */
    @ApiModelProperty("所属小区")
    @Excel(name = "所属小区")
    private Integer fkVillageCode;

    /**
     * 服务商id
     */
    @ApiModelProperty("服务商id")
    @Excel(name = "服务商id")
    private Long fkMainId;

    /**
     * 库存数量(服务商签收 +数量  安装成功 -数量)
     */
    @ApiModelProperty("库存数量(服务商签收 +数量  安装成功 -数量)")
    @Excel(name = "库存数量(服务商签收 +数量  安装成功 -数量)")
    private Integer stockQuantity;

    /**
     * 设备名称
     */
    @ApiModelProperty("设备名称")
    @Excel(name = "设备名称")
    private String stockName;

    /**
     * 设备类型
     */
    @ApiModelProperty("设备类型")
    @Excel(name = "设备类型")
    private Integer stockType;

    /**
     * 发货单(签收单)主键
     */
    @ApiModelProperty("发货单(签收单)主键")
    @Excel(name = "发货单(签收单)主键")
    private String fkAdmDevCode;

    /**
     * 修改时间
     */
    @ApiModelProperty("修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "修改时间" , width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;

    /**
     * 修改人(操作人的uid)
     */
    @ApiModelProperty("修改人(操作人的uid)")
    @Excel(name = "修改人(操作人的uid)")
    private String updateUser;

}
