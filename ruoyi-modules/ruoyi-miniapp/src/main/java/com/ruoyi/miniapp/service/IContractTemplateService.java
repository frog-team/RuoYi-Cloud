package com.ruoyi.miniapp.service;

import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.miniapp.domain.ContractTemplate;

public interface IContractTemplateService {
    /**
     * 基于和合同模板类型 获取 合同资源路径
     * @param conType 合同模板类型
     * @return
     */
    AjaxResult getValidConTemplateByConType(Integer conType);

    /**
     * 给予合同模板类型 查询合同模板信息
     * @param conType 合同模板类型
     * @return
     */
    ContractTemplate getConTemplate(Integer conType);

    /**
     * 基于 小区的code 查询该小区所属的 街道地址
     * @param villageCode 小区的code
     * @return
     */
    String getNamePathByVillageCode(String villageCode);

    /**
     * 基于小区的code 查询 该小区最大三位顺序码
     * @param villageCode  小区的code
     * @return
     */
    String getMaxSequenceCodeByVillageCode(String villageCode);
}
