package com.ruoyi.miniapp.dto;

import com.github.wxiaoqi.security.common.tool.ToolPager;
import com.ruoyi.miniapp.domain.TrainingAddress;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ArgumentsDto extends ToolPager<ArgumentsDto> {

    /**
     * 运维后台报名后台管理
     * 前端返回参数接收
     * 场次id
     */
    private Integer id;

    private Integer evaluationResult;

    /**
     * 运维后台报名后台管理
     * 前端返回参数接收
     * 用户报名编号
     */
    private List<String> keys;

    /**
     * 返回前端参数  名称
     */
    private String trainingName;

    /**
     * 返回前端参数  名称
     */
    private List<TrainingAddress> trainingAddresses;


}
