package com.ruoyi.miniapp.service;

import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.miniapp.domain.TrainingAddress;
import com.ruoyi.miniapp.dto.TrainingAddressDto;

import java.util.List;

public interface ITrainingAddressService {

	List<TrainingAddress> getTrainingAddressInfoList(String divisionCode);

	/**
	 *  查询培训地址
	 * @return
	 */
	public AjaxResult getTrainingAddressInfoOne(String divisionCode);

	/**
	 *  查询培训场次
	 * @return
	 */
	public AjaxResult getTrainingAddressInfoBytrainingName(String trainingName);

	/**
	 *  返回用户报名信息
	 * @return
	 */
	public AjaxResult getUserInfo(Integer id);

	List<TrainingAddress> getCityList();

	/**
	 *  运维后台查询市级
	 * @return
	 */
	public AjaxResult selCityList();

	/**
	 * 运维后台
	 * #根据市级编码查询下面有多少培训基地
	 */
	public AjaxResult selTrainingNameList(String fkDivisionCode);

	/**
	 * 运维后台
	 * #根据培训基地名称查询场次
	 * @return
	 */
	public AjaxResult selTrainingAddressList(String teainingName);

	/**
	 *  返回报名用户id
	 * @return
	 */
	public AjaxResult selStudentList(Integer id);

	/**
	 *  获取培训基地场次（次数）
	 * @return
	 */
	public String getTrainingCountBytrainingName(String trainingName);

	/**
	 *  剩余名额+ -
	 */
	public void updateSurplusPlacesA(Integer id);

	/**
	 *  剩余名额+ -
	 */
	public void updateSurplusPlacesB(Integer id);


	/**
	 * 增加
	 * @param obj
	 * @return
	 */
	public int insertTrainingAddress(TrainingAddress obj);

	/**
	 * 查count
	 * @param obj
	 * @return
	 */
	public int getTrainingAddressCount(TrainingAddress obj);

	/**
	 * 查List
	 * @param obj
	 * @return
	 */
	public List<TrainingAddress> getTrainingAddressList(TrainingAddress obj);

	/**
	 * 分页查询
	 * @param obj
	 * @return
	 */
	public AjaxResult getPageTrainingAddress(TrainingAddress obj);

	/**
	 * 主键查询
	 * @param key
	 * @return
	 */
	public TrainingAddressDto getTrainingAddressByKey(Integer key);

	/**
	 * 多主键查询
	 * @param keys
	 * @return
	 */
	public List<TrainingAddress> getTrainingAddressByKeys(List<Long> keys);

	/**
	 * 修改
	 * @param obj
	 * @return
	 */
	public int updateTrainingAddress(TrainingAddress obj);

	/**
	 * 删除
	 * @param key
	 * @return
	 */
	public AjaxResult deleteTrainingAddress(Integer key);

	/**
	 * 批量增加
	 * @param objList
	 * @return
	 */
	public int batchInsertTrainingAddress(List<TrainingAddress> objList);

	/**
	 * 批量修改
	 * @param objList
	 * @return
	 */
	public int batchUpdateTrainingAddress(List<TrainingAddress> objList);

	/**
	 * 批量删除
	 * @param keys
	 * @return
	 */
	public int batchDeleteTrainingAddress(List<Long> keys);

}