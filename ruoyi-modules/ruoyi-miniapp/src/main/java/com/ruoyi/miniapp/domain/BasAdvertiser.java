package com.ruoyi.miniapp.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 广告主信息对象 bas_advertiser
 *
 * @author lobyliang
 * @date 2022-01-21
 */
@ApiModel("bas_advertiser(广告主信息)")
@Data
@ToString
public class BasAdvertiser
{
    private static final long serialVersionUID = 1L;

    /** 广告主(平台唯一的id) */
    @ApiModelProperty("广告主(平台唯一的id)")
    @TableId(value = "user_id",type = IdType.INPUT)
    private Long userId;

    /** 关联虚拟账户 */
    @ApiModelProperty("关联虚拟账户")
    @Excel(name = "关联虚拟账户")
    private String fkAccountId;

    /** 广告主所在行政区划代码 */
    @ApiModelProperty("广告主所在行政区划代码")
    @Excel(name = "广告主所在行政区划代码")
    private String fkDivisionCode;

    /** 用户微信uid */
    @ApiModelProperty("用户微信uid")
    @Excel(name = "用户微信uid")
    private String fkUnionId;

    /** 统一社会信用代码 */
    @ApiModelProperty("统一社会信用代码")
    @Excel(name = "统一社会信用代码")
    private String fkOrgCode;

    /** 分享人(平台唯一的id) */
    @ApiModelProperty("分享人(平台唯一的id)")
    @Excel(name = "分享人(平台唯一的id)")
    private String shareUserId;

}
