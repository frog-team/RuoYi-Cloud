package com.ruoyi.miniapp.ali;

import java.util.UUID;

/**
 * @author lobyliang
 */
public class UUIDUtil {


    public static String createId() {
        UUID uuid = UUID.randomUUID();
        String str=uuid.toString().replace("-","");
        return str;
    }
}
