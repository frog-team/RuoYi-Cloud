package com.ruoyi.miniapp.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@Configuration
@ConfigurationProperties(prefix = "aliinfo")
public class AliInfoProperty {

    private String accessKeyId;

    private String accessKeySecret;

    private String regionId;


}
