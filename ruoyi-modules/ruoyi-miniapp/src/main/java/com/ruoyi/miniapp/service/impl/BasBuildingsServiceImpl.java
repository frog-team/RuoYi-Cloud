package com.ruoyi.miniapp.service.impl;

import java.util.List;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.miniapp.domain.BasVillage;
import com.ruoyi.miniapp.service.IAdmSeatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.miniapp.mapper.BasBuildingsMapper;
import com.ruoyi.miniapp.domain.BasBuildings;
import com.ruoyi.miniapp.service.IBasBuildingsService;

/**
 * 楼栋信息Service业务层处理
 *
 * @author lobyliang
 * @date 2022-01-12
 */
@Service
@DS("tdddb")
public class BasBuildingsServiceImpl extends ServiceImpl<BasBuildingsMapper, BasBuildings> implements IBasBuildingsService {
    @Autowired
    private BasBuildingsMapper basBuildingsMapper;
    @Autowired
    private IAdmSeatsService admSeatsService;

    /**
     * 查询楼栋信息
     *
     * @param pkBuildingCode 楼栋信息主键
     * @return 楼栋信息
     */
    @Override
    public BasBuildings selectBasBuildingsByPkBuildingCode(String pkBuildingCode) {
        return basBuildingsMapper.selectBasBuildingsByPkBuildingCode(pkBuildingCode);
    }

    /**
     * 查询楼栋信息列表
     *
     * @param basBuildings 楼栋信息
     * @return 楼栋信息
     */
    @Override
    public List<BasBuildings> selectBasBuildingsList(BasBuildings basBuildings) {
        return basBuildingsMapper.selectBasBuildingsList(basBuildings);
    }

    /**
     * 新增楼栋信息
     *
     * @param basBuildings 楼栋信息
     * @return 结果
     */
    @Override
    public int insertBasBuildings(BasBuildings basBuildings) {
        return basBuildingsMapper.insertBasBuildings(basBuildings);
    }

    /**
     * 修改楼栋信息
     *
     * @param basBuildings 楼栋信息
     * @return 结果
     */
    @Override
    public int updateBasBuildings(BasBuildings basBuildings) {
        return basBuildingsMapper.updateBasBuildings(basBuildings);
    }

    /**
     * 批量删除楼栋信息
     *
     * @param pkBuildingCodes 需要删除的楼栋信息主键
     * @return 结果
     */
    @Override
    public int deleteBasBuildingsByPkBuildingCodes(String[] pkBuildingCodes) {
        return basBuildingsMapper.deleteBasBuildingsByPkBuildingCodes(pkBuildingCodes);
    }

    /**
     * 删除楼栋信息信息
     *
     * @param pkBuildingCode 楼栋信息主键
     * @return 结果
     */
    @Override
    public int deleteBasBuildingsByPkBuildingCode(String pkBuildingCode) {
        return basBuildingsMapper.deleteBasBuildingsByPkBuildingCode(pkBuildingCode);
    }

    @Override
    public List<BasBuildings> findByVillageCode(String code) {
//        LambdaQueryWrapper<BasBuildings> wrapper = Wrappers.lambdaQuery();
//        wrapper.eq(BasBuildings::getFkVillageCode, code);
        List<BasBuildings> list = basBuildingsMapper.selectListOrderByEmptyNum(code);
        list.forEach(r -> {
            r.setAdmSeatsList(admSeatsService.findByBuildingCode(r.getPkBuildingCode()));
        });
        return list;
    }

}
