package com.ruoyi.miniapp.domain;


import lombok.Data;
import lombok.ToString;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.ruoyi.common.core.web.domain.BaseEntity;

import java.math.BigDecimal;

/**
 * 行政区划代码GB-T2260-2017对象 t_code_administrative_division
 *
 * @author lobyliang
 * @date 2022-01-12
 */
@ApiModel("t_code_administrative_division(行政区划代码GB-T2260-2017)")
@Data
@ToString
public class TCodeAdministrativeDivision extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 行政区划编码(省市区 6位编码  街道 9位编码)
     */
    @ApiModelProperty("行政区划编码(省市区 6位编码  街道 9位编码)")
    private String pkDivisionCode;

    /**
     * 行政区划名称
     */
    @ApiModelProperty("行政区划名称")
    @Excel(name = "行政区划名称")
    private String divisionName;

    /**
     * 拼音码
     */
    @ApiModelProperty("拼音码")
    @Excel(name = "拼音码")
    private String fullPhonetic;

    /**
     * 上级区域code
     */
    @ApiModelProperty("上级区域code")
    @Excel(name = "上级区域code")
    private String fkParentCode;

    /**
     * 按照层级排列的区划编码，用逗号“,"分隔
     */
    @ApiModelProperty("按照层级排列的区划编码，用逗号")
    @Excel(name = "按照层级排列的区划编码，用逗号")
    private String codePath;

    /**
     * 按照区划级别名称排列的字符串，用>分隔
     */
    @ApiModelProperty("按照区划级别名称排列的字符串，用>分隔")
    @Excel(name = "按照区划级别名称排列的字符串，用>分隔")
    private String namePath;

    /**
     * 行政存级
     */
    @ApiModelProperty("行政存级")
    @Excel(name = "行政存级")
    private Long depth;

    /**
     * 字母码，简短码
     */
    @ApiModelProperty("字母码，简短码")
    @Excel(name = "字母码，简短码")
    private String alphabetCode;

    /**
     * 街道办事处数量
     */
    @ApiModelProperty("街道办事处数量")
    @Excel(name = "街道办事处数量")
    private Long streetAmount;

    /**
     * 社区办事处数量
     */
    @ApiModelProperty("社区办事处数量")
    @Excel(name = "社区办事处数量")
    private Long communityAmount;

    /**
     * 小区数量
     */
    @ApiModelProperty("小区数量")
    @Excel(name = "小区数量")
    private Long villageAmount;

    /**
     * 人口数量/万
     */
    @ApiModelProperty("人口数量/万")
    @Excel(name = "人口数量/万")
    private BigDecimal population;

    /**
     * 广告机机位数量
     */
    @ApiModelProperty("广告机机位数量")
    @Excel(name = "广告机机位数量")
    private Long vdmSeatAmount;

    /**
     * 广告机数量
     */
    @ApiModelProperty("广告机数量")
    @Excel(name = "广告机数量")
    private Long vdmAmount;

}
