package com.ruoyi.miniapp.mongo.repository;

import com.tdd.wx.users.entity.WxUser;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @author loby
 */
@Repository
public interface WxUserRepository extends MongoRepository<WxUser, String> {
}
