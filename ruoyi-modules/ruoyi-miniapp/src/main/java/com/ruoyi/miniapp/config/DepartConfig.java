package com.ruoyi.miniapp.config;

import com.ruoyi.common.core.constant.SystemRoleConstants;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

/**
 * @author lobyliang
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "depart")
public class DepartConfig{
    /**
     * 服务商部门
     */
    Long maintainer;//211
    /**
     * 投资人部门
     */
    Long franchiess;//: 213
    /**
     * 广告主部门
     */
    Long advertiser;//: 202
    /**
     * 服务商 并 投资人部门
     */
    Long mainFranch;//: 236
    /**
     * 广告主 并 投资人部门
     */
    Long franchAdver;//238
    /**
     * 广告主 并 服务商
     */
    Long mainAdver;//239

    /**
     * 服务商 并 投资人 并 广告组部门
     */
    Long mFA;//: 237

    public Long getDepartId(Long[] roles)
    {
        Long ret = advertiser;
        if(Arrays.stream(roles).anyMatch(role ->role.equals(SystemRoleConstants.ADVERTISING_ROLE_ID)))
        {
            if(Arrays.stream(roles).anyMatch(role ->role.equals(SystemRoleConstants.MAINTAINER_ROLE_ID)))
            {
                ret = mainAdver;
                if(Arrays.stream(roles).anyMatch(role ->role.equals(SystemRoleConstants.FRANCHISEES_ROLE_ID)))
                {
                    ret = mFA;
                }
            }
            else
                if(Arrays.stream(roles).anyMatch(role ->role.equals(SystemRoleConstants.FRANCHISEES_ROLE_ID)))
                {
                    ret = franchAdver;
                }
        }
        else
            if(Arrays.stream(roles).anyMatch(role ->role.equals(SystemRoleConstants.FRANCHISEES_ROLE_ID)))
            {
                ret = franchiess;
                if(Arrays.stream(roles).anyMatch(role ->role.equals(SystemRoleConstants.MAINTAINER_ROLE_ID)))
                {
                    ret = mainFranch;
                }
            }
            return ret;
    }
}
