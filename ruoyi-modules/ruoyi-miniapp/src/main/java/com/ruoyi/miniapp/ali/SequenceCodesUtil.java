package com.ruoyi.miniapp.ali;

import net.sourceforge.pinyin4j.PinyinHelper;
import org.apache.commons.lang3.StringUtils;

/**
 * @return 生成新的三位顺序码
 */
public class SequenceCodesUtil {

    /**
     * 得到中文首字母（中国 -> ZG）
     * @param str 需要转化的中文字符串
     * @return 大写首字母缩写的字符串
     */
    public static String getPinYinHeadChar(String str) {
        StringBuilder convert = new StringBuilder();
        if (StringUtils.isBlank(str)) {
            return "";
        }
        for (int j = 0; j < str.length(); j++) {
            char word = str.charAt(j);
            String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(word);
            if (pinyinArray != null) {
                convert.append(pinyinArray[0].charAt(0));
            } else {
                convert.append(word);
            }
        }
        return convert.toString().toUpperCase();
    }


    public static String createNewBidNumber(String str) { //传的是2020-11-11
        int num = Integer.valueOf(str);
        String idNum="";
        // 123+1
        num ++;
            if(num<10){
                //num<10,说明是个位数，前面要补两个0
                idNum = String.format("%03d", num);
                return idNum;
            }
            else if(num<100){
                //num<100,说明是两位数，前面要补一个0
                idNum = "0"+num;
                return idNum;
            }
            else {
                idNum = String.valueOf(num);
                return idNum;
            }
        }
    }
