package com.ruoyi.miniapp.dto;

import lombok.Data;

import java.util.List;


@Data
public class CheckVillageDto {

    private String villageCode;
    private String villageName;

    private List<CheckBuildingDto> buildingList;

    private Integer seatsNum;

}
