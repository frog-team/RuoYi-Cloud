package com.ruoyi.miniapp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.miniapp.domain.TCodeAdministrativeDivision;

import java.util.List;

/**
 * 行政区划代码GB-T2260-2017Service接口
 *
 * @author lobyliang
 * @date 2022-01-12
 */
public interface ITCodeAdministrativeDivisionService extends IService<TCodeAdministrativeDivision> {
    /**
     * 查询行政区划代码GB-T2260-2017
     *
     * @param pkDivisionCode 行政区划代码GB-T2260-2017主键
     * @return 行政区划代码GB-T2260-2017
     */
    public TCodeAdministrativeDivision selectTCodeAdministrativeDivisionByPkDivisionCode(String pkDivisionCode);

    /**
     * 查询行政区划代码GB-T2260-2017列表
     *
     * @param tCodeAdministrativeDivision 行政区划代码GB-T2260-2017
     * @return 行政区划代码GB-T2260-2017集合
     */
    public List<TCodeAdministrativeDivision> selectTCodeAdministrativeDivisionList(TCodeAdministrativeDivision tCodeAdministrativeDivision);

    /**
     * 新增行政区划代码GB-T2260-2017
     *
     * @param tCodeAdministrativeDivision 行政区划代码GB-T2260-2017
     * @return 结果
     */
    public int insertTCodeAdministrativeDivision(TCodeAdministrativeDivision tCodeAdministrativeDivision);

    /**
     * 修改行政区划代码GB-T2260-2017
     *
     * @param tCodeAdministrativeDivision 行政区划代码GB-T2260-2017
     * @return 结果
     */
    public int updateTCodeAdministrativeDivision(TCodeAdministrativeDivision tCodeAdministrativeDivision);

    /**
     * 批量删除行政区划代码GB-T2260-2017
     *
     * @param pkDivisionCodes 需要删除的行政区划代码GB-T2260-2017主键集合
     * @return 结果
     */
    public int deleteTCodeAdministrativeDivisionByPkDivisionCodes(String[] pkDivisionCodes);

    /**
     * 删除行政区划代码GB-T2260-2017信息
     *
     * @param pkDivisionCode 行政区划代码GB-T2260-2017主键
     * @return 结果
     */
    public int deleteTCodeAdministrativeDivisionByPkDivisionCode(String pkDivisionCode);

    TCodeAdministrativeDivision findByCode(String code);
}
