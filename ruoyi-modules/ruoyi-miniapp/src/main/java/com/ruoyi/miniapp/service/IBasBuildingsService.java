package com.ruoyi.miniapp.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.miniapp.domain.BasBuildings;

/**
 * 楼栋信息Service接口
 *
 * @author lobyliang
 * @date 2022-01-12
 */
public interface IBasBuildingsService extends IService<BasBuildings> {
    /**
     * 查询楼栋信息
     *
     * @param pkBuildingCode 楼栋信息主键
     * @return 楼栋信息
     */
    public BasBuildings selectBasBuildingsByPkBuildingCode(String pkBuildingCode);

    /**
     * 查询楼栋信息列表
     *
     * @param basBuildings 楼栋信息
     * @return 楼栋信息集合
     */
    public List<BasBuildings> selectBasBuildingsList(BasBuildings basBuildings);

    /**
     * 新增楼栋信息
     *
     * @param basBuildings 楼栋信息
     * @return 结果
     */
    public int insertBasBuildings(BasBuildings basBuildings);

    /**
     * 修改楼栋信息
     *
     * @param basBuildings 楼栋信息
     * @return 结果
     */
    public int updateBasBuildings(BasBuildings basBuildings);

    /**
     * 批量删除楼栋信息
     *
     * @param pkBuildingCodes 需要删除的楼栋信息主键集合
     * @return 结果
     */
    public int deleteBasBuildingsByPkBuildingCodes(String[] pkBuildingCodes);

    /**
     * 删除楼栋信息信息
     *
     * @param pkBuildingCode 楼栋信息主键
     * @return 结果
     */
    public int deleteBasBuildingsByPkBuildingCode(String pkBuildingCode);

    List<BasBuildings> findByVillageCode(String code);
}
