package com.ruoyi.miniapp.job;

import com.ruoyi.common.core.utils.SpringUtils;
import com.ruoyi.miniapp.service.impl.FranSeatAgreementServiceImpl;

public class OrderJob {

    FranSeatAgreementServiceImpl franSeatAgreementService = SpringUtils.getBean(FranSeatAgreementServiceImpl.class);

    public void batchCancelUnPay(){
        franSeatAgreementService.batchCancelUnPayOrder();
    }

}
