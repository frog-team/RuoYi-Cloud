package com.ruoyi.miniapp.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.miniapp.domain.BasBuildings;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 楼栋信息Mapper接口
 *
 * @author lobyliang
 * @date 2022-01-12
 */
public interface BasBuildingsMapper extends BaseMapper<BasBuildings> {
    /**
     * 查询楼栋信息
     *
     * @param pkBuildingCode 楼栋信息主键
     * @return 楼栋信息
     */
    public BasBuildings selectBasBuildingsByPkBuildingCode(String pkBuildingCode);

    /**
     * 查询楼栋信息列表
     *
     * @param basBuildings 楼栋信息
     * @return 楼栋信息集合
     */
    public List<BasBuildings> selectBasBuildingsList(BasBuildings basBuildings);

    /**
     * 新增楼栋信息
     *
     * @param basBuildings 楼栋信息
     * @return 结果
     */
    public int insertBasBuildings(BasBuildings basBuildings);

    /**
     * 修改楼栋信息
     *
     * @param basBuildings 楼栋信息
     * @return 结果
     */
    public int updateBasBuildings(BasBuildings basBuildings);

    /**
     * 删除楼栋信息
     *
     * @param pkBuildingCode 楼栋信息主键
     * @return 结果
     */
    public int deleteBasBuildingsByPkBuildingCode(String pkBuildingCode);

    /**
     * 批量删除楼栋信息
     *
     * @param pkBuildingCodes 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBasBuildingsByPkBuildingCodes(String[] pkBuildingCodes);

    @Select("SELECT * from bas_buildings t where fk_village_code = #{code} ORDER BY (SELECT count(*) FROM adm_seats where is_empty = 1 and fk_building_code = t.pk_building_code) DESC")
    public List<BasBuildings> selectListOrderByEmptyNum(@Param("code") String code);
}
