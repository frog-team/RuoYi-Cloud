package com.ruoyi.miniapp.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.miniapp.domain.FraFranchisees;
import com.ruoyi.miniapp.domain.FranSeatAgreement;

/**
 * 加盟商购买点位订单Mapper接口
 *
 * @author lobyliang
 * @date 2022-01-14
 */
public interface FranSeatAgreementMapper extends BaseMapper<FranSeatAgreement> {
    /**
     * 查询加盟商购买点位订单
     *
     * @param pkSeatCode 加盟商购买点位订单主键
     * @return 加盟商购买点位订单
     */
    public FranSeatAgreement selectFranSeatAgreementByPkSeatCode(String pkSeatCode);

    /**
     * 查询加盟商购买点位订单列表
     *
     * @param franSeatAgreement 加盟商购买点位订单
     * @return 加盟商购买点位订单集合
     */
    public List<FranSeatAgreement> selectFranSeatAgreementList(FranSeatAgreement franSeatAgreement);

    /**
     * 新增加盟商购买点位订单
     *
     * @param franSeatAgreement 加盟商购买点位订单
     * @return 结果
     */
    public int insertFranSeatAgreement(FranSeatAgreement franSeatAgreement);

    /**
     * 修改加盟商购买点位订单
     *
     * @param franSeatAgreement 加盟商购买点位订单
     * @return 结果
     */
    public int updateFranSeatAgreement(FranSeatAgreement franSeatAgreement);

    /**
     * 删除加盟商购买点位订单
     *
     * @param pkSeatCode 加盟商购买点位订单主键
     * @return 结果
     */
    public int deleteFranSeatAgreementByPkSeatCode(String pkSeatCode);

    /**
     * 批量删除加盟商购买点位订单
     *
     * @param pkSeatCodes 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFranSeatAgreementByPkSeatCodes(String[] pkSeatCodes);
}
