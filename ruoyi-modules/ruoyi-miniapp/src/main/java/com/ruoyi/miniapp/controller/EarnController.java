package com.ruoyi.miniapp.controller;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.miniapp.mongo.WxUserHandler;
import com.ruoyi.miniapp.service.IAccountInfoService;
import com.ruoyi.system.api.domain.base.AccountInfo;
import com.taodaidai.dao.entity.MMaintainerSettlementLog;
import com.taodaidai.dao.entity.MSettlementLog;
import com.tdd.adm.service.feign.IncomeFeignClient;
import com.tdd.wx.users.entity.WxUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api("收益")
@RestController
@RequestMapping("earn")
@Log4j2
public class EarnController {

    @Autowired
    private WxUserHandler wxUserHandler;

    @Autowired
    private IAccountInfoService iAccountInfoService;

//    @DubboReference(version = "1.0.0" , interfaceClass = IncomeServiceApi.class, interfaceName = "IncomeService" , lazy = true, check = false)
//    private IncomeServiceApi incomeServiceApi;
    @Autowired
    IncomeFeignClient incomeFeignClient;

    @ApiOperation("总收入")
    @GetMapping("totalIncome")
    public R totalIncome() {
        Map rtMap = new HashMap<>();
        String unionId = SecurityUtils.getUnionId();
        WxUser wxUser = wxUserHandler.findByUnionId(unionId);
        if (wxUser == null || wxUser.getSysUserId() == null) {
            rtMap.put("total" , "0.00");
        } else {
            try {
                AccountInfo accountInfo = iAccountInfoService.getById(wxUser.getSysUserId());
                accountInfo = iAccountInfoService.initAccountInfo(wxUser.getSysUserId(),wxUser.getUnionId(),wxUser.getOpenId(),BigDecimal.ZERO);
                rtMap.put("total" , accountInfo.getBalance());
            } catch (Exception e) {
                log.error(e.getMessage());
                rtMap.put("total" , "0.00");
            }
        }
        return R.ok(rtMap);
    }

    @ApiOperation("服务商收益明细")
    @GetMapping("getMaintainerIncomeLog")
    public R getMaintainerIncomeLog(Integer pageSize, Integer pageNo) {
        if (pageNo == null) {
            pageNo = 1;
        }
        if (pageSize == null) {
            pageSize = 1;
        }
        Map rtMap = new HashMap<>();
        String unionId = SecurityUtils.getUnionId();
        WxUser wxUser = wxUserHandler.newFindByUnionId(unionId);
        try {
//            List<MSettlementLog> list = incomeServiceApi.getMaintainerIncomeLog(wxUser.getSysUserId(), pageSize, pageNo);
            TableDataInfo maintainerIncomeLog = incomeFeignClient.getMaintainerIncomeLog(wxUser.getSysUserId(), pageSize, pageNo);
            List<MSettlementLog> list = null;
            if(maintainerIncomeLog!=null)
            {
                list = (List<MSettlementLog>) maintainerIncomeLog.getRows();
            }
            else {
                list = new ArrayList<>(0);
            }
            rtMap.put("list" , list);
        } catch (Exception e) {
            log.error(e.getMessage());
            List<MMaintainerSettlementLog> list = new ArrayList<>();
//            for (int i = 0; i < 8; i++) {
//                MMaintainerSettlementLog r = new MMaintainerSettlementLog();
//                r.setMainId(wxUser.getSysUserId());
//                r.setMoney(BigDecimal.ONE);
//                r.setRecordTime(LocalDateTime.now());
//                r.setId((i + 1) + "");
//                list.add(r);
//            }
            rtMap.put("list" , list);
        }
        return R.ok(rtMap);
    }

}
