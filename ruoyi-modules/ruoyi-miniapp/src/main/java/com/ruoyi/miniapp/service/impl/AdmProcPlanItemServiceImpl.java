package com.ruoyi.miniapp.service.impl;

import java.util.List;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.miniapp.mapper.AdmProcPlanItemMapper;
import com.ruoyi.miniapp.domain.AdmProcPlanItem;
import com.ruoyi.miniapp.service.IAdmProcPlanItemService;

/**
 * 生产计划明细(一台机器一条数据)Service业务层处理
 *
 * @author lobyliang
 * @date 2022-01-14
 */
@Service
@DS("tdddb")
public class AdmProcPlanItemServiceImpl extends ServiceImpl<AdmProcPlanItemMapper, AdmProcPlanItem> implements IAdmProcPlanItemService {
    @Autowired
    private AdmProcPlanItemMapper admProcPlanItemMapper;

    /**
     * 查询生产计划明细(一台机器一条数据)
     *
     * @param productSerialCode 生产计划明细(一台机器一条数据)主键
     * @return 生产计划明细(一台机器一条数据)
     */
    @Override
    public AdmProcPlanItem selectAdmProcPlanItemByProductSerialCode(String productSerialCode) {
        return admProcPlanItemMapper.selectAdmProcPlanItemByProductSerialCode(productSerialCode);
    }

    /**
     * 查询生产计划明细(一台机器一条数据)列表
     *
     * @param admProcPlanItem 生产计划明细(一台机器一条数据)
     * @return 生产计划明细(一台机器一条数据)
     */
    @Override
    public List<AdmProcPlanItem> selectAdmProcPlanItemList(AdmProcPlanItem admProcPlanItem) {
        return admProcPlanItemMapper.selectAdmProcPlanItemList(admProcPlanItem);
    }

    /**
     * 新增生产计划明细(一台机器一条数据)
     *
     * @param admProcPlanItem 生产计划明细(一台机器一条数据)
     * @return 结果
     */
    @Override
    public int insertAdmProcPlanItem(AdmProcPlanItem admProcPlanItem) {
        return admProcPlanItemMapper.insertAdmProcPlanItem(admProcPlanItem);
    }

    /**
     * 修改生产计划明细(一台机器一条数据)
     *
     * @param admProcPlanItem 生产计划明细(一台机器一条数据)
     * @return 结果
     */
    @Override
    public int updateAdmProcPlanItem(AdmProcPlanItem admProcPlanItem) {
        return admProcPlanItemMapper.updateAdmProcPlanItem(admProcPlanItem);
    }

    /**
     * 批量删除生产计划明细(一台机器一条数据)
     *
     * @param productSerialCodes 需要删除的生产计划明细(一台机器一条数据)主键
     * @return 结果
     */
    @Override
    public int deleteAdmProcPlanItemByProductSerialCodes(String[] productSerialCodes) {
        return admProcPlanItemMapper.deleteAdmProcPlanItemByProductSerialCodes(productSerialCodes);
    }

    /**
     * 删除生产计划明细(一台机器一条数据)信息
     *
     * @param productSerialCode 生产计划明细(一台机器一条数据)主键
     * @return 结果
     */
    @Override
    public int deleteAdmProcPlanItemByProductSerialCode(String productSerialCode) {
        return admProcPlanItemMapper.deleteAdmProcPlanItemByProductSerialCode(productSerialCode);
    }

    @Override
    public List<AdmProcPlanItem> findByPlanCode(String code){
        LambdaQueryWrapper<AdmProcPlanItem> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(AdmProcPlanItem::getFkAdmPlanCode,code);
        wrapper.orderByAsc(AdmProcPlanItem::getDeployFinishDate);
        return baseMapper.selectList(wrapper);
    }

}
