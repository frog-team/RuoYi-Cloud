package com.ruoyi.miniapp.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.miniapp.domain.BasMaintainer;

/**
 * 服务商Mapper接口
 *
 * @author lobyliang
 * @date 2022-01-21
 */
public interface BasMaintainerMapper extends BaseMapper<BasMaintainer>
{
    /**
     * 查询服务商
     *
     * @param userId 服务商主键
     * @return 服务商
     */
    public BasMaintainer selectBasMaintainerByUserId(Long userId);

    /**
     * 查询服务商列表
     *
     * @param basMaintainer 服务商
     * @return 服务商集合
     */
    public List<BasMaintainer> selectBasMaintainerList(BasMaintainer basMaintainer);

    /**
     * 新增服务商
     *
     * @param basMaintainer 服务商
     * @return 结果
     */
    public int insertBasMaintainer(BasMaintainer basMaintainer);

    /**
     * 修改服务商
     *
     * @param basMaintainer 服务商
     * @return 结果
     */
    public int updateBasMaintainer(BasMaintainer basMaintainer);

    /**
     * 删除服务商
     *
     * @param userId 服务商主键
     * @return 结果
     */
    public int deleteBasMaintainerByUserId(Long userId);

    /**
     * 批量删除服务商
     *
     * @param userIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBasMaintainerByUserIds(Long[] userIds);
}
