package com.ruoyi.miniapp.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.miniapp.domain.AdmSeats;
import com.ruoyi.miniapp.domain.BasVillage;
import com.ruoyi.miniapp.service.IAdmSeatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 广告机机位,锁定机位操作在redis中进行，Controller
 *
 * @author lobyliang
 * @date 2022-01-11
 */
@RestController
@RequestMapping("admseats")
public class AdmSeatsController extends BaseController {
    @Autowired
    private IAdmSeatsService admSeatsService;

    /**
     * 查询广告机机位,锁定机位操作在redis中进行，列表
     */
    @RequiresPermissions("dao:seats:list")
    @GetMapping("/list")
    public TableDataInfo list(AdmSeats admSeats) {
        startPage();
        List<AdmSeats> list = admSeatsService.selectAdmSeatsList(admSeats);
        return getDataTable(list);
    }

    /**
     * 导出广告机机位,锁定机位操作在redis中进行，列表
     */
    @RequiresPermissions("dao:seats:export")
    @Log(title = "广告机机位,锁定机位操作在redis中进行，" , businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AdmSeats admSeats) throws IOException {
        List<AdmSeats> list = admSeatsService.selectAdmSeatsList(admSeats);
        ExcelUtil<AdmSeats> util = new ExcelUtil<AdmSeats>(AdmSeats.class);
        util.exportExcel(response, list, "广告机机位,锁定机位操作在redis中进行，数据");
    }

    /**
     * 获取广告机机位,锁定机位操作在redis中进行，详细信息
     */
    @RequiresPermissions("dao:seats:query")
    @GetMapping(value = "/{pkSeatCode}")
    public AjaxResult getInfo(@PathVariable("pkSeatCode") String pkSeatCode) {
        return AjaxResult.success(admSeatsService.selectAdmSeatsByPkSeatCode(pkSeatCode));
    }

    /**
     * 新增广告机机位,锁定机位操作在redis中进行，
     */
    @RequiresPermissions("dao:seats:add")
    @Log(title = "广告机机位,锁定机位操作在redis中进行，" , businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AdmSeats admSeats) {
        return toAjax(admSeatsService.insertAdmSeats(admSeats));
    }

    /**
     * 修改广告机机位,锁定机位操作在redis中进行，
     */
    @RequiresPermissions("dao:seats:edit")
    @Log(title = "广告机机位,锁定机位操作在redis中进行，" , businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AdmSeats admSeats) {
        return toAjax(admSeatsService.updateAdmSeats(admSeats));
    }

    /**
     * 删除广告机机位,锁定机位操作在redis中进行，
     */
    @RequiresPermissions("dao:seats:remove")
    @Log(title = "广告机机位,锁定机位操作在redis中进行，" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{pkSeatCodes}")
    public AjaxResult remove(@PathVariable String[] pkSeatCodes) {
        return toAjax(admSeatsService.deleteAdmSeatsByPkSeatCodes(pkSeatCodes));
    }

}
