package com.ruoyi.miniapp.dto;

import com.ruoyi.miniapp.domain.TrainingAddress;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TrainingAddressDto extends TrainingAddress {

    /**
     * 报名编号
     */
    private String applyId;

    /**
     * 用户姓名
     */
    private String userName;

    /**
     * 用户手机号
     */
    private String userPhone;

    /**
     * 培训人员编码
     */
    private String certificateNo;
}
