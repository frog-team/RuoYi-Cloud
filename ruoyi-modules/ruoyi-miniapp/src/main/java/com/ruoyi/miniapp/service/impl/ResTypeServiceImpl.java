package com.ruoyi.miniapp.service.impl;

import com.ruoyi.miniapp.domain.ResType;
import com.ruoyi.miniapp.mapper.ResTypeMapper;
import com.ruoyi.miniapp.service.IResTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 资源类型Service业务层处理
 *
 * @author lobyliang
 * @date 2022-01-08
 */
@Service
public class ResTypeServiceImpl implements IResTypeService
{
    @Autowired
    private ResTypeMapper resTypeMapper;

    /**
     * 查询资源类型
     *
     * @param pkResTypeId 资源类型主键
     * @return 资源类型
     */
    @Override
    public ResType selectResTypeByPkResTypeId(Long pkResTypeId)
    {
        return resTypeMapper.selectResTypeByPkResTypeId(pkResTypeId);
    }

    /**
     * 查询资源类型列表
     *
     * @param resType 资源类型
     * @return 资源类型
     */
    @Override
    public List<ResType> selectResTypeList(ResType resType)
    {
        return resTypeMapper.selectResTypeList(resType);
    }

    @Override
    public List<ResType> getParentAllById(Long pkResTypeId){
        return resTypeMapper.getParentAllById(pkResTypeId);
    }

    /**
     * 新增资源类型
     *
     * @param resType 资源类型
     * @return 结果
     */
    @Override
    public int insertResType(ResType resType)
    {
        return resTypeMapper.insertResType(resType);
    }

    /**
     * 修改资源类型
     *
     * @param resType 资源类型
     * @return 结果
     */
    @Override
    public int updateResType(ResType resType)
    {
        return resTypeMapper.updateResType(resType);
    }

    /**
     * 批量删除资源类型
     *
     * @param pkResTypeIds 需要删除的资源类型主键
     * @return 结果
     */
    @Override
    public int deleteResTypeByPkResTypeIds(Long[] pkResTypeIds)
    {
        return resTypeMapper.deleteResTypeByPkResTypeIds(pkResTypeIds);
    }

    /**
     * 删除资源类型信息
     *
     * @param pkResTypeId 资源类型主键
     * @return 结果
     */
    @Override
    public int deleteResTypeByPkResTypeId(Long pkResTypeId)
    {
        return resTypeMapper.deleteResTypeByPkResTypeId(pkResTypeId);
    }
}