package com.ruoyi.miniapp.service.impl;

import java.util.List;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.miniapp.mapper.FranchiseeJoinPayRecordMapper;
import com.ruoyi.miniapp.domain.FranchiseeJoinPayRecord;
import com.ruoyi.miniapp.service.IFranchiseeJoinPayRecordService;

/**
 * 广告机加盟付款记录Service业务层处理
 *
 * @author lobyliang
 * @date 2022-01-14
 */
@Service
@DS("tdddb")
public class FranchiseeJoinPayRecordServiceImpl extends ServiceImpl<FranchiseeJoinPayRecordMapper, FranchiseeJoinPayRecord> implements IFranchiseeJoinPayRecordService {
    @Autowired
    private FranchiseeJoinPayRecordMapper franchiseeJoinPayRecordMapper;

    /**
     * 查询广告机加盟付款记录
     *
     * @param pkSn 广告机加盟付款记录主键
     * @return 广告机加盟付款记录
     */
    @Override
    public FranchiseeJoinPayRecord selectFranchiseeJoinPayRecordByPkSn(Long pkSn) {
        return franchiseeJoinPayRecordMapper.selectFranchiseeJoinPayRecordByPkSn(pkSn);
    }

    /**
     * 查询广告机加盟付款记录列表
     *
     * @param franchiseeJoinPayRecord 广告机加盟付款记录
     * @return 广告机加盟付款记录
     */
    @Override
    public List<FranchiseeJoinPayRecord> selectFranchiseeJoinPayRecordList(FranchiseeJoinPayRecord franchiseeJoinPayRecord) {
        return franchiseeJoinPayRecordMapper.selectFranchiseeJoinPayRecordList(franchiseeJoinPayRecord);
    }

    /**
     * 新增广告机加盟付款记录
     *
     * @param franchiseeJoinPayRecord 广告机加盟付款记录
     * @return 结果
     */
    @Override
    public int insertFranchiseeJoinPayRecord(FranchiseeJoinPayRecord franchiseeJoinPayRecord) {
        return franchiseeJoinPayRecordMapper.insertFranchiseeJoinPayRecord(franchiseeJoinPayRecord);
    }

    /**
     * 修改广告机加盟付款记录
     *
     * @param franchiseeJoinPayRecord 广告机加盟付款记录
     * @return 结果
     */
    @Override
    public int updateFranchiseeJoinPayRecord(FranchiseeJoinPayRecord franchiseeJoinPayRecord) {
        return franchiseeJoinPayRecordMapper.updateFranchiseeJoinPayRecord(franchiseeJoinPayRecord);
    }

    /**
     * 批量删除广告机加盟付款记录
     *
     * @param pkSns 需要删除的广告机加盟付款记录主键
     * @return 结果
     */
    @Override
    public int deleteFranchiseeJoinPayRecordByPkSns(Long[] pkSns) {
        return franchiseeJoinPayRecordMapper.deleteFranchiseeJoinPayRecordByPkSns(pkSns);
    }

    /**
     * 删除广告机加盟付款记录信息
     *
     * @param pkSn 广告机加盟付款记录主键
     * @return 结果
     */
    @Override
    public int deleteFranchiseeJoinPayRecordByPkSn(Long pkSn) {
        return franchiseeJoinPayRecordMapper.deleteFranchiseeJoinPayRecordByPkSn(pkSn);
    }
}
