package com.ruoyi.miniapp.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.miniapp.service.IAdmProcPlanItemService;
import com.ruoyi.miniapp.service.IAdmProcPlanService;
import com.ruoyi.miniapp.service.IFranSeatAgreementItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.miniapp.mapper.FranSeatAgreementMapper;
import com.ruoyi.miniapp.domain.FranSeatAgreement;
import com.ruoyi.miniapp.service.IFranSeatAgreementService;

/**
 * 加盟商购买点位订单Service业务层处理
 *
 * @author lobyliang
 * @date 2022-01-14
 */
@Service
@DS("tdddb")
public class FranSeatAgreementServiceImpl extends ServiceImpl<FranSeatAgreementMapper, FranSeatAgreement> implements IFranSeatAgreementService {
    @Autowired
    private FranSeatAgreementMapper franSeatAgreementMapper;
    @Autowired
    private IFranSeatAgreementItemService franSeatAgreementItemService;
    @Autowired
    private IAdmProcPlanService admProcPlanService;
    @Autowired
    private IAdmProcPlanItemService admProcPlanItemService;

    /**
     * 查询加盟商购买点位订单
     *
     * @param pkSeatCode 加盟商购买点位订单主键
     * @return 加盟商购买点位订单
     */
    @Override
    public FranSeatAgreement selectFranSeatAgreementByPkSeatCode(String pkSeatCode) {
        return franSeatAgreementMapper.selectFranSeatAgreementByPkSeatCode(pkSeatCode);
    }

    /**
     * 查询加盟商购买点位订单列表
     *
     * @param franSeatAgreement 加盟商购买点位订单
     * @return 加盟商购买点位订单
     */
    @Override
    public List<FranSeatAgreement> selectFranSeatAgreementList(FranSeatAgreement franSeatAgreement) {
        return franSeatAgreementMapper.selectFranSeatAgreementList(franSeatAgreement);
    }

    /**
     * 新增加盟商购买点位订单
     *
     * @param franSeatAgreement 加盟商购买点位订单
     * @return 结果
     */
    @Override
    public int insertFranSeatAgreement(FranSeatAgreement franSeatAgreement) {
        return franSeatAgreementMapper.insertFranSeatAgreement(franSeatAgreement);
    }

    /**
     * 修改加盟商购买点位订单
     *
     * @param franSeatAgreement 加盟商购买点位订单
     * @return 结果
     */
    @Override
    public int updateFranSeatAgreement(FranSeatAgreement franSeatAgreement) {
        return franSeatAgreementMapper.updateFranSeatAgreement(franSeatAgreement);
    }

    /**
     * 批量删除加盟商购买点位订单
     *
     * @param pkSeatCodes 需要删除的加盟商购买点位订单主键
     * @return 结果
     */
    @Override
    public int deleteFranSeatAgreementByPkSeatCodes(String[] pkSeatCodes) {
        return franSeatAgreementMapper.deleteFranSeatAgreementByPkSeatCodes(pkSeatCodes);
    }

    /**
     * 删除加盟商购买点位订单信息
     *
     * @param pkSeatCode 加盟商购买点位订单主键
     * @return 结果
     */
    @Override
    public int deleteFranSeatAgreementByPkSeatCode(String pkSeatCode) {
        return franSeatAgreementMapper.deleteFranSeatAgreementByPkSeatCode(pkSeatCode);
    }

    /**
     * 获取所有未支付，未取消订单占用的机位
     *
     * @return
     */
    @Override
    public List<String> findOccupyAdmSeatCodes() {
        LambdaQueryWrapper<FranSeatAgreement> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(FranSeatAgreement::getHasCancel, 0);
        wrapper.eq(FranSeatAgreement::getIsPayment, 0);
        wrapper.isNotNull(FranSeatAgreement::getAdmSeatsCodes);
        List<FranSeatAgreement> franSeatAgreements = baseMapper.selectList(wrapper);
        Set<String> set = new HashSet<>();
        for (FranSeatAgreement r : franSeatAgreements) {
            Set s = new HashSet(Arrays.asList(r.getAdmSeatsCodes().split(",")));
            set.addAll(s);
        }
        return new ArrayList<>(set);
    }

    @Override
    public Page listPage(Page<FranSeatAgreement> page, String unionid, Integer payState) {
        LambdaQueryWrapper<FranSeatAgreement> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(FranSeatAgreement::getFranchiseesUnionid, unionid);
//        wrapper.orderByAsc(FranSeatAgreement::getIsPayment);
        wrapper.orderByDesc(FranSeatAgreement::getCreateDate);
        if(payState != null){
            if(payState == 1){
                wrapper.eq(FranSeatAgreement::getIsPayment,1);
            }
            if(payState == 0){
                wrapper.eq(FranSeatAgreement::getIsPayment,0);
                wrapper.eq(FranSeatAgreement::getHasCancel,0);
            }
            if(payState == -1){
                wrapper.eq(FranSeatAgreement::getHasCancel,1);
            }
        }
        baseMapper.selectPage(page, wrapper);
        page.getRecords().forEach(r -> {
            if(r.getIsPayment() == 0){
                r.setPayEndTime(DateUtil.offsetMinute(r.getCreateDate(),30));
            }
            r.setItemList(franSeatAgreementItemService.findBySeatCode(r.getPkSeatCode()));
            r.setAdmProcPlan(admProcPlanService.findBySeatOrderCode(r.getPkSeatCode()));
            if(r.getAdmProcPlan() != null){
                r.getAdmProcPlan().setAdmProcPlanItemList(admProcPlanItemService.findByPlanCode(r.getAdmProcPlan().getPkAdmProcCode()));
            }
        });
        return page;
    }

    @Override
    public void updateOutTradeNo(String pkSeatCode, String outTradeNo){
        LambdaUpdateWrapper<FranSeatAgreement> wrapper = Wrappers.lambdaUpdate();
        wrapper.eq(FranSeatAgreement::getPkSeatCode,pkSeatCode);
        wrapper.set(FranSeatAgreement::getOutTradeNo,outTradeNo);
        baseMapper.update(null,wrapper);
    }

    @Override
    public List<FranSeatAgreement> timeOutUnPay(){
        LambdaQueryWrapper<FranSeatAgreement> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(FranSeatAgreement::getIsPayment,0);
        wrapper.eq(FranSeatAgreement::getHasCancel,0);
        DateTime d = DateUtil.offsetMinute(DateTime.now(),-30);
        wrapper.lt(FranSeatAgreement::getCreateDate,d);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public void batchCancel(List<String> ids){
        LambdaUpdateWrapper<FranSeatAgreement> wrapper = Wrappers.lambdaUpdate();
        wrapper.in(FranSeatAgreement::getPkSeatCode,ids);
        wrapper.set(FranSeatAgreement::getHasCancel,1);
        baseMapper.update(null,wrapper);
    }

    @Override
    public void batchCancelUnPayOrder(){
        List<FranSeatAgreement> list = this.timeOutUnPay();
        if(list.size() > 0){
            List<String> ids = list.stream().map(FranSeatAgreement::getPkSeatCode).collect(Collectors.toList());
            this.batchCancel(ids);
        }
    }

    @Override
    public FranSeatAgreement findByOutTradeNo(String outTradeNo){
        LambdaQueryWrapper<FranSeatAgreement> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(FranSeatAgreement::getOutTradeNo,outTradeNo);
        return baseMapper.selectOne(wrapper);
    }

    @Override
    public void updatePayStatus(String pkSeatCode){
        LambdaUpdateWrapper<FranSeatAgreement> wrapper = Wrappers.lambdaUpdate();
        wrapper.eq(FranSeatAgreement::getPkSeatCode,pkSeatCode);
        wrapper.set(FranSeatAgreement::getIsPayment,1);
        wrapper.set(FranSeatAgreement::getPayTime,DateTime.now());
        baseMapper.update(null,wrapper);
    }

}
