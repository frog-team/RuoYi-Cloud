package com.ruoyi.miniapp.service.impl;

import java.util.List;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.miniapp.mapper.FraFranchiseesMapper;
import com.ruoyi.miniapp.domain.FraFranchisees;
import com.ruoyi.miniapp.service.IFraFranchiseesService;

/**
 * 加盟商，和金额相关的信息，另建Service业务层处理
 *
 * @author lobyliang
 * @date 2022-01-14
 */
@Service
@DS("tdddb")
public class FraFranchiseesServiceImpl extends ServiceImpl<FraFranchiseesMapper, FraFranchisees> implements IFraFranchiseesService {
    @Autowired
    private FraFranchiseesMapper fraFranchiseesMapper;

    /**
     * 查询加盟商，和金额相关的信息，另建
     *
     * @param userId 加盟商，和金额相关的信息，另建主键
     * @return 加盟商，和金额相关的信息，另建
     */
    @Override
    public FraFranchisees selectFraFranchiseesByUserId(Long userId) {
        return fraFranchiseesMapper.selectFraFranchiseesByUserId(userId);
    }

    /**
     * 查询加盟商，和金额相关的信息，另建列表
     *
     * @param fraFranchisees 加盟商，和金额相关的信息，另建
     * @return 加盟商，和金额相关的信息，另建
     */
    @Override
    public List<FraFranchisees> selectFraFranchiseesList(FraFranchisees fraFranchisees) {
        return fraFranchiseesMapper.selectFraFranchiseesList(fraFranchisees);
    }

    /**
     * 新增加盟商，和金额相关的信息，另建
     *
     * @param fraFranchisees 加盟商，和金额相关的信息，另建
     * @return 结果
     */
    @Override
    public int insertFraFranchisees(FraFranchisees fraFranchisees) {
        return fraFranchiseesMapper.insertFraFranchisees(fraFranchisees);
    }

    /**
     * 修改加盟商，和金额相关的信息，另建
     *
     * @param fraFranchisees 加盟商，和金额相关的信息，另建
     * @return 结果
     */
    @Override
    public int updateFraFranchisees(FraFranchisees fraFranchisees) {
        return fraFranchiseesMapper.updateFraFranchisees(fraFranchisees);
    }

    /**
     * 批量删除加盟商，和金额相关的信息，另建
     *
     * @param userIds 需要删除的加盟商，和金额相关的信息，另建主键
     * @return 结果
     */
    @Override
    public int deleteFraFranchiseesByUserIds(Long[] userIds) {
        return fraFranchiseesMapper.deleteFraFranchiseesByUserIds(userIds);
    }

    /**
     * 删除加盟商，和金额相关的信息，另建信息
     *
     * @param userId 加盟商，和金额相关的信息，另建主键
     * @return 结果
     */
    @Override
    public int deleteFraFranchiseesByUserId(Long userId) {
        return fraFranchiseesMapper.deleteFraFranchiseesByUserId(userId);
    }
}
