package com.ruoyi.miniapp.service;

import java.awt.image.BufferedImage;

/**
 * @author lobyliang
 */
public interface IBufferdImageService {
    BufferedImage getBufferImage(String src);
}
