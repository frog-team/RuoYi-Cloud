package com.ruoyi.miniapp.service.impl;

import com.ruoyi.miniapp.domain.DiscountsScope;
import com.ruoyi.miniapp.mapper.TsalesmanMapper;
import com.ruoyi.miniapp.service.ITsalesmanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TsalesmanServiceImpl implements ITsalesmanService {

    @Autowired
    private TsalesmanMapper tsalesmanMapper;

    @Override
    public int insertTsalesman(DiscountsScope obj) {
        int count = tsalesmanMapper.insertTsalesman(obj);
        return count;
    }
}
