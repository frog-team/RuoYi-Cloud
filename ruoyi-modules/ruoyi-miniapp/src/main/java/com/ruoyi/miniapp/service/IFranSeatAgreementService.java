package com.ruoyi.miniapp.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.miniapp.domain.FranSeatAgreement;

/**
 * 加盟商购买点位订单Service接口
 *
 * @author lobyliang
 * @date 2022-01-14
 */
public interface IFranSeatAgreementService extends IService<FranSeatAgreement> {
    /**
     * 查询加盟商购买点位订单
     *
     * @param pkSeatCode 加盟商购买点位订单主键
     * @return 加盟商购买点位订单
     */
    public FranSeatAgreement selectFranSeatAgreementByPkSeatCode(String pkSeatCode);

    /**
     * 查询加盟商购买点位订单列表
     *
     * @param franSeatAgreement 加盟商购买点位订单
     * @return 加盟商购买点位订单集合
     */
    public List<FranSeatAgreement> selectFranSeatAgreementList(FranSeatAgreement franSeatAgreement);

    /**
     * 新增加盟商购买点位订单
     *
     * @param franSeatAgreement 加盟商购买点位订单
     * @return 结果
     */
    public int insertFranSeatAgreement(FranSeatAgreement franSeatAgreement);

    /**
     * 修改加盟商购买点位订单
     *
     * @param franSeatAgreement 加盟商购买点位订单
     * @return 结果
     */
    public int updateFranSeatAgreement(FranSeatAgreement franSeatAgreement);

    /**
     * 批量删除加盟商购买点位订单
     *
     * @param pkSeatCodes 需要删除的加盟商购买点位订单主键集合
     * @return 结果
     */
    public int deleteFranSeatAgreementByPkSeatCodes(String[] pkSeatCodes);

    /**
     * 删除加盟商购买点位订单信息
     *
     * @param pkSeatCode 加盟商购买点位订单主键
     * @return 结果
     */
    public int deleteFranSeatAgreementByPkSeatCode(String pkSeatCode);

    List<String> findOccupyAdmSeatCodes();

    Page listPage(Page<FranSeatAgreement> page, String unionid, Integer payState);

    void updateOutTradeNo(String pkSeatCode, String outTradeNo);

    List<FranSeatAgreement> timeOutUnPay();

    void batchCancel(List<String> ids);

    void batchCancelUnPayOrder();

    FranSeatAgreement findByOutTradeNo(String outTradeNo);

    void updatePayStatus(String pkSeatCode);
}
