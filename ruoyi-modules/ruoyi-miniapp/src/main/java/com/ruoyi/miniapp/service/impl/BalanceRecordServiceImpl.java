package com.ruoyi.miniapp.service.impl;

import cn.hutool.core.date.DateTime;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.miniapp.mapper.BalanceRecordMapper;
import com.ruoyi.miniapp.service.IBalanceRecordService;
import com.ruoyi.system.api.domain.base.BalanceRecord;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@DS("tdddb")
public class BalanceRecordServiceImpl extends ServiceImpl<BalanceRecordMapper, BalanceRecord> implements IBalanceRecordService {

    @Override
    public void save(Long userId, BigDecimal amount, BigDecimal balance, String action, String remark){
        BalanceRecord r = new BalanceRecord();
        r.setUserId(userId);
        r.setAmount(amount);
        r.setBalance(balance);
        r.setAction(action);
        r.setRemark(remark);
        r.setCreateTime(DateTime.now());
        this.save(r);
    }

}
