package com.ruoyi.miniapp.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.ruoyi.miniapp.domain.TrainingAddress;
import com.ruoyi.miniapp.dto.TrainingAddressDto;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 表名：TRAINING_ADDRESS 	 说明：培训地址 
 */

@Mapper
@Repository
@DS("tdddb")
public interface TrainingAddressMapper {

	/**
	 *  查询培训地址
	 */
	public List<TrainingAddress> getTrainingAddressInfo(String divisionCode);


	/**
	 *  查询培训场次
	 */
	public List<TrainingAddress> getTrainingAddressInfoBytrainingName(String trainingName);

	/**
	 *  运维后台查询市
	 */
	public List<TrainingAddress> selCityList();

	/**
	 * 运维后台
	 * #根据市级编码查询下面有多少培训基地
	 */
	public List<String> selTrainingNameList(String fkDivisionCode);

	/**
	 * 运维后台
	 * #根据培训基地名称查询场次
	 */
	public List<TrainingAddress> selTrainingAddressList(String teainingName);

	/**
	 *  获取培训基地场次（次数）
	 */
	public Integer getTrainingCountBytrainingName(String trainingName);

	/**
	 *  返回报名用户id
	 */
	public List<String> selApplyIdListA(Integer id);
	/**
	 *  返回报名用户id
	 */
	public List<String> selApplyIdListB(Integer id);

	/**
	 *  返回报名用户id
	 */
	public List<Long> selStudentListA(Integer id);

	/**
	 *  返回报名用户id
	 */
	public List<Long> selStudentListB(Integer id);

	/**
	 *  剩余名额+ -
	 */
	public void updateSurplusPlacesA(Integer id);

	/**
	 *  剩余名额+ -
	 */
	public void updateSurplusPlacesB(Integer id);

	/**
	 * 增加
	 * @param obj
	 * @return
	 */
	public int insertTrainingAddress(TrainingAddress obj) ;

	/**
	 * 查询count
	 * @param obj
	 * @return
	 */
	public int getTrainingAddressCount(TrainingAddress obj) ;

	/**
	 * 查询List
	 * @param obj
	 * @return
	 */
	public List<TrainingAddress> getTrainingAddressList(TrainingAddress obj) ;

	/**
	 * 主键查询
	 * @param id
	 * @return
	 */
	public TrainingAddressDto getTrainingAddressByKey(Integer id) ;

	/**
	 * 多主键查询
	 * @param keys
	 * @return
	 */
	public List<TrainingAddress> getTrainingAddressByKeys(List<Long> keys) ;

	/**
	 * 修改
	 * @param obj
	 * @return
	 */
	public int updateTrainingAddress(TrainingAddress obj) ;

	/**
	 * 删除
	 * @param key
	 * @return
	 */
	public int deleteTrainingAddress(Integer key) ;

	/**
	 * 批量增加
	 * @param objList
	 * @return
	 */
	public int batchInsertTrainingAddress(List<TrainingAddress> objList) ;

	/**
	 * 批量修改
	 * @param objList
	 * @return
	 */
	public int batchUpdateTrainingAddress(List<TrainingAddress> objList) ;

	/**
	 * 批量删除
	 * @param keys
	 * @return
	 */
	public int batchDeleteTrainingAddress(List<Long> keys) ;

}