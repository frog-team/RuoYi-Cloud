package com.ruoyi.miniapp.service;

import com.ruoyi.miniapp.domain.DiscountsScope;
import com.ruoyi.miniapp.domain.Tsalesman;

public interface ITsalesmanService {

    public int insertTsalesman(DiscountsScope obj);
}
