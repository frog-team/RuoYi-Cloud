package com.ruoyi.miniapp.domain;

import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 楼栋信息对象 bas_buildings
 *
 * @author lobyliang
 * @date 2022-01-12
 */
@ApiModel("bas_buildings(楼栋信息)")
@Data
@ToString
public class BasBuildings {
    private static final long serialVersionUID = 1L;

    /**
     * 按照小区code继续排，+3位顺序码
     */
    @ApiModelProperty("按照小区code继续排，+3位顺序码")
    @TableId(value = "pk_building_code",type = IdType.INPUT)
    private String pkBuildingCode;

    /**
     * 楼栋号
     */
    @ApiModelProperty("楼栋号")
    @Excel(name = "楼栋号")
    private String buildingNo;

    /**
     * 楼层数量
     */
    @ApiModelProperty("楼层数量")
    @Excel(name = "楼层数量")
    private String storeyAmount;

    /**
     * 每层住户
     */
    @ApiModelProperty("每层住户")
    @Excel(name = "每层住户")
    private String familyPerFloor;

    /**
     * 大概楼栋人口
     */
    @ApiModelProperty("大概楼栋人口")
    @Excel(name = "大概楼栋人口")
    private String generalPopulation;

    /**
     * 经度
     */
    @ApiModelProperty("经度")
    @Excel(name = "经度")
    private Long longitude;

    /**
     * 维度
     */
    @ApiModelProperty("维度")
    @Excel(name = "维度")
    private Long latitude;

    /**
     * 小区的code码
     */
    @ApiModelProperty("小区的code码")
    @Excel(name = "小区的code码")
    private String fkVillageCode;

    /**
     * 入住率
     */
    @ApiModelProperty("入住率")
    @Excel(name = "入住率")
    private BigDecimal occupancyRate;

    @TableField(exist = false)
    @ApiModelProperty("机位信息")
    private List<AdmSeats> admSeatsList;

}
