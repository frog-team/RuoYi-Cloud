package com.ruoyi.miniapp.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 生产计划明细(一台机器一条数据)对象 adm_proc_plan_item
 *
 * @author lobyliang
 * @date 2022-01-14
 */
@ApiModel("adm_proc_plan_item(生产计划明细(一台机器一条数据))")
@Data
@ToString
public class AdmProcPlanItem {
    private static final long serialVersionUID = 1L;

    /**
     * 出厂编号（产品CPU编号，或其他编码规则）需要一个编码生产服务。pk_adm_proc_code+序号
     */
    @ApiModelProperty("出厂编号（产品CPU编号，或其他编码规则）需要一个编码生产服务。pk_adm_proc_code+序号")
    @TableId(value = "product_serial_code" , type = IdType.ASSIGN_UUID)
    private String productSerialCode;

    /**
     * 生产计划id主键
     */
    @ApiModelProperty("生产计划id主键")
    @Excel(name = "生产计划id主键")
    private String fkAdmProcCode;

    /**
     * 安装计划code主键
     */
    @ApiModelProperty("安装计划code主键")
    @Excel(name = "安装计划code主键")
    private String fkAdmPlanCode;

    /**
     * 对应小区code
     */
    @ApiModelProperty("对应小区code")
    @Excel(name = "对应小区code")
    private String fkVillageCode;

    /**
     * 广告机型号
     */
    @ApiModelProperty("广告机型号")
    @Excel(name = "广告机型号")
    private String fkVdmModelId;

    /**
     * 投资商id
     */
    @ApiModelProperty("投资商id")
    @Excel(name = "投资商id")
    private Long fkFranchiseeId;

    /**
     * 对应（发货单/签收单）主键
     */
    @ApiModelProperty("对应（发货单/签收单）主键")
    @Excel(name = "对应" , readConverterExp = "发=货单/签收单")
    private String fkDevCode;

    /**
     * 产品名称
     */
    @ApiModelProperty("产品名称")
    @Excel(name = "产品名称")
    private String productName;

    /**
     * 质检员姓名
     */
    @ApiModelProperty("质检员姓名")
    @Excel(name = "质检员姓名")
    private String qualityInspector;

    /**
     * 固件版本号
     */
    @ApiModelProperty("固件版本号")
    @Excel(name = "固件版本号")
    private String firmwareVersion;

    /**
     * 合格证编号
     */
    @ApiModelProperty("合格证编号")
    @Excel(name = "合格证编号")
    private String certificateNo;

    /**
     * 生产计划创建时间
     */
    @ApiModelProperty("生产计划创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "生产计划创建时间" , width = 30, dateFormat = "yyyy-MM-dd")
    private Date procCreateDate;

    /**
     * 开始生产时间
     */
    @ApiModelProperty("开始生产时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始生产时间" , width = 30, dateFormat = "yyyy-MM-dd")
    private Date procStartDate;

    /**
     * 生产完成时间
     */
    @ApiModelProperty("生产完成时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "生产完成时间" , width = 30, dateFormat = "yyyy-MM-dd")
    private Date procFinishDate;

    /**
     * 安装发货时间
     */
    @ApiModelProperty("安装发货时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "安装发货时间" , width = 30, dateFormat = "yyyy-MM-dd")
    private Date deployDeliveryDate;

    /**
     * 收货时间
     */
    @ApiModelProperty("收货时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "收货时间" , width = 30, dateFormat = "yyyy-MM-dd")
    private Date deployArrivalDate;

    /**
     * 安装完成时间
     */
    @ApiModelProperty("安装完成时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "安装完成时间" , width = 30, dateFormat = "yyyy-MM-dd")
    private Date deployFinishDate;

    /**
     * 该广告机主板code
     */
    @ApiModelProperty("该广告机主板code")
    @Excel(name = "该广告机主板code")
    private String admBoardCode;

    /**
     * 是否需要生产 0=不需要,1=需要
     */
    private Integer isProduction;

}
