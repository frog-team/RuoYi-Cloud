package com.ruoyi.miniapp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.api.domain.base.InvoiceInfo;

public interface IInvoiceInfoService extends IService<InvoiceInfo> {
}
