package com.ruoyi.miniapp.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.miniapp.domain.AdmProcPlanItem;

/**
 * 生产计划明细(一台机器一条数据)Service接口
 *
 * @author lobyliang
 * @date 2022-01-14
 */
public interface IAdmProcPlanItemService extends IService<AdmProcPlanItem> {
    /**
     * 查询生产计划明细(一台机器一条数据)
     *
     * @param productSerialCode 生产计划明细(一台机器一条数据)主键
     * @return 生产计划明细(一台机器一条数据)
     */
    public AdmProcPlanItem selectAdmProcPlanItemByProductSerialCode(String productSerialCode);

    /**
     * 查询生产计划明细(一台机器一条数据)列表
     *
     * @param admProcPlanItem 生产计划明细(一台机器一条数据)
     * @return 生产计划明细(一台机器一条数据)集合
     */
    public List<AdmProcPlanItem> selectAdmProcPlanItemList(AdmProcPlanItem admProcPlanItem);

    /**
     * 新增生产计划明细(一台机器一条数据)
     *
     * @param admProcPlanItem 生产计划明细(一台机器一条数据)
     * @return 结果
     */
    public int insertAdmProcPlanItem(AdmProcPlanItem admProcPlanItem);

    /**
     * 修改生产计划明细(一台机器一条数据)
     *
     * @param admProcPlanItem 生产计划明细(一台机器一条数据)
     * @return 结果
     */
    public int updateAdmProcPlanItem(AdmProcPlanItem admProcPlanItem);

    /**
     * 批量删除生产计划明细(一台机器一条数据)
     *
     * @param productSerialCodes 需要删除的生产计划明细(一台机器一条数据)主键集合
     * @return 结果
     */
    public int deleteAdmProcPlanItemByProductSerialCodes(String[] productSerialCodes);

    /**
     * 删除生产计划明细(一台机器一条数据)信息
     *
     * @param productSerialCode 生产计划明细(一台机器一条数据)主键
     * @return 结果
     */
    public int deleteAdmProcPlanItemByProductSerialCode(String productSerialCode);

    List<AdmProcPlanItem> findByPlanCode(String code);
}
