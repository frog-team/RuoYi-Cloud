package com.ruoyi.miniapp.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.miniapp.domain.AdmVillageStock;
import com.ruoyi.miniapp.service.IAdmVillageStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 广告机小区库存信息Controller
 *
 * @author lobyliang
 * @date 2022-01-12
 */
@RestController
@RequestMapping("admVillageStock")
public class AdmVillageStockController extends BaseController {
    @Autowired
    private IAdmVillageStockService admVillageStockService;

    /**
     * 查询广告机小区库存信息列表
     */
    @RequiresPermissions("dao:stock:list")
    @GetMapping("/list")
    public TableDataInfo list(AdmVillageStock admVillageStock) {
        startPage();
        List<AdmVillageStock> list = admVillageStockService.selectAdmVillageStockList(admVillageStock);
        return getDataTable(list);
    }

    /**
     * 导出广告机小区库存信息列表
     */
    @RequiresPermissions("dao:stock:export")
    @Log(title = "广告机小区库存信息" , businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AdmVillageStock admVillageStock) throws IOException {
        List<AdmVillageStock> list = admVillageStockService.selectAdmVillageStockList(admVillageStock);
        ExcelUtil<AdmVillageStock> util = new ExcelUtil<AdmVillageStock>(AdmVillageStock.class);
        util.exportExcel(response, list, "广告机小区库存信息数据");
    }

    /**
     * 获取广告机小区库存信息详细信息
     */
    @RequiresPermissions("dao:stock:query")
    @GetMapping(value = "/{pkStockId}")
    public AjaxResult getInfo(@PathVariable("pkStockId") Long pkStockId) {
        return AjaxResult.success(admVillageStockService.selectAdmVillageStockByPkStockId(pkStockId));
    }

    /**
     * 新增广告机小区库存信息
     */
    @RequiresPermissions("dao:stock:add")
    @Log(title = "广告机小区库存信息" , businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AdmVillageStock admVillageStock) {
        return toAjax(admVillageStockService.insertAdmVillageStock(admVillageStock));
    }

    /**
     * 修改广告机小区库存信息
     */
    @RequiresPermissions("dao:stock:edit")
    @Log(title = "广告机小区库存信息" , businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AdmVillageStock admVillageStock) {
        return toAjax(admVillageStockService.updateAdmVillageStock(admVillageStock));
    }

    /**
     * 删除广告机小区库存信息
     */
    @RequiresPermissions("dao:stock:remove")
    @Log(title = "广告机小区库存信息" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{pkStockIds}")
    public AjaxResult remove(@PathVariable Long[] pkStockIds) {
        return toAjax(admVillageStockService.deleteAdmVillageStockByPkStockIds(pkStockIds));
    }
}
