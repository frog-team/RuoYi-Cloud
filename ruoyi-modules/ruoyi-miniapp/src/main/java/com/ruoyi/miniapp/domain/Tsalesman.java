package com.ruoyi.miniapp.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@TableName("t_salesman")
public class Tsalesman {

    /**
     * 主键
     */
    private Integer id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 折扣起始范围
     */
    private Integer beginGrossPrice;

    /**
     * 总价结束范围
     */
    private Integer endGrossPrice;

    /**
     * 折扣
     */
    private Double discount;

    /**
     * 折扣类型1=屏幕2=袋身
     */
    private Integer type;

}
