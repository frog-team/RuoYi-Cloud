package com.ruoyi.miniapp.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author lobyliang
 */
@Component
@Data
@ConfigurationProperties(prefix = BucketNameConfig.PREFIX )
public class BucketNameConfig {

    public static  final String PREFIX ="bucket-names";
    String advRes;
    String bagRemoteBaseUrl;
    String scrRes;
    String srcRemoteBaseUrl;

    public String getBucketName(Long fkResTypeId){
        if(fkResTypeId>=200L && fkResTypeId<300L)
        {
            return scrRes;
        }
        return advRes;
    }

    public String getBaseUrl(Long fkResTypeId){
        if(fkResTypeId>=200L && fkResTypeId<300L)
        {
            return srcRemoteBaseUrl;
        }
        return bagRemoteBaseUrl;
    }

    //    public String getBucketName(TempType templateType){
//        switch(templateType)
//        {
//            case OneByOne:return bagImage1;
//            case OneByTwo:return bagImage2;
//            case TwoByTwo:return bagImage4;
//            case ThreeByTwo:return bagImage6;
//            case FourByTwo:return bagImage8;

//            case FiveByTwo:return bagImage10;
//            case SixByTwo:return bagImage12;
//            case SevenByTwo:return bagImage14;
//            case EightByTwo:return bagImage16;
//            case WholePage:
//            default:return bagImage100;
//        }
//    }
}
