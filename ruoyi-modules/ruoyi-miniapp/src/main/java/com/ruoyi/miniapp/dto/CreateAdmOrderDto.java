package com.ruoyi.miniapp.dto;

import lombok.Data;

import java.util.List;


@Data
public class CreateAdmOrderDto {

    private List<String> admSeatsCodes;

    private Integer admModelId;

    private String inviteCode;

}
