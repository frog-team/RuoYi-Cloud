package com.ruoyi.miniapp.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.system.api.model.RealNameAuthInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 资源条目对象 res_item
 * 
 * @author lobyliang
 * @date 2022-01-07
 */
@ApiModel("res_item(资源条目)")
public class ResItem implements Serializable// extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty("主键")
    private String pkResId;

    /** 资源id(res_type表ID) */
    @ApiModelProperty("资源id(res_type表ID)")
    @Excel(name = "资源id(res_type表ID)")
    private Long fkResTypeId;

    /** 带文件名的完整路径 */
    @ApiModelProperty("带文件名的完整路径")
    @Excel(name = "带文件名的完整路径")
    private String fileFullPath;

    /** 原始文件名 */
    @ApiModelProperty("原始文件名")
    @Excel(name = "原始文件名")
    private String originFileName;

    /** 文件大小,单位:m */
    @ApiModelProperty("文件大小,单位:m")
    @Excel(name = "文件大小,单位:m")
    private Float size;

    /** 压缩类型，0：不压缩，1.zip，2.rar。3.gz\nzip_type */
    @ApiModelProperty("压缩类型，0：不压缩，1.zip，2.rar。3.gz\nzip_type")
    @Excel(name = "压缩类型，0：不压缩，1.zip，2.rar。3.gz\nzip_type")
    private Integer dtZipTypeNo;

    /** 上传人用户ID */
    @ApiModelProperty("上传人用户ID")
    @Excel(name = "上传人用户ID")
    private Long fkOwnerUid;

    /** 资源类型(t_code_dict表) */
    @ApiModelProperty("资源类型(t_code_dict表)")
    @Excel(name = "资源类型(t_code_dict表)")
    private Integer dtFileTypeNo;

    /** 图片宽度 */
    @ApiModelProperty("图片宽度")
    @Excel(name = "图片宽度")
    private Integer imgWidth;

    /** 图片长度 */
    @ApiModelProperty("图片长度")
    @Excel(name = "图片长度")
    private Integer imgHeight;

    /** 图片位深度(一个像素储存多少颜色) */
    @ApiModelProperty("图片位深度(一个像素储存多少颜色)")
    @Excel(name = "图片位深度(一个像素储存多少颜色)")
    private Integer imgBitdepth;

    /** 图片dpi(水平，垂直分辨率) */
    @ApiModelProperty("图片dpi(水平，垂直分辨率)")
    @Excel(name = "图片dpi(水平，垂直分辨率)")
    private Integer imgDpi;

    /** 审核状态1=未审核2=已审核3=已驳回 */
    @ApiModelProperty("审核状态1=未审核2=已审核3=已驳回")
    @Excel(name = "审核状态1=未审核2=已审核3=已驳回")
    private Integer checkStatus;

    /** 审核人 */
    @ApiModelProperty("审核人")
    @Excel(name = "审核人")
    private String auditor;

    /** 审核备注 */
    @ApiModelProperty("审核备注")
    @Excel(name = "审核备注")
    private String checkRemark;

    /** 审核时间 */
    @ApiModelProperty("审核时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "审核时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime reviewTime;

    /** 视频时长 */
    @ApiModelProperty("视频时长")
    @Excel(name = "视频时长")
    private Integer videoTime;

    /** 文字广告 */
    @ApiModelProperty("文字广告")
    @Excel(name = "文字广告")
    private String textads;

    /** 广告名称 */
    @ApiModelProperty("广告名称")
    @Excel(name = "广告名称")
    private String resName;

    /** 图片标签 */
    @ApiModelProperty("图片标签")
    @Excel(name = "图片标签")
    private String imgTag;

    public Boolean getIsDel(){
        return isDel;
    }

    public void setIsDel(Boolean del){
        isDel = del;
    }

    /** 图片标签 */
    @ApiModelProperty("删除标志")
    @Excel(name = "删除标志")
    private Boolean isDel;

    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    /** 创建时间*/
    @ApiModelProperty("创建时间")
    @Excel(name = "创建时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    private String remark;

    /**
     * 实名认证信息
     */
    private RealNameAuthInfo realNameAuthInfo;

    public RealNameAuthInfo getRealNameAuthInfo() {
        return realNameAuthInfo;
    }

    public void setRealNameAuthInfo(RealNameAuthInfo realNameAuthInfo) {
        this.realNameAuthInfo = realNameAuthInfo;
    }
    public LocalDateTime getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(LocalDateTime dateTime)
    {
        createTime = dateTime;
    }

    public String getRemark() {
        return this.remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setPkResId(String pkResId) 
    {
        this.pkResId = pkResId;
    }

    public String getPkResId()
    {
        return pkResId;
    }
    public void setFkResTypeId(Long fkResTypeId) 
    {
        this.fkResTypeId = fkResTypeId;
    }

    public Long getFkResTypeId() 
    {
        return fkResTypeId;
    }
    public void setFileFullPath(String fileFullPath) 
    {
        this.fileFullPath = fileFullPath;
    }

    public String getFileFullPath() 
    {
        return fileFullPath;
    }
    public void setOriginFileName(String originFileName) 
    {
        this.originFileName = originFileName;
    }

    public String getOriginFileName() 
    {
        return originFileName;
    }
    public void setSize(Float size)
    {
        this.size = size;
    }

    public Float getSize()
    {
        return size;
    }
    public void setDtZipTypeNo(Integer dtZipTypeNo) 
    {
        this.dtZipTypeNo = dtZipTypeNo;
    }

    public Integer getDtZipTypeNo() 
    {
        return dtZipTypeNo;
    }
    public void setFkOwnerUid(Long fkOwnerUid)
    {
        this.fkOwnerUid = fkOwnerUid;
    }

    public Long getFkOwnerUid()
    {
        return fkOwnerUid;
    }
    public void setDtFileTypeNo(Integer dtFileTypeNo) 
    {
        this.dtFileTypeNo = dtFileTypeNo;
    }

    public Integer getDtFileTypeNo() 
    {
        return dtFileTypeNo;
    }
    public void setImgWidth(Integer imgWidth)
    {
        this.imgWidth = imgWidth;
    }

    public Integer getImgWidth()
    {
        return imgWidth;
    }
    public void setImgHeight(Integer imgHeight)
    {
        this.imgHeight = imgHeight;
    }

    public Integer getImgHeight()
    {
        return imgHeight;
    }
    public void setImgBitdepth(Integer imgBitdepth) 
    {
        this.imgBitdepth = imgBitdepth;
    }

    public Integer getImgBitdepth() 
    {
        return imgBitdepth;
    }
    public void setImgDpi(Integer imgDpi) 
    {
        this.imgDpi = imgDpi;
    }

    public Integer getImgDpi() 
    {
        return imgDpi;
    }
    public void setCheckStatus(Integer checkStatus) 
    {
        this.checkStatus = checkStatus;
    }

    public Integer getCheckStatus() 
    {
        return checkStatus;
    }
    public void setAuditor(String auditor) 
    {
        this.auditor = auditor;
    }

    public String getAuditor() 
    {
        return auditor;
    }
    public void setCheckRemark(String checkRemark) 
    {
        this.checkRemark = checkRemark;
    }

    public String getCheckRemark() 
    {
        return checkRemark;
    }
    public void setReviewTime(LocalDateTime reviewTime) 
    {
        this.reviewTime = reviewTime;
    }

    public LocalDateTime getReviewTime() 
    {
        return reviewTime;
    }
    public void setVideoTime(Integer videoTime)
    {
        this.videoTime = videoTime;
    }

    public Integer getVideoTime()
    {
        return videoTime;
    }
    public void setTextads(String textads) 
    {
        this.textads = textads;
    }

    public String getTextads() 
    {
        return textads;
    }
    public void setResName(String advName)
    {
        this.resName = advName;
    }

    public String getResName()
    {
        return resName;
    }
    public void setImgTag(String imgTag) 
    {
        this.imgTag = imgTag;
    }

    public String getImgTag() 
    {
        return imgTag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("pkResId", getPkResId())
            .append("fkResTypeId", getFkResTypeId())
            .append("fileFullPath", getFileFullPath())
            .append("originFileName", getOriginFileName())
            .append("size", getSize())
            .append("createTime", getCreateTime())
            .append("dtZipTypeNo", getDtZipTypeNo())
            .append("fkOwnerUid", getFkOwnerUid())
            .append("dtFileTypeNo", getDtFileTypeNo())
            .append("imgWidth", getImgWidth())
            .append("imgHeight", getImgHeight())
            .append("imgBitdepth", getImgBitdepth())
            .append("imgDpi", getImgDpi())
            .append("checkStatus", getCheckStatus())
            .append("auditor", getAuditor())
            .append("checkRemark", getCheckRemark())
            .append("reviewTime", getReviewTime())
            .append("videoTime", getVideoTime())
            .append("remark", getRemark())
            .append("textads", getTextads())
            .append("advName", getResName())
            .append("imgTag", getImgTag())
            .toString();
    }
}
