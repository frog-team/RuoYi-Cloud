package com.ruoyi.miniapp.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.ruoyi.miniapp.domain.DiscountsScope;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
@DS("tdddb")
public interface DiscountsScopeMapper {

    public List<DiscountsScope> getDiscountsScopeBytype(Integer type);

    public DiscountsScope getDiscountsScopeByid(Integer id);
}
