package com.ruoyi.miniapp.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.ruoyi.miniapp.domain.BasMaintainerApply;
import com.ruoyi.miniapp.dto.ApplyUserDto;
import com.ruoyi.miniapp.dto.ArgumentsDto;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 表名：BAS_MAINTAINER_APPLY 	 说明：用户报名表 
 */

@Mapper
@Repository
@DS("tdddb")
public interface BasMaintainerApplyMapper {

	/**
	 * 签到
	 * @param obj
	 */
	public Integer updateBasMaintainerApplyByUserId(BasMaintainerApply obj);

	/**
	 * 运维后台
	 * #根据场次查询用户信息
	 */
	public List<ApplyUserDto> getUserInfo(ArgumentsDto obj);

	/**
	 * 运维后台
	 * #根据场次查询用户信息Count 总数
	 */
	public Integer getUserInfoCount(ArgumentsDto obj);

	/**
	 * 获取最大序列码
	 * @param key
	 * @return
	 */
	public String getCertificateNo(Integer key);

	/**
	 * 根据userid查询用户信息
	 * @param Userid
	 * @return
	 */
	public BasMaintainerApply getBasMaintainerApplyByUserId(Long Userid);

	/**
	 * 根据userid查询用户信息，未付款
	 * @param Userid
	 * @return
	 */
	BasMaintainerApply getBasMaintainerApplyByUserIdNoPay(Long Userid);

	/**
	 * 增加
	 * @param obj
	 * @return
	 */
	public int insertBasMaintainerApply(BasMaintainerApply obj) ;

	/**
	 * 查询count
	 * @param obj
	 * @return
	 */
	public int getBasMaintainerApplyCount(BasMaintainerApply obj) ;

	/**
	 * 查询List
	 * @param obj
	 * @return
	 */
	public List<BasMaintainerApply> getBasMaintainerApplyList(BasMaintainerApply obj) ;

	/**
	 * 主键查询
	 * @param applyId
	 * @return
	 */
	public BasMaintainerApply getBasMaintainerApplyByKey(String applyId) ;

	/**
	 * 主键查询
	 * @param orderNum
	 * @return
	 */
	BasMaintainerApply getBasMaintainerApplyByOrderNum(String orderNum);
	/**
	 * 多主键查询
	 * @param keys
	 * @return
	 */
	public List<BasMaintainerApply> getBasMaintainerApplyByKeys(List<Long> keys) ;

	/**
	 * 修改
	 * @param obj
	 * @return
	 */
	public int updateBasMaintainerApply(BasMaintainerApply obj) ;

	/**
	 * 删除
	 * @param key
	 * @return
	 */
	public int deleteBasMaintainerApply(String key) ;

	/**
	 * 批量增加
	 * @param objList
	 * @return
	 */
	public int batchInsertBasMaintainerApply(List<BasMaintainerApply> objList) ;

	/**
	 * 批量修改
	 * @param objList
	 * @return
	 */
	public int batchUpdateBasMaintainerApply(List<BasMaintainerApply> objList) ;

	/**
	 * 批量删除
	 * @param keys
	 * @return
	 */
	public int batchDeleteBasMaintainerApply(List<Long> keys) ;


}