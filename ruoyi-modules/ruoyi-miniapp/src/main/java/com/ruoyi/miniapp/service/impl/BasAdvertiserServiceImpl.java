package com.ruoyi.miniapp.service.impl;

import java.util.List;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.miniapp.mapper.BasAdvertiserMapper;
import com.ruoyi.miniapp.domain.BasAdvertiser;
import com.ruoyi.miniapp.service.IBasAdvertiserService;

/**
 * 广告主信息Service业务层处理
 *
 * @author lobyliang
 * @date 2022-01-21
 */
@Service
@DS("tdddb")
public class BasAdvertiserServiceImpl extends ServiceImpl<BasAdvertiserMapper,BasAdvertiser> implements IBasAdvertiserService
{
    @Autowired
    private BasAdvertiserMapper basAdvertiserMapper;

    /**
     * 查询广告主信息
     *
     * @param userId 广告主信息主键
     * @return 广告主信息
     */
    @Override
    public BasAdvertiser selectBasAdvertiserByUserId(Long userId)
    {
        return basAdvertiserMapper.selectBasAdvertiserByUserId(userId);
    }

    /**
     * 查询广告主信息列表
     *
     * @param basAdvertiser 广告主信息
     * @return 广告主信息
     */
    @Override
    public List<BasAdvertiser> selectBasAdvertiserList(BasAdvertiser basAdvertiser)
    {
        return basAdvertiserMapper.selectBasAdvertiserList(basAdvertiser);
    }

    /**
     * 新增广告主信息
     *
     * @param basAdvertiser 广告主信息
     * @return 结果
     */
    @Override
    public int insertBasAdvertiser(BasAdvertiser basAdvertiser)
    {
        return basAdvertiserMapper.insertBasAdvertiser(basAdvertiser);
    }

    /**
     * 修改广告主信息
     *
     * @param basAdvertiser 广告主信息
     * @return 结果
     */
    @Override
    public int updateBasAdvertiser(BasAdvertiser basAdvertiser)
    {
        return basAdvertiserMapper.updateBasAdvertiser(basAdvertiser);
    }

    /**
     * 批量删除广告主信息
     *
     * @param userIds 需要删除的广告主信息主键
     * @return 结果
     */
    @Override
    public int deleteBasAdvertiserByUserIds(Long[] userIds)
    {
        return basAdvertiserMapper.deleteBasAdvertiserByUserIds(userIds);
    }

    /**
     * 删除广告主信息信息
     *
     * @param userId 广告主信息主键
     * @return 结果
     */
    @Override
    public int deleteBasAdvertiserByUserId(Long userId)
    {
        return basAdvertiserMapper.deleteBasAdvertiserByUserId(userId);
    }
}
