package com.ruoyi.miniapp.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.miniapp.domain.AdmProcPlanItem;
import com.ruoyi.miniapp.service.IAdmProcPlanItemService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 生产计划明细(一台机器一条数据)Controller
 *
 * @author lobyliang
 * @date 2022-01-14
 */
@RestController("/admProcPlanItem")
@RequestMapping("/admProcPlanItem")
@Api(tags = "生产计划子项")
public class AdmProcPlanItemController extends BaseController {
    @Autowired
    private IAdmProcPlanItemService admProcPlanItemService;

    /**
     * 查询生产计划明细(一台机器一条数据)列表
     */
    @RequiresPermissions("miniapp:admProcPlanItem:list")
    @GetMapping("/list")
    public TableDataInfo list(AdmProcPlanItem admProcPlanItem) {
        startPage();
        List<AdmProcPlanItem> list = admProcPlanItemService.selectAdmProcPlanItemList(admProcPlanItem);
        return getDataTable(list);
    }

    /**
     * 导出生产计划明细(一台机器一条数据)列表
     */
    @RequiresPermissions("miniapp:admProcPlanItem:export")
    @Log(title = "生产计划明细(一台机器一条数据)" , businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AdmProcPlanItem admProcPlanItem) throws IOException {
        List<AdmProcPlanItem> list = admProcPlanItemService.selectAdmProcPlanItemList(admProcPlanItem);
        ExcelUtil<AdmProcPlanItem> util = new ExcelUtil<AdmProcPlanItem>(AdmProcPlanItem.class);
        util.exportExcel(response, list, "生产计划明细(一台机器一条数据)数据");
    }

    /**
     * 获取生产计划明细(一台机器一条数据)详细信息
     */
    @RequiresPermissions("miniapp:admProcPlanItem:query")
    @GetMapping(value = "/{productSerialCode}")
    public AjaxResult getInfo(@PathVariable("productSerialCode") String productSerialCode) {
        return AjaxResult.success(admProcPlanItemService.selectAdmProcPlanItemByProductSerialCode(productSerialCode));
    }

    /**
     * 新增生产计划明细(一台机器一条数据)
     */
    @RequiresPermissions("miniapp:admProcPlanItem:add")
    @Log(title = "生产计划明细(一台机器一条数据)" , businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AdmProcPlanItem admProcPlanItem) {
        return toAjax(admProcPlanItemService.insertAdmProcPlanItem(admProcPlanItem));
    }

    /**
     * 修改生产计划明细(一台机器一条数据)
     */
    @RequiresPermissions("miniapp:admProcPlanItem:edit")
    @Log(title = "生产计划明细(一台机器一条数据)" , businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AdmProcPlanItem admProcPlanItem) {
        return toAjax(admProcPlanItemService.updateAdmProcPlanItem(admProcPlanItem));
    }

    /**
     * 删除生产计划明细(一台机器一条数据)
     */
    @RequiresPermissions("miniapp:admProcPlanItem:remove")
    @Log(title = "生产计划明细(一台机器一条数据)" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{productSerialCodes}")
    public AjaxResult remove(@PathVariable String[] productSerialCodes) {
        return toAjax(admProcPlanItemService.deleteAdmProcPlanItemByProductSerialCodes(productSerialCodes));
    }
}
