package com.ruoyi.miniapp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.api.domain.base.BalanceRecord;

import java.math.BigDecimal;

public interface IBalanceRecordService extends IService<BalanceRecord> {
    void save(Long userId, BigDecimal amount, BigDecimal balance, String action, String remark);
}
