package com.ruoyi.miniapp.service.impl;

import com.ruoyi.miniapp.domain.DiscountsScope;
import com.ruoyi.miniapp.mapper.DiscountsScopeMapper;
import com.ruoyi.miniapp.service.IDiscountsScopeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DiscountsScopeServiceImpl implements IDiscountsScopeService {
    @Autowired
    private DiscountsScopeMapper discountsScopeMapper;

    @Override
    public List<DiscountsScope> getDiscountsScopeBytype(Integer type) {
        List<DiscountsScope> discountsScopeInfo = discountsScopeMapper.getDiscountsScopeBytype(type);
        return discountsScopeInfo;
    }

    @Override
    public DiscountsScope getDiscountsScopeByid(Integer id) {
        DiscountsScope discountsScopeByid = discountsScopeMapper.getDiscountsScopeByid(id);
        return discountsScopeByid;
    }
}
