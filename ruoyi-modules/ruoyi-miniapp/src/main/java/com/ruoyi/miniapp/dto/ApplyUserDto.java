package com.ruoyi.miniapp.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApplyUserDto extends TrainingAddressDto{

    /**
     * 报名编号
     */
    private String applyId;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 报名地址（training_address表）
     */
    private Integer addressId;

    /**
     * 考核人id
     */
    private Long assessUserId;
    /**
     * 考核结果
     */
    private Integer evaluationResult;
    /**
     * 证书编号
     */
    private String certificateNo;
    /**
     * 订单编号
     */
    private String orderNum;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")	//出参格式化
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")	//入参格式化
    private Timestamp createDate;
    /**
     * 付款金额
     */
    private BigDecimal totalPrice;
    /**
     * 付款时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")	//出参格式化
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")	//入参格式化
    private LocalDateTime accomplishDate;
    /**
     * 操作人id
     */
    private Long operatorId;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")	//出参格式化
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")	//入参格式化
    private LocalDateTime amendDate;
    /**
     * 新地址
     */
    private Integer newAddress;
    /**
     * 状态
     */
    private Integer state;

    /**
     * 市级编码
     */
    private String fkDivisionCode;

    /**
     * 培训基地名称
     */
    private String trainingName;




}
