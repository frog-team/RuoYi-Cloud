package com.ruoyi.miniapp.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.miniapp.domain.FranchiseeJoinPayRecord;

/**
 * 广告机加盟付款记录Service接口
 *
 * @author lobyliang
 * @date 2022-01-14
 */
public interface IFranchiseeJoinPayRecordService extends IService<FranchiseeJoinPayRecord> {
    /**
     * 查询广告机加盟付款记录
     *
     * @param pkSn 广告机加盟付款记录主键
     * @return 广告机加盟付款记录
     */
    public FranchiseeJoinPayRecord selectFranchiseeJoinPayRecordByPkSn(Long pkSn);

    /**
     * 查询广告机加盟付款记录列表
     *
     * @param franchiseeJoinPayRecord 广告机加盟付款记录
     * @return 广告机加盟付款记录集合
     */
    public List<FranchiseeJoinPayRecord> selectFranchiseeJoinPayRecordList(FranchiseeJoinPayRecord franchiseeJoinPayRecord);

    /**
     * 新增广告机加盟付款记录
     *
     * @param franchiseeJoinPayRecord 广告机加盟付款记录
     * @return 结果
     */
    public int insertFranchiseeJoinPayRecord(FranchiseeJoinPayRecord franchiseeJoinPayRecord);

    /**
     * 修改广告机加盟付款记录
     *
     * @param franchiseeJoinPayRecord 广告机加盟付款记录
     * @return 结果
     */
    public int updateFranchiseeJoinPayRecord(FranchiseeJoinPayRecord franchiseeJoinPayRecord);

    /**
     * 批量删除广告机加盟付款记录
     *
     * @param pkSns 需要删除的广告机加盟付款记录主键集合
     * @return 结果
     */
    public int deleteFranchiseeJoinPayRecordByPkSns(Long[] pkSns);

    /**
     * 删除广告机加盟付款记录信息
     *
     * @param pkSn 广告机加盟付款记录主键
     * @return 结果
     */
    public int deleteFranchiseeJoinPayRecordByPkSn(Long pkSn);
}
