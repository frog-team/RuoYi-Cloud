package com.ruoyi.miniapp.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.TreeEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 资源类型对象 res_type
 *
 * @author lobyliang
 * @date 2022-01-08
 */
@ApiModel("res_type(资源类型)")
public class ResType extends TreeEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    @ApiModelProperty("主键id")
    private Long pkResTypeId;

    /** 类型对应的完整路径 */
    @ApiModelProperty("类型对应的完整路径")
    @Excel(name = "类型对应的完整路径")
    private String fullPath;

    /** 类型对应的目录名称 */
    @ApiModelProperty("类型对应的目录名称")
    @Excel(name = "类型对应的目录名称")
    private String segPath;

    /** 类型名称 */
    @ApiModelProperty("类型名称")
    @Excel(name = "类型名称")
    private String typeName;

    public void setPkResTypeId(Long pkResTypeId)
    {
        this.pkResTypeId = pkResTypeId;
    }

    public Long getPkResTypeId()
    {
        return pkResTypeId;
    }
    public void setFullPath(String fullPath)
    {
        this.fullPath = fullPath;
    }

    public String getFullPath()
    {
        return fullPath;
    }
    public void setSegPath(String segPath)
    {
        this.segPath = segPath;
    }

    public String getSegPath()
    {
        return segPath;
    }
    public void setTypeName(String typeName)
    {
        this.typeName = typeName;
    }

    public String getTypeName()
    {
        return typeName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("pkResTypeId", getPkResTypeId())
                .append("parentId", getParentId())
                .append("fullPath", getFullPath())
                .append("segPath", getSegPath())
                .append("typeName", getTypeName())
                .toString();
    }
}