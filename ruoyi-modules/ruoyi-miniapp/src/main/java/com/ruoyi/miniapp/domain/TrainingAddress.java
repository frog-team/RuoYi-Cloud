package com.ruoyi.miniapp.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.github.wxiaoqi.security.common.tool.ToolPager;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 表名：TRAINING_ADDRESS	说明：培训地址
 */
public class TrainingAddress extends ToolPager<TrainingAddress> implements Serializable {

	private static final long serialVersionUID = 3009305715212664340L;

	/**
	 * 主键
	 */
	private Integer id;
	/**
	 * 详细地址
	 */
	private String detailAddr;
	/**
	 * 市级编码
	 */
	private String fkDivisionCode;
	/**
	 * 活动负责人姓名
	 */
	private String principalName;
	/**
	 * 联系电话
	 */
	private String principalPhone;
	/**
	 * 培训基地名称
	 */
	private String trainingName;
	/**
	 * 场次名称
	 */
	private String sessionName;
	/**
	 * 开始时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")	//出参格式化
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")	//入参格式化
	private LocalDateTime startTime;
	/**
	 * 报名截止时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")	//出参格式化
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")	//入参格式化
	private LocalDateTime addressEndDate;
	/**
	 * 结束时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")	//出参格式化
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")	//入参格式化
	private LocalDateTime endTime;
	/**
	 * 名额
	 */
	private Integer places;
	/**
	 * 剩余名额
	 */
	private Integer surplusPlaces;
	/**
	 * 讲师姓名
	 */
	private String lecturerName;
	/**
	 * 讲师电话
	 */
	private String lecturerPhone;
	/**
	 * 报名费用
	 */
	private BigDecimal expense;
	/**
	 * 状态
	 */
	private Integer state;

	/**
	 * 区域名称
	 */
	private String divisionName;


	/**
	 * 是否已到截止日期
	 */
	private Integer isCutOff;


	//生成get/set方法


	public Integer getIsCutOff() {
		return isCutOff;
	}

	public void setIsCutOff(Integer isCutOff) {
		this.isCutOff = isCutOff;
	}

	public String getDivisionName() {
		return divisionName;
	}

	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}

	/**
	 * @return 主键
	 */
	public Integer getId(){
		return id;
	}
	/**
	 * @param id 主键
	 */
	public void setId(Integer id){
		this.id = id;
	}
	/**
	 * @return 详细地址
	 */
	public String getDetailAddr(){
		return detailAddr;
	}
	/**
	 * @param detailAddr 详细地址
	 */
	public void setDetailAddr(String detailAddr){
		this.detailAddr = detailAddr;
	}
	/**
	 * @return 市级编码
	 */
	public String getFkDivisionCode(){
		return fkDivisionCode;
	}
	/**
	 * @param fkDivisionCode 市级编码
	 */
	public void setFkDivisionCode(String fkDivisionCode){
		this.fkDivisionCode = fkDivisionCode;
	}
	/**
	 * @return 活动负责人姓名
	 */
	public String getSessionName(){
		return sessionName;
	}
	/**
	 * @param sessionName 活动负责人姓名
	 */
	public void setSessionName(String sessionName){
		this.sessionName = sessionName;
	}

	public String getPrincipalName() {
		return principalName;
	}

	public void setPrincipalName(String principalName) {
		this.principalName = principalName;
	}

	public String getPrincipalPhone() {
		return principalPhone;
	}

	public void setPrincipalPhone(String principalPhone) {
		this.principalPhone = principalPhone;
	}

	/**
	 * @return 培训基地名称
	 */
	public String getTrainingName(){
		return trainingName;
	}
	/**
	 * @param trainingName 培训基地名称
	 */
	public void setTrainingName(String trainingName){
		this.trainingName = trainingName;
	}
	/**
	 * @return 开始时间
	 */
	public LocalDateTime getStartTime(){
		return startTime;
	}
	/**
	 * @param startTime 开始时间
	 */
	public void setStartTime(LocalDateTime startTime){
		this.startTime = startTime;
	}

	public LocalDateTime getAddressEndDate() {
		return addressEndDate;
	}

	public void setAddressEndDate(LocalDateTime addressEndDate) {
		this.addressEndDate = addressEndDate;
	}

	/**
	 * @return 结束时间
	 */
	public LocalDateTime getEndTime(){
		return endTime;
	}
	/**
	 * @param endTime 结束时间
	 */
	public void setEndTime(LocalDateTime endTime){
		this.endTime = endTime;
	}
	/**
	 * @return 名额
	 */
	public Integer getPlaces(){
		return places;
	}
	/**
	 * @param places 名额
	 */
	public void setPlaces(Integer places){
		this.places = places;
	}

	public Integer getSurplusPlaces() {
		return surplusPlaces;
	}

	public void setSurplusPlaces(Integer surplusPlaces) {
		this.surplusPlaces = surplusPlaces;
	}

	/**
	 * @return 讲师姓名
	 */
	public String getLecturerName(){
		return lecturerName;
	}
	/**
	 * @param lecturerName 讲师姓名
	 */
	public void setLecturerName(String lecturerName){
		this.lecturerName = lecturerName;
	}
	/**
	 * @return 讲师电话
	 */
	public String getLecturerPhone(){
		return lecturerPhone;
	}
	/**
	 * @param lecturerPhone 讲师电话
	 */
	public void setLecturerPhone(String lecturerPhone){
		this.lecturerPhone = lecturerPhone;
	}
	/**
	 * @return 报名费用
	 */
	public BigDecimal getExpense(){
		return expense;
	}
	/**
	 * @param expense 报名费用
	 */
	public void setExpense(BigDecimal expense){
		this.expense = expense;
	}
	/**
	 * @return 状态
	 */
	public Integer getState(){
		return state;
	}
	/**
	 * @param state 状态
	 */
	public void setState(Integer state){
		this.state = state;
	}
}