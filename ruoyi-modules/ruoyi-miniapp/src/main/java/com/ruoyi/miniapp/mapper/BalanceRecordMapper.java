package com.ruoyi.miniapp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.api.domain.base.BalanceRecord;

public interface BalanceRecordMapper extends BaseMapper<BalanceRecord> {
}
