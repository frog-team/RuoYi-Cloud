package com.ruoyi.miniapp.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.math.BigDecimal;

/**
 * @ClassName ConCntent
 * @Description 合同模板内容 实体类
 * @Date 2022/3/22 10:35
 * @Author songping
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class ConContent {

    /**
     * 投资商分润金额，每袋子/元
     */
    @ApiModelProperty("投资商分润金额，每袋子/元")
    private BigDecimal franPricePerBag;

    /**
     * 有效年份
     */
    private Integer validYear;

}
