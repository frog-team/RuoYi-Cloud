package com.ruoyi.miniapp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.api.model.RealNameAuthInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 实名认证信息Mapper接口
 *
 * @author lobyliang
 * @date 2022-01-05
 */
@Mapper
@Repository
public interface RealNameAuthInfoMapper extends BaseMapper<RealNameAuthInfo> {

    /**
     * 查询用户实名认证信息
     *
     */
    public RealNameAuthInfo selRealNameAuth(Long Userid);

    /**
     * 查询实名认证信息
     *
     * @param id 实名认证信息主键
     * @return 实名认证信息
     */
    public RealNameAuthInfo selectRealNameAuthInfoById(Long id);

    /**
     * 查询实名认证信息列表
     *
     * @param realNameAuthInfo 实名认证信息
     * @return 实名认证信息集合
     */
    public List<RealNameAuthInfo> selectRealNameAuthInfoList(RealNameAuthInfo realNameAuthInfo);

    /**
     * 新增实名认证信息
     *
     * @param realNameAuthInfo 实名认证信息
     * @return 结果
     */
    public int insertRealNameAuthInfo(RealNameAuthInfo realNameAuthInfo);

    /**
     * 修改实名认证信息
     *
     * @param realNameAuthInfo 实名认证信息
     * @return 结果
     */
    public int updateRealNameAuthInfo(RealNameAuthInfo realNameAuthInfo);

    /**
     * 删除实名认证信息
     *
     * @param id 实名认证信息主键
     * @return 结果
     */
    public int deleteRealNameAuthInfoById(Long id);

    /**
     * 批量删除实名认证信息
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteRealNameAuthInfoByIds(Long[] ids);
}
