package com.ruoyi.miniapp.service.impl;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.OSSObject;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.miniapp.ali.OSSKeyUtil;
import com.ruoyi.miniapp.config.BucketNameConfig;
import com.ruoyi.miniapp.domain.ResItem;
import com.ruoyi.miniapp.domain.ResType;
import com.ruoyi.miniapp.mapper.ResItemMapper;
import com.ruoyi.miniapp.mapper.ResTypeMapper;
import com.ruoyi.miniapp.service.IBufferdImageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.compile;

/**
 * @author lobyliang
 */
@Service
@Slf4j
public class BufferedImageServiceImpl implements IBufferdImageService {

    @Autowired
    OSSClient ossClient;
    @Autowired
    private ResItemMapper resItemMapper;
    @Autowired
    private ResTypeMapper resTypeMapper;
    @Autowired
    BucketNameConfig config;
    /**
     * 检查是否是URL
     */
    static Pattern pattern = compile("^([hH][tT]{2}[pP]://|[hH][tT]{2}[pP][sS]://)(([A-Za-z0-9-~]+).)+([A-Za-z0-9-~\\/])+$");
    /***
     * 检查是否是UUID
     */
    static Pattern uuid = compile("^[A-Za-z0-9]{32}$");
    @Override
    public BufferedImage getBufferImage(String src){
        if(pattern.matcher(src).matches())
        {
            try
            {
                URL url = new URL(src);
                BufferedImage buf = ImageIO.read(url);
                return buf;
            } catch(IOException e)
            {
                log.error("读取文件["+src+"]失败:"+e.getLocalizedMessage(),e);
            }
        } else if(uuid.matcher(src.trim()).matches())
        {
            try{
                if(ossClient!=null)
                {
                    ResItem item= resItemMapper.selectResItemByPkResId(src);
                    ResType resType = resTypeMapper.selectResTypeByPkResTypeId(item.getFkResTypeId());
                    Long userId = SecurityUtils.getUserId();
                    String username = SecurityUtils.getUsername();
                    OSSObject object = ossClient.getObject(config.getBucketName(item.getFkResTypeId()), OSSKeyUtil.getOSSKey(item, resType, 1L, "test"));
                    if(object!=null)
                    {
                        BufferedImage image = ImageIO.read(object.getObjectContent());
                        return image;
                    }
                }
            }
            catch(Exception e)
            {
                log.error("读取OSS文件["+src+"]失败:"+e.getLocalizedMessage(),e);
            }
        }else
        {
            File file = new File(src);
            try
            {
                BufferedImage buf = ImageIO.read(file);
                return buf;
            } catch(IOException e)
            {
                log.error("读取本地文件["+src+"]失败:"+e.getLocalizedMessage(),e);
            }
        }
        return null;
    }
}
