package com.ruoyi.miniapp.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
@TableName("feedback")
public class FeedBack {

    @TableId(type = IdType.AUTO)
    private  Long id;

    private String admCode;

    private String content;

    private String unionid;

    private String imgs;

    private String contact;

    private Date createTime;

}
