package com.ruoyi.miniapp.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.redis.service.RedisService;
import com.ruoyi.miniapp.service.IAdmProcPlanItemService;
import com.ruoyi.miniapp.service.IBasVillageService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.miniapp.domain.AdmProcPlan;
import com.ruoyi.miniapp.service.IAdmProcPlanService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 生产计划(基于投资商设备购买)Controller
 *
 * @author lobyliang
 * @date 2022-01-14
 */
@RestController
@RequestMapping("/admProcPlan")
@Api(tags = "生产计划")
public class AdmProcPlanController extends BaseController {
    @Autowired
    private IAdmProcPlanService admProcPlanService;
    @Autowired
    private IAdmProcPlanItemService admProcPlanItemService;
    @Autowired
    private IBasVillageService basVillageService;
    @Autowired
    private RedisService redisService;

    /**
     * 查询生产计划(基于投资商设备购买)列表
     */
    @RequiresPermissions("miniapp:admProcPlan:list")
    @GetMapping("/list")
    public TableDataInfo list(AdmProcPlan admProcPlan) {
        startPage();
        List<AdmProcPlan> list = admProcPlanService.selectAdmProcPlanList(admProcPlan);
        return getDataTable(list);
    }

    /**
     * 导出生产计划(基于投资商设备购买)列表
     */
    @RequiresPermissions("miniapp:admProcPlan:export")
    @Log(title = "生产计划(基于投资商设备购买)" , businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AdmProcPlan admProcPlan) throws IOException {
        List<AdmProcPlan> list = admProcPlanService.selectAdmProcPlanList(admProcPlan);
        ExcelUtil<AdmProcPlan> util = new ExcelUtil<AdmProcPlan>(AdmProcPlan.class);
        util.exportExcel(response, list, "生产计划(基于投资商设备购买)数据");
    }

    /**
     * 获取生产计划(基于投资商设备购买)详细信息
     */
    @RequiresPermissions("miniapp:admProcPlan:query")
    @GetMapping(value = "/{pkAdmProcCode}")
    public AjaxResult getInfo(@PathVariable("pkAdmProcCode") String pkAdmProcCode) {
        return AjaxResult.success(admProcPlanService.selectAdmProcPlanByPkAdmProcCode(pkAdmProcCode));
    }

    /**
     * 新增生产计划(基于投资商设备购买)
     */
    @RequiresPermissions("miniapp:admProcPlan:add")
    @Log(title = "生产计划(基于投资商设备购买)" , businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AdmProcPlan admProcPlan) {
        return toAjax(admProcPlanService.insertAdmProcPlan(admProcPlan));
    }

    /**
     * 修改生产计划(基于投资商设备购买)
     */
    @RequiresPermissions("miniapp:admProcPlan:edit")
    @Log(title = "生产计划(基于投资商设备购买)" , businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AdmProcPlan admProcPlan) {
        return toAjax(admProcPlanService.updateAdmProcPlan(admProcPlan));
    }

    /**
     * 删除生产计划(基于投资商设备购买)
     */
    @RequiresPermissions("miniapp:admProcPlan:remove")
    @Log(title = "生产计划(基于投资商设备购买)" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{pkAdmProcCodes}")
    public AjaxResult remove(@PathVariable String[] pkAdmProcCodes) {
        return toAjax(admProcPlanService.deleteAdmProcPlanByPkAdmProcCodes(pkAdmProcCodes));
    }

}
