package com.ruoyi.miniapp.domain;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 生产计划(基于投资商设备购买)对象 adm_proc_plan
 *
 * @author lobyliang
 * @date 2022-01-14
 */
@ApiModel("adm_proc_plan(生产计划(基于投资商设备购买))")
@Data
@ToString
public class AdmProcPlan {
    private static final long serialVersionUID = 1L;

    /**
     * 同安装计划id，伪外健（为了和垃圾袋统一，使用char）\\nA开头，表示广告机，后续用时间+6位长整型规格化16进制编码（表示安装计划item-id)
     */
    @ApiModelProperty("同安装计划id，伪外健（为了和垃圾袋统一，使用char）\\nA开头，表示广告机，后续用时间+6位长整型规格化16进制编码（表示安装计划item-id)")
    @TableId(value = "pk_adm_proc_code" , type = IdType.ASSIGN_UUID)
    private String pkAdmProcCode;

    /**
     * from t_code_dict 0:未开始1:生产中2:生产完成proc_status发货单：1运输中,2.到货签收
     */
    @ApiModelProperty("from t_code_dict 0:未开始1:生产中2:生产完成proc_status发货单：1运输中,2.到货签收")
    @Excel(name = "from t_code_dict 0:未开始1:生产中2:生产完成proc_status发货单：1运输中,2.到货签收")
    private String dtProcStatusNo;

    /**
     * 计划创建时间
     */
    @ApiModelProperty("计划创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "计划创建时间" , width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /**
     * 所有机器生产完成时间
     */
    @ApiModelProperty("所有机器生产完成时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "所有机器生产完成时间" , width = 30, dateFormat = "yyyy-MM-dd")
    private Date finishDate;

    /**
     * 计划生产的数量
     */
    @ApiModelProperty("计划生产的数量")
    @Excel(name = "计划生产的数量")
    private String planAmount;

    /**
     * 生产完成数量
     */
    @ApiModelProperty("生产完成数量")
    @Excel(name = "生产完成数量")
    private String productedAmount;

    private String fkSeatOrderCode;

    @TableField(exist = false)
    private List<AdmProcPlanItem> admProcPlanItemList;

}
