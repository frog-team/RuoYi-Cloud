package com.ruoyi.miniapp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.api.domain.base.AccountInfo;

public interface AccountInfoMapper extends BaseMapper<AccountInfo> {
}
