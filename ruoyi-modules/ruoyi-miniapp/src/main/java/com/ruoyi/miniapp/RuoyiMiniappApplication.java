package com.ruoyi.miniapp;

import cn.hutool.cron.CronUtil;
import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import com.ruoyi.common.core.utils.WeChatUtil;
import com.ruoyi.common.security.annotation.EnableCustomConfig;
import com.ruoyi.common.security.annotation.EnableRyFeignClients;
import com.ruoyi.common.swagger.annotation.EnableCustomSwagger2;
import com.ruoyi.system.api.*;
import com.tdd.adm.service.feign.IncomeFeignClient;
import com.tdd.adm.service.feign.MonitorServerFeignClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

import java.util.Map;


@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients(clients = {RemoteLogService.class,RemoteSmsService.class,RemoteUserService.class,IncomeFeignClient.class, MonitorServerFeignClient.class,RemoteRealNameService.class,RemoteWithdrawalsRecordService.class})
@SpringBootApplication(exclude = DruidDataSourceAutoConfigure.class)
@ComponentScan(basePackages = {"com.tdd.adm.service.feign.*","com.ruoyi.*"})
public class RuoyiMiniappApplication {

    public static void main(String[] args) {
//        WeChatUtil.payUnifiedorderMA("wx4042c90418a900f1", "淘袋袋-服务商培训", "2022091311390325307", 1L, "o3oFR5NmS3fKrExW0opfp7zj42Ww");
        SpringApplication.run(RuoyiMiniappApplication.class, args);
        CronUtil.setMatchSecond(true);
        CronUtil.start();
    }

}
