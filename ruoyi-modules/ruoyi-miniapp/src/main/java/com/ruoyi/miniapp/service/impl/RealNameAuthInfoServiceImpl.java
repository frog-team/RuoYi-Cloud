package com.ruoyi.miniapp.service.impl;

import cn.hutool.core.date.DateTime;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.miniapp.mapper.RealNameAuthInfoMapper;
import com.ruoyi.miniapp.service.IRealNameAuthInfoService;
import com.ruoyi.system.api.model.RealNameAuthInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 实名认证信息Service业务层处理
 *
 * @author lobyliang
 * @date 2022-01-05
 */
@Service
@DS("tdddb")
public class RealNameAuthInfoServiceImpl extends ServiceImpl<RealNameAuthInfoMapper, RealNameAuthInfo> implements IRealNameAuthInfoService {

    @Autowired
    private RealNameAuthInfoMapper realNameAuthInfoMapper;

    /**
     * 查询用户实名认证信息
     *
     */
    @Override
    public RealNameAuthInfo selRealNameAuth(Long Userid){
        return realNameAuthInfoMapper.selRealNameAuth(Userid);
    }

    /**
     * 查询实名认证信息
     *
     * @param id 实名认证信息主键
     * @return 实名认证信息
     */
    @Override
    public RealNameAuthInfo selectRealNameAuthInfoById(Long id) {
        return baseMapper.selectRealNameAuthInfoById(id);
    }

    /**
     * 查询实名认证信息列表
     *
     * @param realNameAuthInfo 实名认证信息
     * @return 实名认证信息
     */
    @Override
    public List<RealNameAuthInfo> selectRealNameAuthInfoList(RealNameAuthInfo realNameAuthInfo) {
        return baseMapper.selectRealNameAuthInfoList(realNameAuthInfo);
    }

    /**
     * 新增实名认证信息
     *
     * @param realNameAuthInfo 实名认证信息
     * @return 结果
     */
    @Override
    public int insertRealNameAuthInfo(RealNameAuthInfo realNameAuthInfo) {
        realNameAuthInfo.setCreateTime(DateTime.now());
        return baseMapper.insertRealNameAuthInfo(realNameAuthInfo);
    }

    /**
     * 修改实名认证信息
     *
     * @param realNameAuthInfo 实名认证信息
     * @return 结果
     */
    @Override
    public int updateRealNameAuthInfo(RealNameAuthInfo realNameAuthInfo) {
        return baseMapper.updateRealNameAuthInfo(realNameAuthInfo);
    }

    /**
     * 批量删除实名认证信息
     *
     * @param ids 需要删除的实名认证信息主键
     * @return 结果
     */
    @Override
    public int deleteRealNameAuthInfoByIds(Long[] ids) {
        return baseMapper.deleteRealNameAuthInfoByIds(ids);
    }

    /**
     * 删除实名认证信息信息
     *
     * @param id 实名认证信息主键
     * @return 结果
     */
    @Override
    public int deleteRealNameAuthInfoById(Long id) {
        return baseMapper.deleteRealNameAuthInfoById(id);
    }

    @Override
    public List<RealNameAuthInfo> findByUnionId(String unionId) {
        LambdaQueryWrapper<RealNameAuthInfo> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(RealNameAuthInfo::getUnionid, unionId);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public RealNameAuthInfo findByUnionIdAndType(String unionId, Long roleType) {
        LambdaQueryWrapper<RealNameAuthInfo> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(RealNameAuthInfo::getUnionid, unionId);
        wrapper.eq(RealNameAuthInfo::getRoleType, roleType);
        List<RealNameAuthInfo> list = baseMapper.selectList(wrapper);
        if (list.size() > 0) {
            return list.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<RealNameAuthInfo> findByIdCard(String idCard){
        LambdaQueryWrapper<RealNameAuthInfo> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(RealNameAuthInfo::getIdCard,idCard);
        return baseMapper.selectList(wrapper);
    }

}
