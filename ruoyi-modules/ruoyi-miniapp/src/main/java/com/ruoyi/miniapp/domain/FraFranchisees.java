package com.ruoyi.miniapp.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 加盟商，和金额相关的信息，另建对象 fra_franchisees
 *
 * @author lobyliang
 * @date 2022-01-14
 */
@ApiModel("fra_franchisees(加盟商，和金额相关的信息，另建)")
@Data
@ToString
public class FraFranchisees {
    private static final long serialVersionUID = 1L;

    /**
     * 加盟商id(平台唯一id)
     */
    @ApiModelProperty("加盟商id(平台唯一id)")
    @TableId(value = "user_id",type = IdType.INPUT)
    private Long userId;

    /**
     * 加盟商类型no；code_dict;fran_type
     */
    @ApiModelProperty("加盟商类型no；code_dict;fran_type")
    @Excel(name = "加盟商类型no；code_dict;fran_type")
    private Integer franchiseeTypeNo;

    /**
     * 微信uid
     */
    @ApiModelProperty("微信uid")
    @Excel(name = "微信uid")
    private String fkUnionId;

    /**
     * 组织机构
     */
    @ApiModelProperty("组织机构")
    @Excel(name = "组织机构")
    private String fkOrgCode;

    /**
     * 加盟广告机数量
     */
    @ApiModelProperty("加盟广告机数量")
    @Excel(name = "加盟广告机数量")
    private Integer admAmount;

}
