package com.ruoyi.miniapp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.miniapp.domain.AdmSeats;
import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * 广告机机位,锁定机位操作在redis中进行，Mapper接口
 *
 * @author lobyliang
 * @date 2022-01-11
 */
@Mapper
public interface AdmSeatsMapper extends BaseMapper<AdmSeats> {
    /**
     * 查询广告机机位,锁定机位操作在redis中进行，
     *
     * @param pkSeatCode 广告机机位,锁定机位操作在redis中进行，主键
     * @return 广告机机位, 锁定机位操作在redis中进行，
     */
    public AdmSeats selectAdmSeatsByPkSeatCode(String pkSeatCode);

    /**
     * 查询广告机机位,锁定机位操作在redis中进行，列表
     *
     * @param admSeats 广告机机位,锁定机位操作在redis中进行，
     * @return 广告机机位, 锁定机位操作在redis中进行，集合
     */
    public List<AdmSeats> selectAdmSeatsList(AdmSeats admSeats);

    /**
     * 新增广告机机位,锁定机位操作在redis中进行，
     *
     * @param admSeats 广告机机位,锁定机位操作在redis中进行，
     * @return 结果
     */
    public int insertAdmSeats(AdmSeats admSeats);

    /**
     * 修改广告机机位,锁定机位操作在redis中进行，
     *
     * @param admSeats 广告机机位,锁定机位操作在redis中进行，
     * @return 结果
     */
    public int updateAdmSeats(AdmSeats admSeats);

    /**
     * 删除广告机机位,锁定机位操作在redis中进行，
     *
     * @param pkSeatCode 广告机机位,锁定机位操作在redis中进行，主键
     * @return 结果
     */
    public int deleteAdmSeatsByPkSeatCode(String pkSeatCode);

    /**
     * 批量删除广告机机位,锁定机位操作在redis中进行，
     *
     * @param pkSeatCodes 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAdmSeatsByPkSeatCodes(String[] pkSeatCodes);
}
