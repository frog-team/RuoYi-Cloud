package com.ruoyi.miniapp.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 加盟商购买点位订单对象 fran_seat_agreement
 *
 * @author lobyliang
 * @date 2022-01-14
 */
@ApiModel("fran_seat_agreement(加盟商购买点位订单)")
@Data
@ToString
@TableName("fran_seat_agreement")
public class FranSeatAgreement {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    @ApiModelProperty("${column.columnComment}")
    @Excel(name = "${comment}" , readConverterExp = "$column.readConverterExp()")
    @TableId(value = "pk_seat_code" , type = IdType.ASSIGN_UUID)
    private String pkSeatCode;

    /**
     * 机位数量
     */
    @ApiModelProperty("机位数量")
    @Excel(name = "机位数量")
    private Long placementAmount;

    /**
     * 总价格
     */
    @ApiModelProperty("总价格")
    @Excel(name = "总价格")
    private BigDecimal totalPrice;

    /**
     * 是否已付款(0 未付款 1 已付款 )
     */
    @ApiModelProperty("是否已付款(0 未付款 1 已付款 )")
    @Excel(name = "是否已付款(0 未付款 1 已付款 )")
    private Long isPayment;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间" , width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createDate;

    /**
     * 申请解约
     */
    @ApiModelProperty("申请解约")
    @Excel(name = "申请解约")
    private Long applyTerminate;

    /**
     * 申请时间
     */
    @ApiModelProperty("申请时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "申请时间" , width = 30, dateFormat = "yyyy-MM-dd")
    private Date applyDate;

    /**
     * 审核人
     */
    @ApiModelProperty("审核人")
    @Excel(name = "审核人")
    private String auditor;

    /**
     * 审核时间
     */
    @ApiModelProperty("审核时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "审核时间" , width = 30, dateFormat = "yyyy-MM-dd")
    private Date auditDate;

    /**
     * 是否解约(0 未解约 1 已解约)
     */
    @ApiModelProperty("是否解约(0 未解约 1 已解约)")
    @Excel(name = "是否解约(0 未解约 1 已解约)")
    private Long isTerminate;

    /**
     * 退款金额
     */
    @ApiModelProperty("退款金额")
    @Excel(name = "退款金额")
    private BigDecimal refundMoney;

    /**
     * 退款时间
     */
    @ApiModelProperty("退款时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "退款时间" , width = 30, dateFormat = "yyyy-MM-dd")
    private Date refundDate;

    /**
     * 是否退款(0 未退款 1 已退款)
     */
    @ApiModelProperty("是否退款(0 未退款 1 已退款)")
    @Excel(name = "是否退款(0 未退款 1 已退款)")
    private Long isRefund;

    private Integer hasCancel;

    private String admSeatsCodes;

    private Long franchiseesId;

    private String franchiseesUnionid;

    private String orderNo;

    private Date payTime;

    private String outTradeNo;

    private Long inviterUserId;

    @TableField(exist = false)
    private List<FranSeatAgreementItem> itemList;

    @TableField(exist = false)
    private AdmProcPlan admProcPlan;

    @TableField(exist = false)
    private Date payEndTime;

}
