package com.ruoyi.miniapp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.api.domain.base.AccountInfo;

import java.math.BigDecimal;
import java.util.Map;

public interface IAccountInfoService extends IService<AccountInfo> {
    AccountInfo initAccountInfo(Long userId, String unionid, String openid, BigDecimal balance);

    void update(Long userId, String unionid, String openid, String bankCardNo, String alipayAccount, String realName);

    Map updateBalance(Long userId, BigDecimal amount, boolean negate, String action, String remark);
}
