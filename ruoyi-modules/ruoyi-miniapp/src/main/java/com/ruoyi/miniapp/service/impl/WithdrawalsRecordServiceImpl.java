package com.ruoyi.miniapp.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.miniapp.mapper.WithdrawalsRecordMapper;
import com.ruoyi.miniapp.service.IWithdrawalsRecordService;
import com.ruoyi.system.api.domain.base.WithdrawalsRecord;
import org.springframework.stereotype.Service;

@Service
@DS("tdddb")
public class WithdrawalsRecordServiceImpl extends ServiceImpl<WithdrawalsRecordMapper, WithdrawalsRecord> implements IWithdrawalsRecordService {

    @Override
    public Page<WithdrawalsRecord> listPage(Page<WithdrawalsRecord> page,Long userId){
        LambdaQueryWrapper<WithdrawalsRecord> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(userId != null,WithdrawalsRecord::getUserId,userId);
        wrapper.orderByDesc(WithdrawalsRecord::getWithdrawalsTime);
        return baseMapper.selectPage(page,wrapper);
    }

}
