package com.ruoyi.miniapp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.miniapp.domain.AdmVillageStock;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 广告机小区库存信息Mapper接口
 *
 * @author lobyliang
 * @date 2022-01-12
 */
@Mapper
public interface AdmVillageStockMapper extends BaseMapper<AdmVillageStock> {
    /**
     * 查询广告机小区库存信息
     *
     * @param pkStockId 广告机小区库存信息主键
     * @return 广告机小区库存信息
     */
    public AdmVillageStock selectAdmVillageStockByPkStockId(Long pkStockId);

    /**
     * 查询广告机小区库存信息列表
     *
     * @param admVillageStock 广告机小区库存信息
     * @return 广告机小区库存信息集合
     */
    public List<AdmVillageStock> selectAdmVillageStockList(AdmVillageStock admVillageStock);

    /**
     * 新增广告机小区库存信息
     *
     * @param admVillageStock 广告机小区库存信息
     * @return 结果
     */
    public int insertAdmVillageStock(AdmVillageStock admVillageStock);

    /**
     * 修改广告机小区库存信息
     *
     * @param admVillageStock 广告机小区库存信息
     * @return 结果
     */
    public int updateAdmVillageStock(AdmVillageStock admVillageStock);

    /**
     * 删除广告机小区库存信息
     *
     * @param pkStockId 广告机小区库存信息主键
     * @return 结果
     */
    public int deleteAdmVillageStockByPkStockId(Long pkStockId);

    /**
     * 批量删除广告机小区库存信息
     *
     * @param pkStockIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAdmVillageStockByPkStockIds(Long[] pkStockIds);
}
