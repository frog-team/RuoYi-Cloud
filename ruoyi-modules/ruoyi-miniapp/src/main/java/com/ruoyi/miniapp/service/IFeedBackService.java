package com.ruoyi.miniapp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.miniapp.domain.FeedBack;

public interface IFeedBackService extends IService<FeedBack> {
}
