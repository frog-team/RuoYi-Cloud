package com.ruoyi.miniapp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.miniapp.domain.BasVillage;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 小区信息Mapper接口
 *
 * @author lobyliang
 * @date 2022-01-12
 */
@Mapper
public interface BasVillageMapper extends BaseMapper<BasVillage> {
    /**
     * 查询小区信息
     *
     * @param pkVillageCode 小区信息主键
     * @return 小区信息
     */
    public BasVillage selectBasVillageByPkVillageCode(String pkVillageCode);

    /**
     * 查询小区信息列表
     *
     * @param basVillage 小区信息
     * @return 小区信息集合
     */
    public List<BasVillage> selectBasVillageList(BasVillage basVillage);

    /**
     * 新增小区信息
     *
     * @param basVillage 小区信息
     * @return 结果
     */
    public int insertBasVillage(BasVillage basVillage);

    /**
     * 修改小区信息
     *
     * @param basVillage 小区信息
     * @return 结果
     */
    public int updateBasVillage(BasVillage basVillage);

    /**
     * 删除小区信息
     *
     * @param pkVillageCode 小区信息主键
     * @return 结果
     */
    public int deleteBasVillageByPkVillageCode(String pkVillageCode);

    /**
     * 批量删除小区信息
     *
     * @param pkVillageCodes 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBasVillageByPkVillageCodes(String[] pkVillageCodes);
}
