package com.ruoyi.miniapp.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.miniapp.domain.FranchiseeJoinPayRecord;
import com.ruoyi.miniapp.service.IFranchiseeJoinPayRecordService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 广告机加盟付款记录Controller
 *
 * @author lobyliang
 * @date 2022-01-14
 */
@RestController
@Api(tags = "加盟商付款记录")
@RequestMapping("franchiseeJoinPayRecord")
public class FranchiseeJoinPayRecordController extends BaseController {
    @Autowired
    private IFranchiseeJoinPayRecordService franchiseeJoinPayRecordService;

    /**
     * 查询广告机加盟付款记录列表
     */
    @RequiresPermissions("miniapp:franchiseeJoinPayRecord:list")
    @GetMapping("/list")
    public TableDataInfo list(FranchiseeJoinPayRecord franchiseeJoinPayRecord) {
        startPage();
        List<FranchiseeJoinPayRecord> list = franchiseeJoinPayRecordService.selectFranchiseeJoinPayRecordList(franchiseeJoinPayRecord);
        return getDataTable(list);
    }

    /**
     * 导出广告机加盟付款记录列表
     */
    @RequiresPermissions("miniapp:franchiseeJoinPayRecord:export")
    @Log(title = "广告机加盟付款记录" , businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FranchiseeJoinPayRecord franchiseeJoinPayRecord) throws IOException {
        List<FranchiseeJoinPayRecord> list = franchiseeJoinPayRecordService.selectFranchiseeJoinPayRecordList(franchiseeJoinPayRecord);
        ExcelUtil<FranchiseeJoinPayRecord> util = new ExcelUtil<FranchiseeJoinPayRecord>(FranchiseeJoinPayRecord.class);
        util.exportExcel(response, list, "广告机加盟付款记录数据");
    }

    /**
     * 获取广告机加盟付款记录详细信息
     */
    @RequiresPermissions("miniapp:franchiseeJoinPayRecord:query")
    @GetMapping(value = "/{pkSn}")
    public AjaxResult getInfo(@PathVariable("pkSn") Long pkSn) {
        return AjaxResult.success(franchiseeJoinPayRecordService.selectFranchiseeJoinPayRecordByPkSn(pkSn));
    }

    /**
     * 新增广告机加盟付款记录
     */
    @RequiresPermissions("miniapp:franchiseeJoinPayRecord:add")
    @Log(title = "广告机加盟付款记录" , businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FranchiseeJoinPayRecord franchiseeJoinPayRecord) {
        return toAjax(franchiseeJoinPayRecordService.insertFranchiseeJoinPayRecord(franchiseeJoinPayRecord));
    }

    /**
     * 修改广告机加盟付款记录
     */
    @RequiresPermissions("miniapp:franchiseeJoinPayRecord:edit")
    @Log(title = "广告机加盟付款记录" , businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FranchiseeJoinPayRecord franchiseeJoinPayRecord) {
        return toAjax(franchiseeJoinPayRecordService.updateFranchiseeJoinPayRecord(franchiseeJoinPayRecord));
    }

    /**
     * 删除广告机加盟付款记录
     */
    @RequiresPermissions("miniapp:franchiseeJoinPayRecord:remove")
    @Log(title = "广告机加盟付款记录" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{pkSns}")
    public AjaxResult remove(@PathVariable Long[] pkSns) {
        return toAjax(franchiseeJoinPayRecordService.deleteFranchiseeJoinPayRecordByPkSns(pkSns));
    }
}
