package com.ruoyi.miniapp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.miniapp.domain.AdmSeats;

import java.math.BigDecimal;
import java.util.List;

/**
 * 广告机机位,锁定机位操作在redis中进行，Service接口
 *
 * @author lobyliang
 * @date 2022-01-11
 */
public interface IAdmSeatsService extends IService<AdmSeats> {
    /**
     * 查询广告机机位,锁定机位操作在redis中进行，
     *
     * @param pkSeatCode 广告机机位,锁定机位操作在redis中进行，主键
     * @return 广告机机位, 锁定机位操作在redis中进行，
     */
    public AdmSeats selectAdmSeatsByPkSeatCode(String pkSeatCode);

    /**
     * 查询广告机机位,锁定机位操作在redis中进行，列表
     *
     * @param admSeats 广告机机位,锁定机位操作在redis中进行，
     * @return 广告机机位, 锁定机位操作在redis中进行，集合
     */
    public List<AdmSeats> selectAdmSeatsList(AdmSeats admSeats);

    /**
     * 新增广告机机位,锁定机位操作在redis中进行，
     *
     * @param admSeats 广告机机位,锁定机位操作在redis中进行，
     * @return 结果
     */
    public int insertAdmSeats(AdmSeats admSeats);

    /**
     * 修改广告机机位,锁定机位操作在redis中进行，
     *
     * @param admSeats 广告机机位,锁定机位操作在redis中进行，
     * @return 结果
     */
    public int updateAdmSeats(AdmSeats admSeats);

    /**
     * 批量删除广告机机位,锁定机位操作在redis中进行，
     *
     * @param pkSeatCodes 需要删除的广告机机位,锁定机位操作在redis中进行，主键集合
     * @return 结果
     */
    public int deleteAdmSeatsByPkSeatCodes(String[] pkSeatCodes);

    /**
     * 删除广告机机位,锁定机位操作在redis中进行，信息
     *
     * @param pkSeatCode 广告机机位,锁定机位操作在redis中进行，主键
     * @return 结果
     */
    public int deleteAdmSeatsByPkSeatCode(String pkSeatCode);

    Integer getVillageNum(String villageCode, Integer isEmpty);

    List<AdmSeats> findByBuildingCode(String code);

    List<AdmSeats> findByCodes(List<String> codes);

    void updateFranchiseeIdByCodes(List<String> codes, Long franchiseeId);

    void updateFranchiseeIdByCodes(String code, Long franchiseeId);

    void updateSeatEmpt(String code, Integer status);

    Integer getAdmCount(String villageCode, Integer hasBuy);

    List<String> villageAdmSeatsCodes(String code);

    /**
     * 依据 adm_seats 主键补全 投资人分润金额
     * @param code 机位主键
     * @param franPricePerBag 投资人分润金额
     * @author songping  时间 2022-03-22
     */
    void updateFranPricePerBagByCode(String code, BigDecimal franPricePerBag);
}
