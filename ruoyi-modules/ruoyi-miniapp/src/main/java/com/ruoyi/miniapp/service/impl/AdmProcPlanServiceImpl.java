package com.ruoyi.miniapp.service.impl;

import java.util.List;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.miniapp.mapper.AdmProcPlanMapper;
import com.ruoyi.miniapp.domain.AdmProcPlan;
import com.ruoyi.miniapp.service.IAdmProcPlanService;
import org.springframework.validation.annotation.Validated;

/**
 * 生产计划(基于投资商设备购买)Service业务层处理
 *
 * @author lobyliang
 * @date 2022-01-14
 */
@Service
@DS("tdddb")
public class AdmProcPlanServiceImpl extends ServiceImpl<AdmProcPlanMapper, AdmProcPlan> implements IAdmProcPlanService {
    @Autowired
    private AdmProcPlanMapper admProcPlanMapper;

    /**
     * 查询生产计划(基于投资商设备购买)
     *
     * @param pkAdmProcCode 生产计划(基于投资商设备购买)主键
     * @return 生产计划(基于投资商设备购买)
     */
    @Override
    public AdmProcPlan selectAdmProcPlanByPkAdmProcCode(String pkAdmProcCode) {
        return admProcPlanMapper.selectAdmProcPlanByPkAdmProcCode(pkAdmProcCode);
    }

    /**
     * 查询生产计划(基于投资商设备购买)列表
     *
     * @param admProcPlan 生产计划(基于投资商设备购买)
     * @return 生产计划(基于投资商设备购买)
     */
    @Override
    public List<AdmProcPlan> selectAdmProcPlanList(AdmProcPlan admProcPlan) {
        return admProcPlanMapper.selectAdmProcPlanList(admProcPlan);
    }

    /**
     * 新增生产计划(基于投资商设备购买)
     *
     * @param admProcPlan 生产计划(基于投资商设备购买)
     * @return 结果
     */
    @Override
    public int insertAdmProcPlan(AdmProcPlan admProcPlan) {
        return admProcPlanMapper.insertAdmProcPlan(admProcPlan);
    }

    /**
     * 修改生产计划(基于投资商设备购买)
     *
     * @param admProcPlan 生产计划(基于投资商设备购买)
     * @return 结果
     */
    @Override
    public int updateAdmProcPlan(AdmProcPlan admProcPlan) {
        return admProcPlanMapper.updateAdmProcPlan(admProcPlan);
    }

    /**
     * 批量删除生产计划(基于投资商设备购买)
     *
     * @param pkAdmProcCodes 需要删除的生产计划(基于投资商设备购买)主键
     * @return 结果
     */
    @Override
    public int deleteAdmProcPlanByPkAdmProcCodes(String[] pkAdmProcCodes) {
        return admProcPlanMapper.deleteAdmProcPlanByPkAdmProcCodes(pkAdmProcCodes);
    }

    /**
     * 删除生产计划(基于投资商设备购买)信息
     *
     * @param pkAdmProcCode 生产计划(基于投资商设备购买)主键
     * @return 结果
     */
    @Override
    public int deleteAdmProcPlanByPkAdmProcCode(String pkAdmProcCode) {
        return admProcPlanMapper.deleteAdmProcPlanByPkAdmProcCode(pkAdmProcCode);
    }

    @Override
    public AdmProcPlan findBySeatOrderCode(String fkSeatOrderCode){
        LambdaQueryWrapper<AdmProcPlan> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(AdmProcPlan::getFkSeatOrderCode,fkSeatOrderCode);
        return baseMapper.selectOne(wrapper);
    }
}
