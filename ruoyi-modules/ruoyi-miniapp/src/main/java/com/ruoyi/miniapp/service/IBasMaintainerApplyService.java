package com.ruoyi.miniapp.service;

import com.github.wxiaoqi.security.common.tool.ToolPager;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.miniapp.domain.BasMaintainerApply;
import com.ruoyi.miniapp.dto.ArgumentsDto;

import java.util.List;

public interface IBasMaintainerApplyService {
	/**
	 * 将二维码保存为资源接口
	 * @param resName
	 * @param content
	 * @param width
	 * @param height
	 * @return
	 */
	public AjaxResult save(String resName,String content,int width,int height);
	/**
	 * 签到
	 * @param obj
	 * @return
	 */
	public AjaxResult updateBasMaintainerApplyByUserId(BasMaintainerApply obj);



	/**
	 * 运维后台
	 * #根据场次查询用户信息
	 * @return
	 */
	public AjaxResult getUserInfoList(ArgumentsDto obj);

	/**
	 * 获取最大序列码
	 * @param key
	 * @return
	 */
	public String getCertificateNo(Integer key);

	/**
	 * 根据userid查询用户信息
	 * Userid
	 * @return
	 */
	public BasMaintainerApply getBasMaintainerApplyByUserId();

	/**
	 * 根据userid查询用户信息,未付款
	 * Userid
	 * @return
	 */
	BasMaintainerApply getBasMaintainerApplyByUserIdNoPay(Long userId);

	/**
	 * 增加
	 * @param obj
	 * @return
	 */
	public int insertBasMaintainerApply(BasMaintainerApply obj);

	/**
	 * 查count
	 * @param obj
	 * @return
	 */
	public int getBasMaintainerApplyCount(BasMaintainerApply obj);

	/**
	 * 查List
	 * @param obj
	 * @return
	 */
	public List<BasMaintainerApply> getBasMaintainerApplyList(BasMaintainerApply obj);

	/**
	 * 分页查询
	 * @param obj
	 * @return
	 */
	public ToolPager<BasMaintainerApply> getPageBasMaintainerApply(BasMaintainerApply obj);

	/**
	 * 主键查询
	 * @param applyId
	 * @return
	 */
	public BasMaintainerApply getBasMaintainerApplyByKey(String applyId);

	/**
	 * 主键查询
	 * @param orderNum
	 * @return
	 */
	BasMaintainerApply getBasMaintainerApplyByOrderNum(String orderNum);

	/**
	 * 多主键查询
	 * @param keys
	 * @return
	 */
	public List<BasMaintainerApply> getBasMaintainerApplyByKeys(List<Long> keys);

	/**
	 * 修改
	 * @param obj
	 * @return
	 */
	public int updateBasMaintainerApply(BasMaintainerApply obj);

	/**
	 * 删除
	 * @param key
	 * @return
	 */
	public int deleteBasMaintainerApply(String key);

	/**
	 * 批量增加
	 * @param objList
	 * @return
	 */
	public int batchInsertBasMaintainerApply(List<BasMaintainerApply> objList);

	/**
	 * 批量修改
	 * @param objList
	 * @return
	 */
	public int batchUpdateBasMaintainerApply(List<BasMaintainerApply> objList);

	/**
	 * 批量删除
	 * @param keys
	 * @return
	 */
	public int batchDeleteBasMaintainerApply(List<Long> keys);

}