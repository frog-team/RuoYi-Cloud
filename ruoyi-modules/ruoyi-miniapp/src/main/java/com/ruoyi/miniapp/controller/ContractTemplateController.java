package com.ruoyi.miniapp.controller;

import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.miniapp.service.IContractTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author songping
 * @date 2022/2/14 11:43:59
 * 合同模板 相关的接口
 */
@RestController
@RequestMapping("/contractTemplate")
public class ContractTemplateController {


    @Autowired
    private IContractTemplateService contractTemplateService;
    /**
     * 基于合同模板的类型 获取 合同资源的路径
     * @param conType
     * @return
     */
    @GetMapping("/getValidConTemplateByConType/{conType}")
    public AjaxResult getValidConTemplateByConType(@PathVariable Integer conType){
        return contractTemplateService.getValidConTemplateByConType(conType);
    }

}
