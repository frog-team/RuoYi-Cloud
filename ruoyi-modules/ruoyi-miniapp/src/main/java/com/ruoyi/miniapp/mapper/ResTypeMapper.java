package com.ruoyi.miniapp.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.ruoyi.miniapp.domain.ResType;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * 资源类型Mapper接口
 *
 * @author lobyliang
 * @date 2022-01-08
 */
@Mapper
@Repository
@DS("tdddb")
public interface ResTypeMapper
{
    /**
     * 查询资源类型
     *
     * @param pkResTypeId 资源类型主键
     * @return 资源类型
     */
    public ResType selectResTypeByPkResTypeId(Long pkResTypeId);

    /**
     * 查询资源类型列表
     *
     * @param resType 资源类型
     * @return 资源类型集合
     */
    public List<ResType> selectResTypeList(ResType resType);

    /**
     * 查询资源类型及其子类型
     * @param pkResTypeId
     *
     * @return
     */
    public List<ResType> getParentAllById(Long pkResTypeId);

    /**
     * 新增资源类型
     *
     * @param resType 资源类型
     * @return 结果
     */
    public int insertResType(ResType resType);

    /**
     * 修改资源类型
     *
     * @param resType 资源类型
     * @return 结果
     */
    public int updateResType(ResType resType);

    /**
     * 删除资源类型
     *
     * @param pkResTypeId 资源类型主键
     * @return 结果
     */
    public int deleteResTypeByPkResTypeId(Long pkResTypeId);

    /**
     * 批量删除资源类型
     *
     * @param pkResTypeIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteResTypeByPkResTypeIds(Long[] pkResTypeIds);
}