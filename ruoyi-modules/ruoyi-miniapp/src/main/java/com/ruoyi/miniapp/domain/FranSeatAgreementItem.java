package com.ruoyi.miniapp.domain;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.springframework.data.annotation.Id;

/**
 * 加盟商购买点位订单明细对象 fran_seat_agreement_item
 *
 * @author lobyliang
 * @date 2022-01-14
 */
@ApiModel("fran_seat_agreement_item(加盟商购买点位订单明细)")
@Data
@ToString
public class FranSeatAgreementItem {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty("主键")
    @TableId(value = "pk_fran_seat_item_id" , type = IdType.NONE)
    private Long pkFranSeatItemId;

    /**
     * 关联投资人设备购买订单表
     */
    @ApiModelProperty("关联投资人设备购买订单表")
    @Excel(name = "关联投资人设备购买订单表")
    private String fkSeatCode;

    /**
     * 机位信息
     */
    @ApiModelProperty("机位信息")
    @Excel(name = "机位信息")
    private String placementInfo;

    /**
     * 机位单价
     */
    @ApiModelProperty("机位单价")
    @Excel(name = "机位单价")
    private BigDecimal placementPrice;

    /**
     * 投资商分润金额，每袋子/元
     */
    @ApiModelProperty("投资商分润金额，每袋子/元")
    @Excel(name = "投资商分润金额，每袋子/元")
    private BigDecimal franPricePerBag;

    private String placementCode;

    private String itemOrderNo;

    private Integer vdmModelId;

    private String buildingsCode;

    private String villageCode;

    private String buildingsName;

    private String villageName;

}
