package com.ruoyi.miniapp.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;

@Data
@Configuration
@ConfigurationProperties(prefix = "cashinfo")
public class CashInfoProperty {

    private BigDecimal taxRate;

}
