package com.ruoyi.miniapp.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 广告机加盟付款记录对象 franchisee_join_pay_record
 *
 * @author lobyliang
 * @date 2022-01-14
 */
@ApiModel("franchisee_join_pay_record(广告机加盟付款记录)")
@Data
@ToString
public class FranchiseeJoinPayRecord {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    @ApiModelProperty("${column.columnComment}")
    private Long pkSn;

    /**
     * 付款记录code，即申请code
     */
    @ApiModelProperty("付款记录code，即申请code")
    @Excel(name = "付款记录code，即申请code")
    private String fkReqCode;

    /**
     * 付款金额
     */
    @ApiModelProperty("付款金额")
    @Excel(name = "付款金额")
    private Long amount;

    /**
     * 付款时间
     */
    @ApiModelProperty("付款时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "付款时间" , width = 30, dateFormat = "yyyy-MM-dd")
    private Date payTime;

    /**
     * 付款方式
     */
    @ApiModelProperty("付款方式")
    @Excel(name = "付款方式")
    private String payMethodNo;

    /**
     * 付款账号，微信，支付宝，或银行账号
     */
    @ApiModelProperty("付款账号，微信，支付宝，或银行账号")
    @Excel(name = "付款账号，微信，支付宝，或银行账号")
    private String payAccount;

}
