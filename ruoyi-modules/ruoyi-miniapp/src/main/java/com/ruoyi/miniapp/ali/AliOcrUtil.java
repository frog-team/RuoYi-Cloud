package com.ruoyi.miniapp.ali;

import com.aliyun.ocr20191230.models.RecognizeIdentityCardRequest;
import com.aliyun.ocr20191230.models.RecognizeIdentityCardResponse;
import com.aliyun.teaopenapi.models.Config;

import java.util.HashMap;
import java.util.Map;

public class AliOcrUtil {

    /**
     * 使用AK&SK初始化账号Client
     * @param accessKeyId
     * @param accessKeySecret
     * @return Client
     * @throws Exception
     */
    public static com.aliyun.ocr20191230.Client createClient(String accessKeyId, String accessKeySecret) throws Exception {
        Config config = new Config()
                // 您的AccessKey ID
                .setAccessKeyId(accessKeyId)
                // 您的AccessKey Secret
                .setAccessKeySecret(accessKeySecret);
        // 访问的域名
        config.endpoint = "ocr.cn-shanghai.aliyuncs.com";
        return new com.aliyun.ocr20191230.Client(config);
    }


    /**
     * 解析身份证人像面 名字 和 身份证号
     * @param accessKeyId
     * @param accessKeySecret
     * @param url
     * @return
     * @throws Exception
     */
    public static Map recognizeIdentityCardFace(String accessKeyId,String accessKeySecret,String url) throws Exception {
        com.aliyun.ocr20191230.Client client = AliOcrUtil.createClient(accessKeyId, accessKeySecret);
        RecognizeIdentityCardRequest recognizeIdentityCardRequest = new RecognizeIdentityCardRequest()
                .setImageURL(url)
                .setSide("face");
        // 复制代码运行请自行打印 API 的返回值
        RecognizeIdentityCardResponse response = client.recognizeIdentityCard(recognizeIdentityCardRequest);
        Map rtMap = new HashMap<>();
        rtMap.put("name",response.getBody().getData().getFrontResult().getName());
        rtMap.put("card",response.getBody().getData().getFrontResult().getIDNumber());
        return rtMap;
    }


    public static void main(String[] args_) throws Exception {
        java.util.List<String> args = java.util.Arrays.asList(args_);
        com.aliyun.ocr20191230.Client client = AliOcrUtil.createClient("accessKeyId", "accessKeySecret");
        RecognizeIdentityCardRequest recognizeIdentityCardRequest = new RecognizeIdentityCardRequest();
        // 复制代码运行请自行打印 API 的返回值
        client.recognizeIdentityCard(recognizeIdentityCardRequest);
    }

}
