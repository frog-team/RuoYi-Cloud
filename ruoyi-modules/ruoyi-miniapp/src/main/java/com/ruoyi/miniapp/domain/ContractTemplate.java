package com.ruoyi.miniapp.domain;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

/**
 * @author songping
 * @date 2022/2/14 11:50:45
 */
@ApiModel("contract_template(合同模板)")
@Data
@ToString
public class ContractTemplate {

    /**
     * 主键
     */
    private Integer pkConTemplateId;
    /**
     * 合同版本号
     */
    private String conVersionNum;
    /**
     * 合同类型
     */
    private String conType;
    /**
     * 有效标志 0=无效 1=有效
     */
    private String isValid;
    /**
     * 合同名称
     */
    private String conName;
    /**
     * 资源外键
     */
    private String fkResId;
    /**
     * 合同内容
     */
    private String content;

}
