package com.ruoyi.miniapp.controller;

import cn.hutool.crypto.SecureUtil;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.miniapp.mongo.WxUserPointRecordHandler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("wxUserPoint")
@Api(tags = "用户积分")
public class WxUserPointController {

    @Autowired
    private WxUserPointRecordHandler wxUserPointRecordHandler;

    @ApiOperation("积分明细")
    @GetMapping("listPage")
    public R listPage(Integer pageNo, Integer pageSize) {
        String unionid = SecurityUtils.getUnionId();
        return R.ok(wxUserPointRecordHandler.listPage(pageNo, pageSize, unionid));
    }

}
