package com.ruoyi.miniapp.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.miniapp.domain.FraFranchisees;
import com.ruoyi.miniapp.service.IFraFranchiseesService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 加盟商，和金额相关的信息，另建Controller
 *
 * @author lobyliang
 * @date 2022-01-14
 */
@RestController
@Api(tags = "加盟商")
@RequestMapping("franchisees")
public class FraFranchiseesController extends BaseController {
    @Autowired
    private IFraFranchiseesService fraFranchiseesService;

    /**
     * 查询加盟商，和金额相关的信息，另建列表
     */
    @RequiresPermissions("miniapp:franchisees:list")
    @GetMapping("/list")
    public TableDataInfo list(FraFranchisees fraFranchisees) {
        startPage();
        List<FraFranchisees> list = fraFranchiseesService.selectFraFranchiseesList(fraFranchisees);
        return getDataTable(list);
    }

    /**
     * 导出加盟商，和金额相关的信息，另建列表
     */
    @RequiresPermissions("miniapp:franchisees:export")
    @Log(title = "加盟商，和金额相关的信息，另建" , businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FraFranchisees fraFranchisees) throws IOException {
        List<FraFranchisees> list = fraFranchiseesService.selectFraFranchiseesList(fraFranchisees);
        ExcelUtil<FraFranchisees> util = new ExcelUtil<FraFranchisees>(FraFranchisees.class);
        util.exportExcel(response, list, "加盟商，和金额相关的信息，另建数据");
    }

    /**
     * 获取加盟商，和金额相关的信息，另建详细信息
     */
    @RequiresPermissions("miniapp:franchisees:query")
    @GetMapping(value = "/{userId}")
    public AjaxResult getInfo(@PathVariable("userId") Long userId) {
        return AjaxResult.success(fraFranchiseesService.selectFraFranchiseesByUserId(userId));
    }

    /**
     * 新增加盟商，和金额相关的信息，另建
     */
    @RequiresPermissions("miniapp:franchisees:add")
    @Log(title = "加盟商，和金额相关的信息，另建" , businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FraFranchisees fraFranchisees) {
        return toAjax(fraFranchiseesService.insertFraFranchisees(fraFranchisees));
    }

    /**
     * 修改加盟商，和金额相关的信息，另建
     */
    @RequiresPermissions("miniapp:franchisees:edit")
    @Log(title = "加盟商，和金额相关的信息，另建" , businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FraFranchisees fraFranchisees) {
        return toAjax(fraFranchiseesService.updateFraFranchisees(fraFranchisees));
    }

    /**
     * 删除加盟商，和金额相关的信息，另建
     */
    @RequiresPermissions("miniapp:franchisees:remove")
    @Log(title = "加盟商，和金额相关的信息，另建" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{userIds}")
    public AjaxResult remove(@PathVariable Long[] userIds) {
        return toAjax(fraFranchiseesService.deleteFraFranchiseesByUserIds(userIds));
    }
}
