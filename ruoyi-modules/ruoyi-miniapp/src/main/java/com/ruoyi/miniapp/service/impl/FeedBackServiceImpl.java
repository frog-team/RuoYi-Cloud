package com.ruoyi.miniapp.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.miniapp.domain.FeedBack;
import com.ruoyi.miniapp.mapper.FeedBackMapper;
import com.ruoyi.miniapp.service.IFeedBackService;
import org.springframework.stereotype.Service;

@Service
@DS("tdddb")
public class FeedBackServiceImpl extends ServiceImpl<FeedBackMapper, FeedBack> implements IFeedBackService {
}
