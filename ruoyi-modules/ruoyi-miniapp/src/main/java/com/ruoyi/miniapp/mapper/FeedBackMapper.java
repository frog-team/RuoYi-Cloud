package com.ruoyi.miniapp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.miniapp.domain.FeedBack;


public interface FeedBackMapper extends BaseMapper<FeedBack> {

}
