package com.ruoyi.miniapp.controller;

import cn.hutool.core.date.DateTime;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.miniapp.domain.FeedBack;
import com.ruoyi.miniapp.service.IFeedBackService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("feedBack")
@Api(tags = "意见反馈")
public class FeedBackController {

    @Autowired
    private IFeedBackService feedBackService;

    @ApiOperation("反馈")
    @PostMapping("add")
    public R add(@RequestBody FeedBack feedBack){
        String unionid = SecurityUtils.getUnionId();
        feedBack.setUnionid(unionid);
        feedBack.setCreateTime(DateTime.now());
        feedBackService.save(feedBack);
        return R.ok();
    }

}
