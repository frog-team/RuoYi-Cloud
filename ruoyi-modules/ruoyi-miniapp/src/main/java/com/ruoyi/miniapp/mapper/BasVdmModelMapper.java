package com.ruoyi.miniapp.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.miniapp.domain.BasVdmModel;

/**
 * 广告机型号Mapper接口
 *
 * @author lobyliang
 * @date 2022-01-14
 */
public interface BasVdmModelMapper extends BaseMapper<BasVdmModel> {
    /**
     * 查询广告机型号
     *
     * @param vdmModelId 广告机型号主键
     * @return 广告机型号
     */
    public BasVdmModel selectBasVdmModelByVdmModelId(String vdmModelId);

    /**
     * 查询广告机型号列表
     *
     * @param basVdmModel 广告机型号
     * @return 广告机型号集合
     */
    public List<BasVdmModel> selectBasVdmModelList(BasVdmModel basVdmModel);

    /**
     * 新增广告机型号
     *
     * @param basVdmModel 广告机型号
     * @return 结果
     */
    public int insertBasVdmModel(BasVdmModel basVdmModel);

    /**
     * 修改广告机型号
     *
     * @param basVdmModel 广告机型号
     * @return 结果
     */
    public int updateBasVdmModel(BasVdmModel basVdmModel);

    /**
     * 删除广告机型号
     *
     * @param vdmModelId 广告机型号主键
     * @return 结果
     */
    public int deleteBasVdmModelByVdmModelId(String vdmModelId);

    /**
     * 批量删除广告机型号
     *
     * @param vdmModelIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBasVdmModelByVdmModelIds(String[] vdmModelIds);
}
