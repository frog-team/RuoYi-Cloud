package com.ruoyi.miniapp.controller;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.miniapp.service.IInvoiceInfoService;
import com.ruoyi.system.api.domain.base.InvoiceInfo;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("invoiceInfo")
@Api(tags = "发票信息")
public class InvoiceInfoController {

    @Autowired
    private IInvoiceInfoService iInvoiceInfoService;

    @PostMapping("save")
    public R save(@RequestBody InvoiceInfo info){
        iInvoiceInfoService.save(info);
        return R.ok();
    }

}
