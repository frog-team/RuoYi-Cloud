package com.ruoyi.miniapp.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 服务商对象 bas_maintainer
 *
 * @author lobyliang
 * @date 2022-01-21
 */
@ApiModel("bas_maintainer(服务商)")
@Data
@ToString
public class BasMaintainer
{
    private static final long serialVersionUID = 1L;

    /** 服务商id(平台唯一id) */
    @ApiModelProperty("服务商id(平台唯一id)")
    @TableId(value = "user_id",type = IdType.INPUT)
    private Long userId;

    /** 服务商类型maintainer_type from t_code_dict,个人还是组织 */
    @ApiModelProperty("服务商类型maintainer_type from t_code_dict,个人还是组织")
    @Excel(name = "服务商类型maintainer_type from t_code_dict,个人还是组织")
    private Integer dtMaintainerType;

    /** 服务小区数量 */
    @ApiModelProperty("服务小区数量")
    @Excel(name = "服务小区数量")
    private Integer serverVillageAmount;

    /** 用户微信uid */
    @ApiModelProperty("用户微信uid")
    @Excel(name = "用户微信uid")
    private String fkUnionId;

    /** 合同编码 */
    @ApiModelProperty("合同编码")
    @Excel(name = "合同编码")
    private String fkContractCode;

    /** 统一社会信用代码 */
    @ApiModelProperty("统一社会信用代码")
    @Excel(name = "统一社会信用代码")
    private String fkOrgCode;

}
