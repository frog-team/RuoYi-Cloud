package com.ruoyi.miniapp.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.miniapp.domain.FranSeatAgreementItem;
import com.ruoyi.miniapp.service.IFranSeatAgreementItemService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 加盟商购买点位订单明细Controller
 *
 * @author lobyliang
 * @date 2022-01-14
 */
@RestController
@RequestMapping("franSeatAgreementItem")
@Api(tags = "加盟商购买点位订单明细")
public class FranSeatAgreementItemController extends BaseController {
    @Autowired
    private IFranSeatAgreementItemService franSeatAgreementItemService;

    /**
     * 查询加盟商购买点位订单明细列表
     */
    @RequiresPermissions("miniapp:franSeatAgreementItem:list")
    @GetMapping("/list")
    public TableDataInfo list(FranSeatAgreementItem franSeatAgreementItem) {
        startPage();
        List<FranSeatAgreementItem> list = franSeatAgreementItemService.selectFranSeatAgreementItemList(franSeatAgreementItem);
        return getDataTable(list);
    }

    /**
     * 导出加盟商购买点位订单明细列表
     */
    @RequiresPermissions("miniapp:franSeatAgreementItem:export")
    @Log(title = "加盟商购买点位订单明细" , businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FranSeatAgreementItem franSeatAgreementItem) throws IOException {
        List<FranSeatAgreementItem> list = franSeatAgreementItemService.selectFranSeatAgreementItemList(franSeatAgreementItem);
        ExcelUtil<FranSeatAgreementItem> util = new ExcelUtil<FranSeatAgreementItem>(FranSeatAgreementItem.class);
        util.exportExcel(response, list, "加盟商购买点位订单明细数据");
    }

    /**
     * 获取加盟商购买点位订单明细详细信息
     */
    @RequiresPermissions("miniapp:franSeatAgreementItem:query")
    @GetMapping(value = "/{pkFranSeatItemId}")
    public AjaxResult getInfo(@PathVariable("pkFranSeatItemId") Long pkFranSeatItemId) {
        return AjaxResult.success(franSeatAgreementItemService.selectFranSeatAgreementItemByPkFranSeatItemId(pkFranSeatItemId));
    }

    /**
     * 新增加盟商购买点位订单明细
     */
    @RequiresPermissions("miniapp:franSeatAgreementItem:add")
    @Log(title = "加盟商购买点位订单明细" , businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FranSeatAgreementItem franSeatAgreementItem) {
        return toAjax(franSeatAgreementItemService.insertFranSeatAgreementItem(franSeatAgreementItem));
    }

    /**
     * 修改加盟商购买点位订单明细
     */
    @RequiresPermissions("miniapp:franSeatAgreementItem:edit")
    @Log(title = "加盟商购买点位订单明细" , businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FranSeatAgreementItem franSeatAgreementItem) {
        return toAjax(franSeatAgreementItemService.updateFranSeatAgreementItem(franSeatAgreementItem));
    }

    /**
     * 删除加盟商购买点位订单明细
     */
    @RequiresPermissions("miniapp:franSeatAgreementItem:remove")
    @Log(title = "加盟商购买点位订单明细" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{pkFranSeatItemIds}")
    public AjaxResult remove(@PathVariable Long[] pkFranSeatItemIds) {
        return toAjax(franSeatAgreementItemService.deleteFranSeatAgreementItemByPkFranSeatItemIds(pkFranSeatItemIds));
    }
}
