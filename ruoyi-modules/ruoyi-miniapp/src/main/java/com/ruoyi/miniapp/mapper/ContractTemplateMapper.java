package com.ruoyi.miniapp.mapper;


import com.ruoyi.miniapp.domain.ContractTemplate;
import org.apache.ibatis.annotations.Mapper;

/**
 * 合同模板Mapper接口
 *
 * @author songping
 * @date 2022-02-14
 */
@Mapper
public interface ContractTemplateMapper {


    /**
     * 基于 合同类型 查询 有效的合同模板 url
     * @param conType 合同类型
     * @return
     */
    String selValidTemplateByConType(Integer conType);

    /**
     * 给予合同模板类型 查询当前合同的详细信息
     * @param conType 合同模板类型
     * @return
     */
    ContractTemplate selConTemplateByConType(Integer conType);
    /**
     * 查询行政区划表
     * @param villageCode
     * @return “湖北省武汉市江岸区大智街道”
     */
    String selNamePathByVillageCode(String villageCode);

    /**
     * 查询 最大 三位顺序码
     * @param villageCode
     * @return
     */
    String selMaxSequenceCodeByVillageCode(String villageCode);
}
