package com.ruoyi.miniapp.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.miniapp.service.IRealNameAuthInfoService;
import com.ruoyi.system.api.model.RealNameAuthInfo;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 实名认证信息Controller
 *
 * @author lobyliang
 * @date 2022-01-05
 */
@RestController
@RequestMapping("realNameInfo")
public class RealNameAuthInfoController extends BaseController {
    @Autowired
    private IRealNameAuthInfoService realNameAuthInfoService;


    /**
     * 查询实名认证信息列表
     */
    @RequiresPermissions("miniapp:info:list")
    @GetMapping("/list")
    public TableDataInfo list(RealNameAuthInfo realNameAuthInfo) {
        startPage();
        List<RealNameAuthInfo> list = realNameAuthInfoService.selectRealNameAuthInfoList(realNameAuthInfo);
        return getDataTable(list);
    }

    /**
     * 导出实名认证信息列表
     */
    @RequiresPermissions("miniapp:info:export")
    @Log(title = "实名认证信息" , businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, RealNameAuthInfo realNameAuthInfo) throws IOException {
        List<RealNameAuthInfo> list = realNameAuthInfoService.selectRealNameAuthInfoList(realNameAuthInfo);
        ExcelUtil<RealNameAuthInfo> util = new ExcelUtil<RealNameAuthInfo>(RealNameAuthInfo.class);
        util.exportExcel(response, list, "实名认证信息数据");
    }

    /**
     * 获取实名认证信息详细信息
     */
    @RequiresPermissions("miniapp:info:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(realNameAuthInfoService.selectRealNameAuthInfoById(id));
    }

    /**
     * 新增实名认证信息
     */
    @RequiresPermissions("miniapp:info:add")
    @Log(title = "实名认证信息" , businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody RealNameAuthInfo realNameAuthInfo) {
        return toAjax(realNameAuthInfoService.insertRealNameAuthInfo(realNameAuthInfo));
    }

    /**
     * 修改实名认证信息
     */
    @RequiresPermissions("miniapp:info:edit")
    @Log(title = "实名认证信息" , businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody RealNameAuthInfo realNameAuthInfo) {
        return toAjax(realNameAuthInfoService.updateRealNameAuthInfo(realNameAuthInfo));
    }

    /**
     * 删除实名认证信息
     */
    @RequiresPermissions("miniapp:info:remove")
    @Log(title = "实名认证信息" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(realNameAuthInfoService.deleteRealNameAuthInfoByIds(ids));
    }
}
