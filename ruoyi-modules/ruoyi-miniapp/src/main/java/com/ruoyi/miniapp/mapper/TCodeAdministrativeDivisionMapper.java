package com.ruoyi.miniapp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.miniapp.domain.TCodeAdministrativeDivision;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 行政区划代码GB-T2260-2017Mapper接口
 *
 * @author lobyliang
 * @date 2022-01-12
 */
@Mapper
public interface TCodeAdministrativeDivisionMapper extends BaseMapper<TCodeAdministrativeDivision> {

    /**
     * 查询所在市的名称
     * @param pkDivisionCode
     * @return
     */
    public String selDivisionNameByCode(String pkDivisionCode);

    /**
     * 查询行政区划代码GB-T2260-2017
     *
     * @param pkDivisionCode 行政区划代码GB-T2260-2017主键
     * @return 行政区划代码GB-T2260-2017
     */
    public TCodeAdministrativeDivision selectTCodeAdministrativeDivisionByPkDivisionCode(String pkDivisionCode);

    /**
     * 查询行政区划代码GB-T2260-2017列表
     *
     * @param tCodeAdministrativeDivision 行政区划代码GB-T2260-2017
     * @return 行政区划代码GB-T2260-2017集合
     */
    public List<TCodeAdministrativeDivision> selectTCodeAdministrativeDivisionList(TCodeAdministrativeDivision tCodeAdministrativeDivision);

    /**
     * 新增行政区划代码GB-T2260-2017
     *
     * @param tCodeAdministrativeDivision 行政区划代码GB-T2260-2017
     * @return 结果
     */
    public int insertTCodeAdministrativeDivision(TCodeAdministrativeDivision tCodeAdministrativeDivision);

    /**
     * 修改行政区划代码GB-T2260-2017
     *
     * @param tCodeAdministrativeDivision 行政区划代码GB-T2260-2017
     * @return 结果
     */
    public int updateTCodeAdministrativeDivision(TCodeAdministrativeDivision tCodeAdministrativeDivision);

    /**
     * 删除行政区划代码GB-T2260-2017
     *
     * @param pkDivisionCode 行政区划代码GB-T2260-2017主键
     * @return 结果
     */
    public int deleteTCodeAdministrativeDivisionByPkDivisionCode(String pkDivisionCode);

    /**
     * 批量删除行政区划代码GB-T2260-2017
     *
     * @param pkDivisionCodes 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTCodeAdministrativeDivisionByPkDivisionCodes(String[] pkDivisionCodes);
}
