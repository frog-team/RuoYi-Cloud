package com.ruoyi.miniapp.service.impl;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.StrUtil;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.miniapp.mapper.AccountInfoMapper;
import com.ruoyi.miniapp.service.IAccountInfoService;
import com.ruoyi.miniapp.service.IBalanceRecordService;
import com.ruoyi.system.api.domain.base.AccountInfo;
import com.ruoyi.system.api.domain.base.BalanceRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Service
@DS("tdddb")
public class AccountInfoServiceImpl extends ServiceImpl<AccountInfoMapper, AccountInfo> implements IAccountInfoService {

    @Autowired
    private IBalanceRecordService iBalanceRecordService;

    @Override
    public AccountInfo initAccountInfo(Long userId, String unionid, String openid, BigDecimal balance){
        AccountInfo info = this.getById(userId);
        if(info == null){
            info = new AccountInfo();
            info.setUserId(userId);
            info.setBalance(balance);
            info.setCreateDate(DateTime.now());
            info.setFkUid(unionid);
            info.setOpenid(openid);
            this.save(info);
        }
        return info;
    }

    @Override
    public void update(Long userId, String unionid, String openid, String bankCardNo, String alipayAccount, String realName){
        LambdaUpdateWrapper<AccountInfo> wrapper = Wrappers.lambdaUpdate();
        wrapper.eq(AccountInfo::getUserId,userId);
        wrapper.set(StrUtil.isNotEmpty(unionid),AccountInfo::getFkUid,unionid);
        wrapper.set(StrUtil.isNotEmpty(openid),AccountInfo::getOpenid,openid);
        wrapper.set(StrUtil.isNotEmpty(bankCardNo),AccountInfo::getBankCardNo,bankCardNo);
        wrapper.set(StrUtil.isNotEmpty(alipayAccount),AccountInfo::getAlipayAccount,alipayAccount);
        wrapper.set(StrUtil.isNotEmpty(realName),AccountInfo::getRealName,realName);
        baseMapper.update(null,wrapper);
    }

    /**
     *
     * @param userId
     * @param amount
     * @param negate  true 加   false  减
     * @param action
     * @param remark
     * @return
     */
    @Override
    public Map updateBalance(Long userId, BigDecimal amount, boolean negate, String action, String remark){
        Map rtMap = new HashMap();
        AccountInfo info = this.getById(userId);
        BigDecimal newBalance = BigDecimal.ZERO;
        if(!negate){    //减
            //比较金额是否
            if(info.getBalance().compareTo(amount) < 0){
                rtMap.put("flag",false);
                rtMap.put("msg","余额不足");
                return rtMap;
            }
            newBalance = info.getBalance().subtract(amount);
        }else {
            newBalance = info.getBalance().add(amount);
        }
        //加记录  改余额
        LambdaUpdateWrapper<AccountInfo> wrapper = Wrappers.lambdaUpdate();
        wrapper.eq(AccountInfo::getUserId,userId);
        wrapper.set(AccountInfo::getBalance,newBalance);
        this.update(null,wrapper);
        iBalanceRecordService.save(userId,amount,newBalance,action,remark);
        rtMap.put("flag",true);
        rtMap.put("balance",newBalance);
        rtMap.put("msg","success");
        return rtMap;
    }

}
