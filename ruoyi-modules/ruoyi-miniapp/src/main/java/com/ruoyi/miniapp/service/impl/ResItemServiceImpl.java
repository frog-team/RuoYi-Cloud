package com.ruoyi.miniapp.service.impl;

import com.ruoyi.miniapp.domain.ResItem;
import com.ruoyi.miniapp.mapper.ResItemMapper;
import com.ruoyi.miniapp.service.IResItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 资源条目Service业务层处理
 *
 * @author lobyliang
 * @date 2022-01-08
 */
@Service
public class ResItemServiceImpl implements IResItemService
{
    @Autowired
    private ResItemMapper resItemMapper;

    /**
     * 查询资源条目
     *
     * @param pkResId 资源条目主键
     * @return 资源条目
     */
    @Override
    @Cacheable(cacheNames = "resItem", unless = "#result==null")
    public ResItem selectResItemByPkResId(String pkResId)
    {
        return resItemMapper.selectResItemByPkResId(pkResId);
    }

    /**
     * 查询资源条目列表
     *
     * @param checkStatus
     * @param resTypeId
     * @param offsetEnd
     * @param offsetStart
     * @return 资源条目
     */
    @Override
    public List<ResItem> selectResItemList(Integer checkStatus,Long resTypeId,Integer offsetStart,Integer offsetEnd)
    {
        return resItemMapper.selectResItemList(checkStatus,resTypeId,offsetStart,offsetEnd);
    }

    @Override
    public Integer selectItemListCount(Integer checkStatus, Long resTypeId) {
        return resItemMapper.selectResItemListCount(checkStatus,resTypeId);
    }

    /**
     * 查询资源OSS外网访问地址
     * @param pkResId  pkKey
     *
     * @return OSS外网访问地址
     */
    @Override
    public String getResUrl(String pkResId){
        return resItemMapper.getResUrl(pkResId);
    }

    /**
     * 新增资源条目
     *
     * @param resItem 资源条目
     * @return 结果
     */
    @Override
    public int insertResItem(ResItem resItem)
    {
//        resItem.setCreateTime(new Date());
        return resItemMapper.insertResItem(resItem);
    }

    /**
     * 修改资源条目
     *
     * @param resItem 资源条目
     * @return 结果
     */
    @Override
    public int updateResItem(ResItem resItem)
    {
        return resItemMapper.updateResItem(resItem);
    }

    /**
     * 批量删除资源条目
     *
     * @param pkResIds 需要删除的资源条目主键
     * @return 结果
     */
    @Override
    public int deleteResItemByPkResIds(String[] pkResIds)
    {
        return resItemMapper.deleteResItemByPkResIds(pkResIds);
    }

    /**
     * 删除资源条目信息
     *
     * @param pkResId 资源条目主键
     * @return 结果
     */
    @Override
    public int deleteResItemByPkResId(String pkResId)
    {
        return resItemMapper.deleteResItemByPkResId(pkResId);
    }

    @Override
    public int softDelete(String pkResId)
    {
        return resItemMapper.softDelete(pkResId);
    }

    @Override
    public List<ResItem> selectResItemByResType(Integer checkStatus,List<String> pkResIds,long userId,Integer offsetStart,Integer offsetEnd)
    {
        return resItemMapper.selectResItemByResType(checkStatus,pkResIds,userId,offsetStart,offsetEnd);
    }

    @Override
    public Integer selectResItemByResTypeCount(Integer checkStatus, List<String> pkResIds, long ownerUId) {
        Integer count = resItemMapper.selResItemByResTypeCount(checkStatus, pkResIds, ownerUId);
        return count;
    }

}
