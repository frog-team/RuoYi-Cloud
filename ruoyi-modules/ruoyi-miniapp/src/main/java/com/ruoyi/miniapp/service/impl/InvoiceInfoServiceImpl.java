package com.ruoyi.miniapp.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.miniapp.mapper.InvoiceInfoMapper;
import com.ruoyi.miniapp.service.IInvoiceInfoService;
import com.ruoyi.system.api.domain.base.InvoiceInfo;
import org.springframework.stereotype.Service;

@Service
@DS("tdddb")
public class InvoiceInfoServiceImpl extends ServiceImpl<InvoiceInfoMapper,InvoiceInfo> implements IInvoiceInfoService {
}
