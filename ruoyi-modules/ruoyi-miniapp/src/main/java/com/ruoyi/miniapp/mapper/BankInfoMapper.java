package com.ruoyi.miniapp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.api.domain.base.BankInfo;

public interface BankInfoMapper extends BaseMapper<BankInfo> {
}
