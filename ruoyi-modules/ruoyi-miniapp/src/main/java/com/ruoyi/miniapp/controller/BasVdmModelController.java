package com.ruoyi.miniapp.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;

import com.ruoyi.common.core.domain.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.miniapp.domain.BasVdmModel;
import com.ruoyi.miniapp.service.IBasVdmModelService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 广告机型号Controller
 *
 * @author lobyliang
 * @date 2022-01-14
 */
@RestController
@RequestMapping("vdmModel")
@Api(tags = "广告机类型")
public class BasVdmModelController extends BaseController {
    @Autowired
    private IBasVdmModelService basVdmModelService;

    /**
     * 查询广告机型号列表
     */
    @RequiresPermissions("miniapp:miniapp:list")
    @GetMapping("/list")
    public TableDataInfo list(BasVdmModel basVdmModel) {
        startPage();
        List<BasVdmModel> list = basVdmModelService.selectBasVdmModelList(basVdmModel);
        return getDataTable(list);
    }

    /**
     * 导出广告机型号列表
     */
    @RequiresPermissions("miniapp:miniapp:export")
    @Log(title = "广告机型号" , businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BasVdmModel basVdmModel) throws IOException {
        List<BasVdmModel> list = basVdmModelService.selectBasVdmModelList(basVdmModel);
        ExcelUtil<BasVdmModel> util = new ExcelUtil<BasVdmModel>(BasVdmModel.class);
        util.exportExcel(response, list, "广告机型号数据");
    }

    /**
     * 获取广告机型号详细信息
     */
    @RequiresPermissions("miniapp:miniapp:query")
    @GetMapping(value = "/{vdmModelId}")
    public AjaxResult getInfo(@PathVariable("vdmModelId") String vdmModelId) {
        return AjaxResult.success(basVdmModelService.selectBasVdmModelByVdmModelId(vdmModelId));
    }

    /**
     * 新增广告机型号
     */
    @RequiresPermissions("miniapp:miniapp:add")
    @Log(title = "广告机型号" , businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BasVdmModel basVdmModel) {
        return toAjax(basVdmModelService.insertBasVdmModel(basVdmModel));
    }

    /**
     * 修改广告机型号
     */
    @RequiresPermissions("miniapp:miniapp:edit")
    @Log(title = "广告机型号" , businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BasVdmModel basVdmModel) {
        return toAjax(basVdmModelService.updateBasVdmModel(basVdmModel));
    }

    /**
     * 删除广告机型号
     */
    @RequiresPermissions("miniapp:miniapp:remove")
    @Log(title = "广告机型号" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{vdmModelIds}")
    public AjaxResult remove(@PathVariable String[] vdmModelIds) {
        return toAjax(basVdmModelService.deleteBasVdmModelByVdmModelIds(vdmModelIds));
    }

    @ApiOperation("获取广告机型号列表")
    @GetMapping("getList")
    public R getList() {
        return R.ok(basVdmModelService.list());
    }
}
