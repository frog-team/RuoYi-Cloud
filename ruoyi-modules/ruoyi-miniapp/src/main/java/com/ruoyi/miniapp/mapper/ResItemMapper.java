package com.ruoyi.miniapp.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.ruoyi.miniapp.domain.ResItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 资源条目Mapper接口
 *
 * @author lobyliang
 * @date 2022-01-07
 */
@Mapper
@Repository
@DS("tdddb")
public interface ResItemMapper
{
    /**
     * 查询资源条目
     *
     * @param pkResId 资源条目主键
     * @return 资源条目
     */
    public ResItem selectResItemByPkResId(String pkResId);

    /**
     * 查询所有的资源
     * @param checkStatus
     * @param offsetStart
     * @param offsetEnd
     * @param fkResTypeId
     * @return 资源条目集合
     */
    public List<ResItem> selectResItemList(@Param("checkStatus") Integer checkStatus,@Param("fkResTypeId") Long fkResTypeId,@Param("offsetStart") Integer offsetStart,@Param("offsetEnd") Integer offsetEnd);

    /**
     * 查询所有的资源的条数
     * @param checkStatus
     * @param fkResTypeId
     * @return
     */
    public Integer selectResItemListCount(@Param("checkStatus") Integer checkStatus,@Param("fkResTypeId") Long fkResTypeId);


    /**
     * 查询给定资源类型的列表
     * @param fkResTypeIds resType列表，逗号分割
     * @param offsetStart
     * @param offsetEnd
     * @param checkStatus
     * @param userId
     * @return 资源列表
     */
    public List<ResItem> selectResItemByResType(@Param("checkStatus") Integer checkStatus,@Param("fkResTypeIds") List<String> fkResTypeIds,@Param("fkOwnerUid")long userId,@Param("offsetStart")Integer offsetStart,@Param("offsetEnd") Integer offsetEnd);

    /**
     * 查询我的资源列表的条数
     * @param checkStatus
     * @param fkResTypeIds
     * @param userId
     * @return
     */
    public Integer selResItemByResTypeCount(@Param("checkStatus") Integer checkStatus,@Param("fkResTypeIds") List<String> fkResTypeIds,@Param("fkOwnerUid")long userId);

    /**
     * 查询资源OSS外网访问地址
     * @param pkResId  pkKey
     *
     * @return OSS外网访问地址
     */
    String getResUrl(String pkResId);

    /**
     * 新增资源条目
     *
     * @param resItem 资源条目
     * @return 结果
     */
    public int insertResItem(ResItem resItem);

    /**
     * 修改资源条目
     *
     * @param resItem 资源条目
     * @return 结果
     */
    public int updateResItem(ResItem resItem);

    /**
     * 删除资源条目
     *
     * @param pkResId 资源条目主键
     * @return 结果
     */
    public int deleteResItemByPkResId(String pkResId);

    /**
     * 批量删除资源条目
     *
     * @param pkResIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteResItemByPkResIds(String[] pkResIds);

    public int softDelete(String pkResId);


}
