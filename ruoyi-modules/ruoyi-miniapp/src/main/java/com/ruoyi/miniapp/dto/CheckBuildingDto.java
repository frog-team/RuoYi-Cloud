package com.ruoyi.miniapp.dto;

import lombok.Data;

import java.util.List;


@Data
public class CheckBuildingDto {

    private String buildingCode;
    private String buildingName;

    private List<CheckAdmSeatsDto> admSeatsList;

}
