package com.ruoyi.miniapp.service.impl;

import java.util.List;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.miniapp.domain.AdmVillageStock;
import com.ruoyi.miniapp.mapper.AdmVillageStockMapper;
import com.ruoyi.miniapp.service.IAdmVillageStockService;
import org.springframework.stereotype.Service;

/**
 * 广告机小区库存信息Service业务层处理
 *
 * @author lobyliang
 * @date 2022-01-12
 */
@Service
@DS("tdddb")
public class AdmVillageStockServiceImpl extends ServiceImpl<AdmVillageStockMapper, AdmVillageStock> implements IAdmVillageStockService {

    /**
     * 查询广告机小区库存信息
     *
     * @param pkStockId 广告机小区库存信息主键
     * @return 广告机小区库存信息
     */
    @Override
    public AdmVillageStock selectAdmVillageStockByPkStockId(Long pkStockId) {
        return baseMapper.selectAdmVillageStockByPkStockId(pkStockId);
    }

    /**
     * 查询广告机小区库存信息列表
     *
     * @param admVillageStock 广告机小区库存信息
     * @return 广告机小区库存信息
     */
    @Override
    public List<AdmVillageStock> selectAdmVillageStockList(AdmVillageStock admVillageStock) {
        return baseMapper.selectAdmVillageStockList(admVillageStock);
    }

    /**
     * 新增广告机小区库存信息
     *
     * @param admVillageStock 广告机小区库存信息
     * @return 结果
     */
    @Override
    public int insertAdmVillageStock(AdmVillageStock admVillageStock) {
        return baseMapper.insertAdmVillageStock(admVillageStock);
    }

    /**
     * 修改广告机小区库存信息
     *
     * @param admVillageStock 广告机小区库存信息
     * @return 结果
     */
    @Override
    public int updateAdmVillageStock(AdmVillageStock admVillageStock) {
        return baseMapper.updateAdmVillageStock(admVillageStock);
    }

    /**
     * 批量删除广告机小区库存信息
     *
     * @param pkStockIds 需要删除的广告机小区库存信息主键
     * @return 结果
     */
    @Override
    public int deleteAdmVillageStockByPkStockIds(Long[] pkStockIds) {
        return baseMapper.deleteAdmVillageStockByPkStockIds(pkStockIds);
    }

    /**
     * 删除广告机小区库存信息信息
     *
     * @param pkStockId 广告机小区库存信息主键
     * @return 结果
     */
    @Override
    public int deleteAdmVillageStockByPkStockId(Long pkStockId) {
        return baseMapper.deleteAdmVillageStockByPkStockId(pkStockId);
    }
}
