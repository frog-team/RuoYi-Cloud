package com.ruoyi.miniapp.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.miniapp.domain.BasAdvertiser;

/**
 * 广告主信息Service接口
 *
 * @author lobyliang
 * @date 2022-01-21
 */
public interface IBasAdvertiserService extends IService<BasAdvertiser>
{
    /**
     * 查询广告主信息
     *
     * @param userId 广告主信息主键
     * @return 广告主信息
     */
    public BasAdvertiser selectBasAdvertiserByUserId(Long userId);

    /**
     * 查询广告主信息列表
     *
     * @param basAdvertiser 广告主信息
     * @return 广告主信息集合
     */
    public List<BasAdvertiser> selectBasAdvertiserList(BasAdvertiser basAdvertiser);

    /**
     * 新增广告主信息
     *
     * @param basAdvertiser 广告主信息
     * @return 结果
     */
    public int insertBasAdvertiser(BasAdvertiser basAdvertiser);

    /**
     * 修改广告主信息
     *
     * @param basAdvertiser 广告主信息
     * @return 结果
     */
    public int updateBasAdvertiser(BasAdvertiser basAdvertiser);

    /**
     * 批量删除广告主信息
     *
     * @param userIds 需要删除的广告主信息主键集合
     * @return 结果
     */
    public int deleteBasAdvertiserByUserIds(Long[] userIds);

    /**
     * 删除广告主信息信息
     *
     * @param userId 广告主信息主键
     * @return 结果
     */
    public int deleteBasAdvertiserByUserId(Long userId);
}
