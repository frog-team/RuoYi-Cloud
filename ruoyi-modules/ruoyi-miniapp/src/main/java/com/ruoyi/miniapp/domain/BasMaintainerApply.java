package com.ruoyi.miniapp.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.github.wxiaoqi.security.common.tool.ToolPager;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 表名：BAS_MAINTAINER_APPLY	说明：用户报名表
 */
public class BasMaintainerApply extends ToolPager<BasMaintainerApply> implements Serializable {

	private static final long serialVersionUID = 8171462621960226993L;

	/**
	 * 报名编号
	 */
	private String applyId;
	/**
	 * 用户id
	 */
	private Long userId;
	/**
	 * 报名地址（training_address表）
	 */
	private Integer addressId;

	/**
	 * 考核人id
	 */
	private Long assessUserId;
	/**
	 * 考核结果
	 */
	private Integer evaluationResult;

	/**
	 * 考核备注
	 */
	private String remark;
	/**
	 * 考核时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")	//出参格式化
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")	//入参格式化
	private LocalDateTime evaluationDate;
	/**
	 * 证书编号
	 */
	private String certificateNo;
	/**
	 * 订单编号
	 */
	private String orderNum;
	/**
	 * 创建时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")	//出参格式化
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")	//入参格式化
	private LocalDateTime createDate;
	/**
	 * 付款金额
	 */
	private BigDecimal totalPrice;
	/**
	 * 付款时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")	//出参格式化
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")	//入参格式化
	private LocalDateTime accomplishDate;
	/**
	 * 操作人id
	 */
	private Long operatorId;
	/**
	 * 修改时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")	//出参格式化
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")	//入参格式化
	private LocalDateTime amendDate;
	/**
	 * 新地址
	 */
	private Integer newAddress;
	/**
	 * 状态
	 */
	private Integer state;

	/**
	 * 签到时间
	 */
	private Date signinDate;

	/**
	 * 用户头像资源路径
	 */
	private String itemId;

	/**
	 * 到期时间
	 */
	private Date overdueoutDate;

	/**
	 * 二维码资源路径
	 */
	private String qrcodeId;


//	/**
//	 * 市级编码
//	 */
//	private String fkDivisionCode;
//
//	/**
//	 * 市级名称
//	 */
//	private String fkDivisionName;
//
//	/**
//	 * 培训基地名称
//	 */
//	private String trainingName;
	/**
	 * 屏幕折扣id
	 */
	private Integer type1;

	/**
	 * 袋身折扣id
	 */
	private Integer type2;

	//生成get/set方法


	public Integer getType1() {
		return type1;
	}

	public void setType1(Integer type1) {
		this.type1 = type1;
	}

	public Integer getType2() {
		return type2;
	}

	public void setType2(Integer type2) {
		this.type2 = type2;
	}

	public Date getSigninDate() {
		return signinDate;
	}

	public void setSigninDate(Date signinDate) {
		this.signinDate = signinDate;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

//	public String getFkDivisionName() {
//		return fkDivisionName;
//	}
//
//	public void setFkDivisionName(String fkDivisionName) {
//		this.fkDivisionName = fkDivisionName;
//	}

	public LocalDateTime getEvaluationDate() {
		return evaluationDate;
	}

	public void setEvaluationDate(LocalDateTime evaluationDate) {
		this.evaluationDate = evaluationDate;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public Date getOverdueoutDate() {
		return overdueoutDate;
	}

	public void setOverdueoutDate(Date overdueoutDate) {
		this.overdueoutDate = overdueoutDate;
	}

	public String getQrcodeId() {
		return qrcodeId;
	}

	public void setQrcodeId(String qrcodeId) {
		this.qrcodeId = qrcodeId;
	}

//	public String getTrainingName() {
//		return trainingName;
//	}
//
//	public void setTrainingName(String trainingName) {
//		this.trainingName = trainingName;
//	}
//
//	public String getFkDivisionCode() {
//		return fkDivisionCode;
//	}
//
//	public void setFkDivisionCode(String fkDivisionCode) {
//		this.fkDivisionCode = fkDivisionCode;
//	}

	public Long getAssessUserId() {
		return assessUserId;
	}

	public void setAssessUserId(Long assessUseId) {
		this.assessUserId = assessUseId;
	}

	/**
	 * @return 报名编号
	 */
	public String getApplyId(){
		return applyId;
	}
	/**
	 * @param applyId 报名编号
	 */
	public void setApplyId(String applyId){
		this.applyId = applyId;
	}
	/**
	 * @return 用户id
	 */
	public Long getUserId(){
		return userId;
	}
	/**
	 * @param userId 用户id
	 */
	public void setUserId(Long userId){
		this.userId = userId;
	}
	/**
	 * @return 报名地址（training_address表）
	 */
	public Integer getAddressId(){
		return addressId;
	}
	/**
	 * @param addressId 报名地址（training_address表）
	 */
	public void setAddressId(Integer addressId){
		this.addressId = addressId;
	}
	/**
	 * @return 考核结果
	 */
	public Integer getEvaluationResult(){
		return evaluationResult;
	}
	/**
	 * @param evaluationResult 考核结果
	 */
	public void setEvaluationResult(Integer evaluationResult){
		this.evaluationResult = evaluationResult;
	}
	/**
	 * @return 证书编号
	 */
	public String getCertificateNo(){
		return certificateNo;
	}
	/**
	 * @param certificateNo 证书编号
	 */
	public void setCertificateNo(String certificateNo){
		this.certificateNo = certificateNo;
	}
	/**
	 * @return 订单编号
	 */
	public String getOrderNum(){
		return orderNum;
	}
	/**
	 * @param orderNum 订单编号
	 */
	public void setOrderNum(String orderNum){
		this.orderNum = orderNum;
	}
	/**
	 * @return 创建时间
	 */
	public LocalDateTime getCreateDate(){
		return createDate;
	}
	/**
	 * @param createDate 创建时间
	 */
	public void setCreateDate(LocalDateTime createDate){
		this.createDate = createDate;
	}
	/**
	 * @return 付款金额
	 */
	public BigDecimal getTotalPrice(){
		return totalPrice;
	}
	/**
	 * @param totalPrice 付款金额
	 */
	public void setTotalPrice(BigDecimal totalPrice){
		this.totalPrice = totalPrice;
	}
	/**
	 * @return 付款时间
	 */
	public LocalDateTime getAccomplishDate(){
		return accomplishDate;
	}
	/**
	 * @param accomplishDate 付款时间
	 */
	public void setAccomplishDate(LocalDateTime accomplishDate){
		this.accomplishDate = accomplishDate;
	}
	/**
	 * @return 操作人id
	 */
	public Long getOperatorId(){
		return operatorId;
	}
	/**
	 * @param operatorId 操作人id
	 */
	public void setOperatorId(Long operatorId){
		this.operatorId = operatorId;
	}
	/**
	 * @return 修改时间
	 */
	public LocalDateTime getAmendDate(){
		return amendDate;
	}
	/**
	 * @param amendDate 修改时间
	 */
	public void setAmendDate(LocalDateTime amendDate){
		this.amendDate = amendDate;
	}
	/**
	 * @return 新地址
	 */
	public Integer getNewAddress(){
		return newAddress;
	}
	/**
	 * @param newAddress 新地址
	 */
	public void setNewAddress(Integer newAddress){
		this.newAddress = newAddress;
	}
	/**
	 * @return 状态
	 */
	public Integer getState(){
		return state;
	}
	/**
	 * @param state 状态
	 */
	public void setState(Integer state){
		this.state = state;
	}
}