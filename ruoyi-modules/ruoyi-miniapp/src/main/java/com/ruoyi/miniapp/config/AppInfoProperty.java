package com.ruoyi.miniapp.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@Configuration
@ConfigurationProperties(prefix = "appinfo")
public class AppInfoProperty {

    private String appDownloadUrl;

    private List<Contact> contacts;

    @Data
    public static class Contact {

        private String title;

        private String city;

        private String address;

        private String phone;

    }

}
