package com.ruoyi.miniapp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.api.domain.base.BankInfo;

public interface IBankInfoService extends IService<BankInfo> {
}
