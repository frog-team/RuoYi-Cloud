package com.ruoyi.miniapp.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "wechat.miniapp")
public class WeChatMiniappProperty {

    private String appid;

    private String appsecret;

}
