package com.ruoyi.miniapp.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.miniapp.domain.FraFranchisees;

/**
 * 加盟商，和金额相关的信息，另建Mapper接口
 *
 * @author lobyliang
 * @date 2022-01-14
 */
public interface FraFranchiseesMapper extends BaseMapper<FraFranchisees> {
    /**
     * 查询加盟商，和金额相关的信息，另建
     *
     * @param userId 加盟商，和金额相关的信息，另建主键
     * @return 加盟商，和金额相关的信息，另建
     */
    public FraFranchisees selectFraFranchiseesByUserId(Long userId);

    /**
     * 查询加盟商，和金额相关的信息，另建列表
     *
     * @param fraFranchisees 加盟商，和金额相关的信息，另建
     * @return 加盟商，和金额相关的信息，另建集合
     */
    public List<FraFranchisees> selectFraFranchiseesList(FraFranchisees fraFranchisees);

    /**
     * 新增加盟商，和金额相关的信息，另建
     *
     * @param fraFranchisees 加盟商，和金额相关的信息，另建
     * @return 结果
     */
    public int insertFraFranchisees(FraFranchisees fraFranchisees);

    /**
     * 修改加盟商，和金额相关的信息，另建
     *
     * @param fraFranchisees 加盟商，和金额相关的信息，另建
     * @return 结果
     */
    public int updateFraFranchisees(FraFranchisees fraFranchisees);

    /**
     * 删除加盟商，和金额相关的信息，另建
     *
     * @param userId 加盟商，和金额相关的信息，另建主键
     * @return 结果
     */
    public int deleteFraFranchiseesByUserId(Long userId);

    /**
     * 批量删除加盟商，和金额相关的信息，另建
     *
     * @param userIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFraFranchiseesByUserIds(Long[] userIds);
}
