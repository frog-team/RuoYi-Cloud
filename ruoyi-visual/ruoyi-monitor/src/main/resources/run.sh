#!/bin/bash
echo "Stop Procedure : ruoyi-visual-monitor.jar"
pid=`ps -ef |grep java|grep ruoyi-visual-monitor.jar|awk '{print $2}'`
echo 'old service pid:'$pid
if [ "$pid" = "" ]
then
  echo "service is stoped"
else
  kill -9 $pid
  echo 'service is killed'
fi
sleep 3
nohup java -jar /opt/monitor/ruoyi-visual-monitor.jar > /opt/monitor/log/monitor.log 2>&1 &
pid=`ps -ef |grep java|grep ruoyi-visual-monitor.jar|awk '{print $2}'`
echo 'monitor started:/opt/monitor/log/monitor.log PID:'$pid
# tail -f /opt/monitor/log/monitor.log